'use strict';

exports = module.exports = {
    get
};

const assert = require('assert'),
    BoxError = require('./boxerror.js'),
    dashboard = require('./dashboard.js'),
    domains = require('./domains.js'),
    ejs = require('ejs'),
    fs = require('fs'),
    mail = require('./mail.js'),
    mailServer = require('./mailserver.js'),
    safe = require('safetydance'),
    superagent = require('./superagent.js');

const MAIL_AUTOCONFIG_EJS = fs.readFileSync(__dirname + '/autoconfig.xml.ejs', { encoding: 'utf8' });

async function get(domain, location) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof location, 'string');

    if (location === 'autoconfig/mail/config-v1.1.xml') { // this also gets a ?emailaddress
        const mailDomain = await mail.getDomain(domain);
        if (!mailDomain) throw new BoxError(BoxError.NOT_FOUND, 'Domain not found');
        if (!mailDomain.enabled) throw new BoxError(BoxError.NOT_FOUND, 'Email not enabled');
        const { fqdn } = await mailServer.getLocation();

        const autoconfig = ejs.render(MAIL_AUTOCONFIG_EJS, { domain, fqdn });

        return { type: 'text/xml', body: autoconfig };
    } else if (location === 'host-meta' || location === 'matrix/server' || location === 'matrix/client') {
        const type = location === 'host-meta' ? 'text/xml' : 'application/json';

        const domainObject = await domains.get(domain);
        if (!domainObject) throw new BoxError(BoxError.NOT_FOUND, 'Domain not found');
        if (!domainObject.wellKnown || !(location in domainObject.wellKnown)) throw new BoxError(BoxError.NOT_FOUND, 'No custom well-known config');

        return { type, body: domainObject.wellKnown[location] };
    } else if (location === 'openid-configuration') {
        const { fqdn:dashboardFqdn } = await dashboard.getLocation();
        if (domain !== dashboardFqdn) throw new BoxError(BoxError.NOT_FOUND, 'No custom well-known config');
        // the oidc-provider module does not expose this in javascript but only via a route handler
        // we have to use the external route even
        const [error, result] = await safe(superagent.get(`https://${dashboardFqdn}/openid/.well-known/openid-configuration`));
        if (error) return new BoxError(BoxError.INTERNAL_ERROR, 'unable to connect to internal OpenID routes');

        return { type: 'application/json', body: result.body };
    } else {
        throw new BoxError(BoxError.NOT_FOUND, 'No custom well-known config');
    }
}
