'use strict';

exports = module.exports = {
    get,
    set,

    getJson,
    setJson,

    getBlob,
    setBlob,

    APPSTORE_API_TOKEN_KEY: 'appstore_api_token',
    API_SERVER_ORIGIN_KEY: 'api_server_origin',
    AUTOUPDATE_PATTERN_KEY: 'autoupdate_pattern',
    BACKUP_STORAGE_KEY: 'backup_storage',
    BACKUP_LIMITS_KEY: 'backup_limits',
    BACKUP_POLICY_KEY: 'backup_policy',
    CLOUDRON_AVATAR_KEY: 'cloudron_avatar',
    CLOUDRON_BACKGROUND_KEY: 'cloudron_background',
    CLOUDRON_ID_KEY: 'cloudron_id',
    CLOUDRON_NAME_KEY: 'cloudron_name',
    CONSOLE_SERVER_ORIGIN_KEY: 'console_server_origin',
    DASHBOARD_DOMAIN_KEY: 'dashboard_domain',
    DASHBOARD_SUBDOMAIN_KEY: 'dashboard_subdomain',
    DIRECTORY_SERVER_KEY: 'directory_server_config',
    DYNAMIC_DNS_KEY: 'dynamic_dns',
    EXTERNAL_LDAP_KEY: 'external_ldap_config',
    FOOTER_KEY: 'footer',
    FIREWALL_BLOCKLIST_KEY: 'firewall_blocklist',
    GHOSTS_CONFIG_KEY: 'ghosts_config',
    IPV4_CONFIG_KEY: 'ipv4_config',
    IPV6_CONFIG_KEY: 'ipv6_config',
    LANGUAGE_KEY: 'language',
    MAIL_DOMAIN_KEY: 'mail_domain',
    MAIL_SUBDOMAIN_KEY: 'mail_subdomain',
    OIDC_COOKIE_SECRET_KEY: 'cookie_secret',
    PROFILE_CONFIG_KEY: 'profile_config',
    REGISTRY_CONFIG_KEY: 'registry_config',
    REVERSE_PROXY_CONFIG_KEY: 'reverseproxy_config',
    SERVICES_CONFIG_KEY: 'services_config',
    TIME_ZONE_KEY: 'time_zone',
    TRUSTED_IPS_KEY: 'trusted_ips_key',
    WEB_SERVER_ORIGIN_KEY: 'web_server_origin',

    // testing
    _clear: clear,
    _set: set
};

const assert = require('assert'),
    database = require('./database.js'),
    safe = require('safetydance');

const SETTINGS_FIELDS = [ 'name', 'value' ].join(',');
const SETTINGS_BLOB_FIELDS = [ 'name', 'valueBlob' ].join(',');

async function get(key) {
    assert.strictEqual(typeof key, 'string');

    const result = await database.query(`SELECT ${SETTINGS_FIELDS} FROM settings WHERE name = ?`, [ key ]);
    if (result.length === 0) return null; // can't return the default value here because we might need to massage/json parse the result

    return result[0].value;
}

async function set(key, value) {
    assert.strictEqual(typeof key, 'string');
    assert(value === null || typeof value === 'string');

    await database.query('INSERT INTO settings (name, value) VALUES (?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)', [ key, value ]); // don't rely on affectedRows here since it gives 2
}

async function getJson(key) {
    assert.strictEqual(typeof key, 'string');

    return safe.JSON.parse(await get(key));
}

async function setJson(key, value) {
    assert.strictEqual(typeof key, 'string');
    assert.strictEqual(typeof value, 'object'); // can be null

    await set(key, value ? JSON.stringify(value) : null);
}

async function getBlob(key) {
    assert.strictEqual(typeof key, 'string');

    const result = await database.query(`SELECT ${SETTINGS_BLOB_FIELDS} FROM settings WHERE name = ?`, [ key ]);
    if (result.length === 0) return null;

    return result[0].valueBlob;
}

async function setBlob(key, value) {
    assert.strictEqual(typeof key, 'string');
    assert(value === null || Buffer.isBuffer(value));

    await database.query('INSERT INTO settings (name, valueBlob) VALUES (?, ?) ON DUPLICATE KEY UPDATE valueBlob=VALUES(valueBlob)', [ key, value ]); // don't rely on affectedRows here since it gives 2
}

async function clear() {
    await database.query('DELETE FROM settings');
}
