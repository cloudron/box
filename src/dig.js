'use strict';

exports = module.exports = {
    resolve,
};

const assert = require('assert'),
    constants = require('./constants.js'),
    dns = require('dns');

// a note on TXT records. It doesn't have quotes ("") at the DNS level. Those quotes
// are added for DNS server software to enclose spaces. Such quotes may also be returned
// by the DNS REST API of some providers
async function resolve(hostname, rrtype, options) {
    assert.strictEqual(typeof hostname, 'string');
    assert.strictEqual(typeof rrtype, 'string');
    assert(options && typeof options === 'object'); // { server, timeout }

    const resolver = new dns.promises.Resolver({ timeout: options.timeout || 10000, tries: options.tries || 2 });

    if (constants.CLOUDRON) resolver.setServers([ options.server || '127.0.0.150' ]); // unbound runs here

    const result = await resolver.resolve(hostname, rrtype);

    // when you query a random record, it errors with ENOTFOUND. But, if you query a record which has a different type
    // we sometimes get empty array and sometimes ENODATA. for TXT records, result is 2d array of strings
    return result;
}
