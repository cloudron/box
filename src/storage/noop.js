'use strict';

exports = module.exports = {
    getAvailableSize,

    upload,
    exists,
    download,
    copy,

    listDir,

    remove,
    removeDir,

    cleanup,

    testConfig,
    removePrivateFields,
    injectPrivateFields
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    debug = require('debug')('box:storage/noop'),
    fs = require('fs');

async function getAvailableSize(apiConfig) {
    assert.strictEqual(typeof apiConfig, 'object');

    return Number.POSITIVE_INFINITY;
}

async function upload(apiConfig, backupFilePath) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof backupFilePath, 'string');

    debug(`upload: ${backupFilePath}`);

    const uploadStream = fs.createWriteStream('/dev/null');

    return {
        stream: uploadStream,
        async finish() {}
    };
}

async function exists(apiConfig, backupFilePath) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof backupFilePath, 'string');

    debug(`exists: ${backupFilePath}`);

    return false;
}

async function download(apiConfig, backupFilePath) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof backupFilePath, 'string');

    debug('download: %s', backupFilePath);

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'Cannot download from noop backend');
}

async function listDir(apiConfig, dir, batchSize, marker) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof dir, 'string');
    assert.strictEqual(typeof batchSize, 'number');
    assert(typeof marker !== 'undefined');

    return { entries: [], marker: null };
}

async function copy(apiConfig, oldFilePath, newFilePath, progressCallback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof oldFilePath, 'string');
    assert.strictEqual(typeof newFilePath, 'string');
    assert.strictEqual(typeof progressCallback, 'function');

    debug(`copy: ${oldFilePath} -> ${newFilePath}`);
}

async function remove(apiConfig, filename) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof filename, 'string');

    debug(`remove: ${filename}`);
}

async function removeDir(apiConfig, pathPrefix, progressCallback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof pathPrefix, 'string');
    assert.strictEqual(typeof progressCallback, 'function');

    debug(`removeDir: ${pathPrefix}`);
}

async function cleanup(apiConfig, progressCallback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof progressCallback, 'function');
}

async function testConfig(apiConfig) {
    assert.strictEqual(typeof apiConfig, 'object');
}

function removePrivateFields(apiConfig) {
    return apiConfig;
}

// eslint-disable-next-line no-unused-vars
function injectPrivateFields(newConfig, currentConfig) {
}
