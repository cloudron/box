'use strict';

exports = module.exports = {
    checkForUpdates,

    getUpdateInfo,

    _checkAppUpdates: checkAppUpdates
};

const apps = require('./apps.js'),
    appstore = require('./appstore.js'),
    assert = require('assert'),
    debug = require('debug')('box:updatechecker'),
    notifications = require('./notifications.js'),
    paths = require('./paths.js'),
    safe = require('safetydance'),
    util = require('util');

function setUpdateInfo(state) {
    // appid -> update info { creationDate, manifest }
    // box -> { version, changelog, upgrade, sourceTarballUrl }
    state.version = 2;
    if (!safe.fs.writeFileSync(paths.UPDATE_CHECKER_FILE, JSON.stringify(state, null, 4), 'utf8')) debug(`setUpdateInfo: Error writing to update checker file: ${safe.error.message}`);
}

function getUpdateInfo() {
    const state = safe.JSON.parse(safe.fs.readFileSync(paths.UPDATE_CHECKER_FILE, 'utf8'));
    if (!state || state.version !== 2) return {};
    delete state.version;
    return state;
}

async function checkAppUpdates(options) {
    assert.strictEqual(typeof options, 'object');

    debug('checkAppUpdates: checking for updates');

    const state = getUpdateInfo();
    const newState = { };  // create new state so that old app ids are removed

    const result = await apps.list();

    for (const app of result) {
        if (app.appStoreId === '') continue; // appStoreId can be '' for dev apps

        const [error, updateInfo] = await safe(appstore.getAppUpdate(app, options));
        if (error) {
            debug('checkAppUpdates: Error getting app update info for %s', app.id, error);
            continue;  // continue to next
        }

        if (!updateInfo) continue; // skip if no next version is found
        newState[app.id] = updateInfo;
    }

    if ('box' in state) newState.box = state.box; // preserve the latest box state information
    setUpdateInfo(newState);
}

async function checkBoxUpdates(options) {
    assert.strictEqual(typeof options, 'object');

    debug('checkBoxUpdates: checking for updates');

    const updateInfo = await appstore.getBoxUpdate(options);

    const state = getUpdateInfo();

    if (!updateInfo) { // no update
        if ('box' in state) {
            delete state.box;
            setUpdateInfo(state);
        }
        debug('checkBoxUpdates: no updates');
        return;
    }

    if (util.isDeepStrictEqual(state.box, updateInfo)) {
        debug(`checkBoxUpdates: Skipping notification of box update ${updateInfo.version} as user was already notified`);
        return;
    }

    debug(`checkBoxUpdates: ${updateInfo.version} is available. renotification: ${!!state.box}`);

    const changelog = updateInfo.changelog.map((m) => `* ${m}\n`).join('');
    const message = `Changelog:\n${changelog}\n\nGo to the Settings view to update.\n\n`;

    await notifications.pin(notifications.TYPE_BOX_UPDATE, `Cloudron v${updateInfo.version} is available`, message, { context: updateInfo.version });

    state.box = updateInfo;
    setUpdateInfo(state);
}

async function checkForUpdates(options) {
    assert.strictEqual(typeof options, 'object');

    const [boxError] = await safe(checkBoxUpdates(options));
    if (boxError) debug('checkForUpdates: error checking for box updates: %o', boxError);

    const [appError] = await safe(checkAppUpdates(options));
    if (appError) debug('checkForUpdates: error checking for app updates: %o', appError);
}
