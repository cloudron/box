'use strict';

const assert = require('assert');

class Location {
    constructor(subdomain, domain, type, certificate) {
        assert(subdomain === null || typeof subdomain === 'string');
        assert(domain === null || typeof domain === 'string');
        assert.strictEqual(typeof type, 'string');

        this.subdomain = subdomain;
        this.domain = domain;
        this.type = type;
        this.certificate = certificate || null;
        this.fqdn = domain ? (subdomain + (subdomain ? '.' : '') + domain) : '';
    }
}

// subdomain (table) types
Location.TYPE_PRIMARY = 'primary';
Location.TYPE_SECONDARY = 'secondary';
Location.TYPE_REDIRECT = 'redirect';
Location.TYPE_ALIAS = 'alias';
Location.TYPE_DASHBOARD = 'dashboard';
Location.TYPE_MAIL = 'mail';
Location.TYPE_DIRECTORY_SERVER = 'directoryserver';

exports = module.exports = Location;
