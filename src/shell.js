'use strict';

const assert = require('assert'),
    BoxError = require('./boxerror.js'),
    child_process = require('child_process'),
    debug = require('debug')('box:shell'),
    once = require('./once.js'),
    util = require('util');

exports = module.exports = shell;

function shell(tag) {
    assert.strictEqual(typeof tag, 'string');

    return {
        bash: bash.bind(null, tag),
        spawn: spawn.bind(null, tag),
        sudo: sudo.bind(null, tag),
        promises: { sudo: util.promisify(sudo.bind(null, tag)) }
    };
}

const SUDO = '/usr/bin/sudo';

function lineCount(buffer) {
    assert(Buffer.isBuffer(buffer));

    const NEW_LINE = Buffer.from('\n');
    let index = buffer.indexOf(NEW_LINE);
    let count = 0;
    while (index >= 0) {
        index = buffer.indexOf(NEW_LINE, index+1);
        ++count;
    }
    return count;
}

function spawn(tag, file, args, options) {
    assert.strictEqual(typeof tag, 'string');
    assert.strictEqual(typeof file, 'string');
    assert(Array.isArray(args));
    assert.strictEqual(typeof options, 'object'); // note: spawn() has no encoding option of it's own

    debug(`${tag}: ${file} ${args.join(' ').replace(/\n/g, '\\n')}`);

    const maxLines = options.maxLines || Number.MAX_SAFE_INTEGER;

    return new Promise((resolve, reject) => {
        const cp = child_process.spawn(file, args, options);
        const stdoutBuffers = [], stderrBuffers = [];
        let stdoutLineCount = 0, stderrLineCount = 0;

        cp.stdout.on('data', (data) => {
            stdoutBuffers.push(data);
            stdoutLineCount += lineCount(data);
            if (stdoutLineCount >= maxLines) return cp.kill('SIGKILL');
        });
        cp.stderr.on('data', (data) => {
            stderrBuffers.push(data);
            stderrLineCount += lineCount(data);
            if (stderrLineCount >= maxLines) return cp.kill('SIGKILL');
        });

        cp.on('close', function (code, signal) { // always called. after 'exit' or 'error'
            const stdoutBuffer = Buffer.concat(stdoutBuffers);
            const stdout = options.encoding ? stdoutBuffer.toString(options.encoding) : stdoutBuffer;
            if (code === 0) return resolve(stdout);

            const stderrBuffer = Buffer.concat(stderrBuffers);
            const stderr = options.encoding ? stderrBuffer.toString(options.encoding) : stderrBuffer;

            const e = new BoxError(BoxError.SHELL_ERROR, `${file} exited with code ${code} signal ${signal}`);
            e.stdout = stdout; // when promisified, this is the way to get stdout
            e.stdoutLineCount = stdoutLineCount;
            e.stderr = stderr; // when promisified, this is the way to get stderr
            e.stderrLineCount = stderrLineCount;
            e.code = code;
            e.signal = signal;

            debug(`${tag}: ${file} ${args.join(' ').replace(/\n/g, '\\n')} errored`, e);

            reject(e);
        });

        cp.on('error', function (error) { // when the command itself could not be started
            debug(`${tag}: ${file} ${args.join(' ').replace(/\n/g, '\\n')} errored`, error);
        });

        // https://github.com/nodejs/node/issues/25231
        if (options.input) {
            cp.stdin.write(options.input);
            cp.stdin.end();
        }
    });
}

async function bash(tag, script, options) {
    assert.strictEqual(typeof tag, 'string');
    assert.strictEqual(typeof script, 'string');
    assert.strictEqual(typeof options, 'object');

    return await spawn(tag, '/bin/bash', [ '-c', script ], options);
}

function sudo(tag, args, options, callback) {
    assert.strictEqual(typeof tag, 'string');
    assert(Array.isArray(args));
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof callback, 'function');

    const sudoArgs = [ '-S' ]; // -S makes sudo read stdin for password
    if (options.preserveEnv) sudoArgs.push('-E'); // -E preserves environment

    callback = once(callback); // exit may or may not be called after an 'error'

    const logFunc = options.outputHasTimestamps ? process.stdout.write : debug;

    if (options.ipc) {
        sudoArgs.push('--close-from=4'); // keep the ipc open. requires closefrom_override in sudoers file
        options.stdio = ['pipe', 'pipe', 'pipe', 'ipc'];
    }

    const spawnArgs = sudoArgs.concat(args);

    debug(`${tag} ${SUDO} ${spawnArgs.join(' ').replace(/\n/g, '\\n')}`);
    const cp = child_process.spawn(SUDO, spawnArgs, options);
    let stdoutResult = '';

    cp.stdout.on('data', (data) => {
        if (options.captureStdout) stdoutResult += data.toString('utf8');
        if (!options.quiet) logFunc(data.toString('utf8'));
    });
    cp.stderr.on('data', (data) => logFunc(data.toString('utf8')));

    cp.on('exit', function (code, signal) {
        if (code === 0) return callback(null, options.captureStdout ? stdoutResult : null);
        const e = new BoxError(BoxError.SHELL_ERROR, `${tag} exited with code ${code} signal ${signal}`);
        e.code = code;
        e.signal = signal;

        if (cp.terminated) {
            debug(`${tag}: ${SUDO} ${spawnArgs.join(' ').replace(/\n/g, '\\n')} terminated`); // was killed by us
        } else {
            debug(`${tag}: ${SUDO} ${spawnArgs.join(' ').replace(/\n/g, '\\n')} errored`, e);
        }

        callback(e);
    });

    cp.on('error', function (error) {
        debug(`${tag}: ${SUDO} ${spawnArgs.join(' ').replace(/\n/g, '\\n')} errored`, error);
        const e = new BoxError(BoxError.SHELL_ERROR, `${tag} errored with code ${error.code} message ${error.message}`);
        e.code = error.code;
        e.signal = error.signal;
        callback(e);
    });

    // sudo forks and execs the program. sudo also hangs around as the parent of the program waiting on the program and also forwarding signals.
    // sudo does not forward signals when the originator comes from the same process group. recently, there has been a change where it will
    // forward signals as long as sudo or the command is not the group leader (https://www.sudo.ws/repos/sudo/rev/d1bf60eac57f)
    // for us, this means that calling kill from this node process doesn't work since it's in the same group (and ubuntu 22 doesn't have the above fix).
    // the workaround is to invoke a kill from a different process group and this is done by starting detached
    // another idea is: use "ps --pid cp.pid -o pid=" to get the pid of the command and then send it signal directly
    cp.terminate = function () {
        cp.terminated = true; // hint for better debug message in 'exit'
        child_process.spawn('kill', ['-SIGTERM', cp.pid], { detached: true }, (error) => { if (error) debug(`${tag} could not terminate`, error); });
    };

    cp.stdin.end();
    if (options.onMessage) cp.on('message', options.onMessage);

    return cp;
}
