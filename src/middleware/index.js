'use strict';

exports = module.exports = {
    cookieParser: require('cookie-parser'),
    cors: require('./cors.js'),
    lastMile: require('connect-lastmile'),
    multipart: require('./multipart.js'),
    timeout: require('connect-timeout')
};
