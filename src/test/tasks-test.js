/* jslint node:true */
/* global it:false */
/* global before:false */
/* global after:false */
/* global describe:false */

'use strict';

const BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    fs = require('fs'),
    paths = require('../paths.js'),
    safe = require('safetydance'),
    tasks = require('../tasks.js'),
    _ = require('../underscore.js');

describe('task', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    let taskId;

    const TASK = {
        type: 'tasktype',
        args: [ 1 ],
        percent: 0,
        message: 'Queued'
    };

    it('add succeeds', async function () {
        const id = await tasks.add(TASK.type, TASK.args);
        expect(id).to.be.ok();
        taskId = id;
    });

    it('get succeeds', async function () {
        const task = await tasks.get(taskId);
        expect(_.pick(task, Object.keys(TASK))).to.eql(TASK);
    });

    it('get random task fails', async function () {
        const task = await tasks.get('random');
        expect(task).to.be(null);
    });

    it('update succeeds', async function () {
        TASK.percent = 34;
        TASK.message = 'almost ther';
        await tasks.update(taskId, { percent: TASK.percent, message: TASK.message });
        const task = await tasks.get(taskId);
        expect(_.pick(task, Object.keys(TASK))).to.eql(TASK);
    });

    it('list succeeds - does not exist', async function () {
        const result = await tasks.listByTypePaged('randomtask', 1, 1);
        expect(result.length).to.be(0);
    });

    it('list succeeds - by type', async function () {
        const result = await tasks.listByTypePaged(TASK.type, 1, 1);
        expect(result.length).to.be(1);
        expect(_.pick(result[0], Object.keys(TASK))).to.eql(TASK);
    });

    it('list succeeds - all', async function () {
        const result = await tasks.listByTypePaged(null, 1, 1);
        expect(result.length).to.be(1);
        expect(_.pick(result[0], Object.keys(TASK))).to.eql(TASK);
    });

    it('del succeeds', async function () {
        await tasks._del(taskId);
        const task = await tasks.get(taskId);
        expect(task).to.be(null);
    });

    it('del missing task fails', async function () {
        const [error] = await safe(tasks._del('1235'));
        expect(error.reason).to.be(BoxError.NOT_FOUND);
    });

    it('can run valid task - success', async function () {
        const taskId = await tasks.add(tasks._TASK_IDENTITY, [ 'ping' ]);

        return new Promise((resolve, reject) => {
            tasks.startTask(taskId, {}, function (error, result) {
                if (error) return reject(error);
                expect(result).to.equal('ping');
                resolve();
            });
        });
    });

    it('can run valid task - error', async function () {
        const taskId = await tasks.add(tasks._TASK_ERROR, [ 'ping' ]);

        return new Promise((resolve, reject) => {
            tasks.startTask(taskId, {}, function (error, result) {
                if (!error) return reject(new Error('expecting task to fail'));
                expect(error.message).to.be('Failed for arg: ping');
                expect(result).to.be(null);
                resolve();
            });
        });
    });

    it('can get logs of crash', async function () {
        const taskId = await tasks.add(tasks._TASK_CRASH, [ 'ping' ]);

        return new Promise((resolve, reject) => {
            tasks.startTask(taskId, {}, function (error, result) {
                if (!error) return reject(new Error('expecting task to crash'));
                expect(error.message).to.contain(`Task ${taskId} crashed`);
                expect(result).to.be(null);

                const logs = fs.readFileSync(`${paths.TASKS_LOG_DIR}/${taskId}.log`, 'utf8');
                expect(logs).to.contain('Crashing for arg: ping');
                resolve();
            });
        });
    });

    it('can stop task', async function () {
        const taskId = await tasks.add(tasks._TASK_SLEEP, [ 10000 ]);

        return new Promise((resolve, reject) => {
            tasks.startTask(taskId, {}, function (error, result) {
                if (!error) return reject(new Error('expecting task to stop'));
                expect(error.message).to.contain('stopped');
                expect(result).to.be(null);
                resolve();
            });

            setTimeout(async function () {
                await tasks.stopTask(taskId);
            }, 2000);
        });
    });
});
