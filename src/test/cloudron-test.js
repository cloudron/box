/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    cloudron = require('../cloudron.js'),
    expect = require('expect.js'),
    safe = require('safetydance');

describe('Cloudron', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    describe('timezone', function () {
        it('can get default timezone', async function () {
            const tz = await cloudron.getTimeZone();
            expect(tz).to.be('UTC');
        });

        it('cannot set invalid timezone', async function () {
            const [error] = await safe(cloudron.setTimeZone('OMG'));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot set valid timezone', async function () {
            await cloudron.setTimeZone('Africa/Johannesburg');
            const tz = await cloudron.getTimeZone();
            expect(tz).to.be('Africa/Johannesburg');
        });
    });

    describe('language', function () {
        it('can get default language', async function () {
            const lang = await cloudron.getLanguage();
            expect(lang).to.be('en');
        });

        it('cannot set invalid language', async function () {
            const [error] = await safe(cloudron.setLanguage('ta'));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot set valid language', async function () {
            await cloudron.setLanguage('de');
            const lang = await cloudron.getLanguage();
            expect(lang).to.be('de');
        });
    });
});
