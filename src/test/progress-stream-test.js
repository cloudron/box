/* global it:false */
/* global describe:false */

'use strict';

const expect = require('expect.js'),
    fs = require('fs'),
    ProgressStream = require('../progress-stream.js'),
    stream = require('stream');

describe('progress stream', function () {
    it('can create stream', function (done) {
        const input = fs.createReadStream(`${__dirname}/progress-stream-test.js`);
        const progress = new ProgressStream({ interval: 1000 });
        const output = fs.createWriteStream('/dev/null');

        stream.pipeline(input, progress, output, function (error) {
            expect(error).to.not.be.ok();
            const size = fs.statSync(`${__dirname}/progress-stream-test.js`).size;
            expect(progress._transferred).to.be(size);

            done();
        });
    });
});
