/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const addonConfigs = require('../addonconfigs.js'),
    common = require('./common.js'),
    expect = require('expect.js');

describe('Addon config', function () {
    const { setup, cleanup, app } = common;

    before(setup);
    after(cleanup);

    it('returns empty addon config array for invalid app', async function () {
        const results = await addonConfigs.getByAppId('randomid');
        expect(results).to.eql([ ]);
    });

    it('set succeeds', async function () {
        await addonConfigs.set(app.id, 'addonid1', [ { name: 'ENV1', value: 'env' }, { name: 'ENV2', value: 'env2' } ]);
        await addonConfigs.set(app.id, 'addonid2', [ { name: 'ENV3', value: 'env' } ]);
    });

    it('get succeeds', async function () {
        const results = await addonConfigs.get(app.id, 'addonid1');
        expect(results).to.eql([ { name: 'ENV1', value: 'env' }, { name: 'ENV2', value: 'env2' } ]);
    });

    it('getByAppId succeeds', async function () {
        const results = await addonConfigs.getByAppId(app.id);
        expect(results).to.eql([ { name: 'ENV1', value: 'env' }, { name: 'ENV2', value: 'env2' }, { name: 'ENV3', value: 'env' } ]);
    });

    it('getByName succeeds', async function () {
        const value = await addonConfigs.getByName(app.id, 'addonid1', 'ENV2');
        expect(value).to.be('env2');
    });

    it('getByName of unknown value succeeds', async function () {
        const value = await addonConfigs.getByName(app.id, 'addonid1', 'ENVRANDOM');
        expect(value).to.be(null);
    });

    it('getAppIdByValue succeeds', async function () {
        const appId = await addonConfigs.getAppIdByValue('addonid1', 'ENV1', 'env');
        expect(appId).to.be(app.id);
    });

    it('getAppIdByValue pattern succeeds', async function () {
        const appId = await addonConfigs.getAppIdByValue('addonid1', '%ENV1', 'env');
        expect(appId).to.be(app.id);
    });

    it('getAppIdByValue pattern of unknown succeeds', async function () {
        const appId = await addonConfigs.getAppIdByValue('addonid1', '%ENV1', 'envx');
        expect(appId).to.be(null);
    });

    it('unset succeeds', async function () {
        await addonConfigs.unset(app.id, 'addonid1');
    });

    it('unsetAddonConfig did remove configs', async function () {
        const results = await addonConfigs.getByAppId(app.id);
        expect(results).to.eql([ { name: 'ENV3', value: 'env' }]);
    });

    it('unsetByAppId succeeds', async function () {
        await addonConfigs.unsetByAppId(app.id);
    });

    it('unsetByAppId did remove configs', async function () {
        const results = await addonConfigs.getByAppId(app.id);
        expect(results).to.eql([ ]);
    });
});
