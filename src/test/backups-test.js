/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const backups = require('../backups.js'),
    BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    safe = require('safetydance');

describe('backups', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    const boxBackup = {
        id: null,
        remotePath: 'backup-box',
        encryptionVersion: 2,
        packageVersion: '1.0.0',
        type: backups.BACKUP_TYPE_BOX,
        state: backups.BACKUP_STATE_NORMAL,
        identifier: 'box',
        dependsOn: [ 'dep1' ],
        manifest: null,
        format: 'tgz',
        preserveSecs: 0,
        label: '',
        appConfig: null
    };

    const appBackup = {
        id: null,
        remotePath: 'app_appid_123',
        encryptionVersion: null,
        packageVersion: '1.0.0',
        type: backups.BACKUP_TYPE_APP,
        state: backups.BACKUP_STATE_CREATING,
        identifier: 'appid',
        dependsOn: [ ],
        manifest: { foo: 'bar' },
        format: 'tgz',
        preserveSecs: 0,
        label: '',
        appConfig: null
    };

    describe('crud', function () {
        it('add succeeds', async function () {
            boxBackup.id = await backups.add(boxBackup);
        });

        it('fails with duplicate path', async function () {
            const [error] = await safe(backups.add(boxBackup));
            expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
        });

        it('get succeeds', async function () {
            const result = await backups.get(boxBackup.id);
            delete result.creationTime;
            expect(result).to.eql(boxBackup);
        });

        it('get of unknown id fails', async function () {
            const result = await backups.get('somerandom');
            expect(result).to.be(null);
        });

        it('getByTypePaged succeeds', async function () {
            const results = await backups.getByTypePaged(backups.BACKUP_TYPE_BOX, 1, 5);
            expect(results.length).to.be(1);
            delete results[0].creationTime;
            expect(results[0]).to.eql(boxBackup);
        });

        it('update succeeds', async function () {
            await backups.update(boxBackup.id, { label: 'DuMonde', preserveSecs: 30 });
            const result = await backups.get(boxBackup.id);
            expect(result.label).to.eql('DuMonde');
            expect(result.preserveSecs).to.eql(30);
        });

        it('delete succeeds', async function () {
            await backups.del(boxBackup.id);
            const result = await backups.get(boxBackup.id);
            expect(result).to.be(null);
        });

        it('add app backup succeeds', async function () {
            appBackup.id = await backups.add(appBackup);
        });

        it('get app backup succeeds', async function () {
            const result = await backups.get(appBackup.id);
            delete result.creationTime;
            expect(result).to.eql(appBackup);
        });

        it('getByIdentifierAndStatePaged succeeds', async function () {
            const results = await backups.getByIdentifierAndStatePaged(appBackup.identifier, backups.BACKUP_STATE_CREATING, 1, 5);
            expect(results.length).to.be(1);
            delete results[0].creationTime;
            expect(results[0]).to.eql(appBackup);
        });

        it('delete app backup succeeds', async function () {
            await backups.del(appBackup.id);
            const result = await backups.get(appBackup.id);
            expect(result).to.be(null);
        });
    });

    describe('config and policy', function () {
        it('can get backup config', async function () {
            const backupConfig = await backups.getConfig();
            expect(backupConfig.provider).to.be('filesystem');
            expect(backupConfig.backupFolder).to.be('/var/backups');
        });

        it('can set backup config', async function () {
            let backupConfig = await backups.getConfig();
            backupConfig = Object.assign({}, backupConfig, { backupFolder: '/tmp/backups' });
            await backups.setConfig(backupConfig);

            const newBackupConfig = await backups.getConfig();
            expect(newBackupConfig.backupFolder).to.be('/tmp/backups');
        });

        it('cannot set backup policy with invalid schedule', async function () {
            const [error] = await safe(backups.setPolicy({ schedule: '', retention: { keepWithinSecs: 1 }}));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot set backup policy with missing retention', async function () {
            const [error] = await safe(backups.setPolicy({ schedule: '00 * * * * *'}));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot set backup policy with invalid retention', async function () {
            const [error] = await safe(backups.setPolicy({ schedule: '00 * * * * *', retention: { keepWhenever: 4 }}));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('can set valid backup policy', async function () {
            await backups.setPolicy({ schedule: '00 00 2,23 * * 0,1,2', retention: { keepWithinSecs: 1 }});
        });
    });
});
