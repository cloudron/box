/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    expect = require('expect.js'),
    system = require('../system.js');

describe('System', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    it('can get filesystems', async function () {
        // does not work on archlinux 8!
        if (require('child_process').execSync('uname -a').toString().indexOf('-arch') !== -1) return;

        const disks = await system._getFilesystems();
        expect(disks).to.be.ok();
        expect(Object.keys(disks).some(fs => disks[fs].mountpoint === '/')).to.be.ok();
    });

    it('can get swaps', async function () {
        // does not work on archlinux 8!
        if (require('child_process').execSync('uname -a').toString().indexOf('-arch') !== -1) return;

        const swaps = await system.getSwaps();
        expect(swaps).to.be.ok();
        expect(Object.keys(swaps).some(n => swaps[n].type === 'partition' || swaps[n].type === 'file')).to.be.ok();
    });

    it('can check for disk space', async function () {
        // does not work on archlinux 8!
        if (require('child_process').execSync('uname -a').toString().indexOf('-arch') !== -1) return;

        await system.checkDiskSpace();
    });

    it('can get memory', async function () {
        const memory = await system.getMemory();
        expect(memory.memory).to.be.a('number');
        expect(memory.swap).to.be.a('number');
    });

    it('can get CPUs', async function () {
        const cpus = await system.getCpus();
        expect(cpus).to.be.an(Array);
        expect(cpus[0].model).to.be.a('string');
    });

    it('can get info', async function () {
        const info = await system.getInfo();

        expect(info.sysVendor).to.be.a('string');
        expect(info.productName).to.be.a('string');
        expect(info.uptimeSecs).to.be.a('number');
        expect(info.rebootRequired).to.be.a('boolean');
    });

    it('can get diskUsage', async function () {
        const usage = await system.getDiskUsage();
        expect(usage).to.be(null); // nothing cached
    });

    it('can updateDiskUsage', async function () {
        const usage = await system.updateDiskUsage(function () {});
        const tmp = usage[Object.keys(usage)[0]];

        expect(tmp.size).to.be.greaterThan(0);
        expect(tmp.used).to.be.greaterThan(0);
        expect(tmp.available).to.be.greaterThan(0);
        expect(tmp.capacity).to.be.greaterThan(0);
        expect(tmp.speed).to.be.a('number');
    });
});
