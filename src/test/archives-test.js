/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const archives = require('../archives.js'),
    backups = require('../backups.js'),
    BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    safe = require('safetydance');

describe('Archives', function () {
    const { setup, cleanup, auditSource } = common;

    const appBackup = {
        id: null,
        remotePath: 'backup-box',
        encryptionVersion: 2,
        packageVersion: '1.0.0',
        type: backups.BACKUP_TYPE_APP,
        state: backups.BACKUP_STATE_NORMAL,
        identifier: 'box',
        dependsOn: [ 'dep1' ],
        manifest: null,
        format: 'tgz',
        preserveSecs: 0,
        label: '',
        appConfig: { loc: 'loc1' }
    };

    before(async function () {
        await setup();
        appBackup.id = await backups.add(appBackup);
    });
    after(cleanup);

    let archiveId;

    it('cannot add bad backup to archives', async function () {
        const [error] = await safe(archives.add('badId', {}, auditSource));
        expect(error.reason).to.be(BoxError.NOT_FOUND);
    });

    it('can add good backup to archives', async function () {
        archiveId = await archives.add(appBackup.id, {}, auditSource);
    });

    it('cannot get invalid archive', async function () {
        const result = await archives.get('bad');
        expect(result).to.be(null);
    });

    it('can get archive', async function () {
        const result = await archives.get(archiveId);
        expect(result.appConfig).to.eql(appBackup.appConfig);
    });

    it('can list archives', async function () {
        const result = await archives.list(1, 100);
        expect(result.length).to.be(1);
        expect(result[0].id).to.be(archiveId);
        expect(result[0].appConfig).to.eql(appBackup.appConfig);
    });

    it('can list backupIds', async function () {
        const result = await archives.listBackupIds();
        expect(result.length).to.be(1);
        expect(result[0]).to.be(appBackup.id);
    });

    it('cannot delete bad archive', async function () {
        const [error] = await safe(archives.del({ id: 'badId' }, auditSource));
        expect(error.reason).to.be(BoxError.NOT_FOUND);
    });

    it('can del valid archive', async function () {
        await archives.del({ id: archiveId }, auditSource);
        const result = await archives.list(1, 10);
        expect(result.length).to.be(0);
    });
});
