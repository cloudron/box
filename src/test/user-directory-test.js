/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    expect = require('expect.js'),
    tokens = require('../tokens.js'),
    userDirectory = require('../user-directory.js');

describe('User Directory', function () {
    const { setup, cleanup, admin, auditSource } = common;

    before(setup);
    after(cleanup);

    describe('profile config', function () {
        it('can get default profile config', async function () {
            const profileConfig = await userDirectory.getProfileConfig();
            expect(profileConfig.lockUserProfiles).to.be(false);
            expect(profileConfig.mandatory2FA).to.be(false);
        });

        it('can set default profile config', async function () {
            await tokens.add({ name: 'token1', identifier: admin.id, clientId: tokens.ID_WEBADMIN, expires: Number.MAX_SAFE_INTEGER, lastUsedTime: null, allowedIpRanges: '' });
            let result = await tokens.listByUserId(admin.id);
            expect(result.length).to.be(1); // just confirm the token was really added!

            await userDirectory.setProfileConfig({ mandatory2FA: true, lockUserProfiles: true }, { persistUserIdSessions: 'random' }, auditSource);
            result = await tokens.listByUserId(admin.id);
            expect(result.length).to.be(0); // should have been removed by mandatory 2fa setting change
        });
    });
});
