/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const blobs = require('../blobs.js'),
    common = require('./common.js'),
    expect = require('expect.js');

describe('blobs', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    it('can set value', async function () {
        await blobs.set('someid', Buffer.from('somevalue'));
    });
    it('can get the set value', async function () {
        const value = await blobs.get('someid');
        expect(value).to.eql(Buffer.from('somevalue'));
    });
    it('can update a value', async function () {
        await blobs.set('someid', Buffer.from('someothervalue'));
    });
    it('can get updated value', async function () {
        const value = await blobs.get('someid');
        expect(value).to.eql(Buffer.from('someothervalue'));
    });
    it('cannot get randome value', async function () {
        const value = await blobs.get('unknownid');
        expect(value).to.be(null);
    });
});
