'use strict';

/* global it, describe, before, after */

const expect = require('expect.js'),
    _ = require('../underscore.js');

describe('Underscore', function () {
    it('pick', function () {
        expect(_.pick({ x: 1, y: 2 }, ['x'])).to.eql({x: 1});
        expect(_.pick({ x: 1, y: 2 }, ['x', 'y', 'z'])).to.eql({x: 1, y:2});
        expect(_.pick({ x: 1, y: 2 }, ['z'])).to.eql({});
        expect(_.pick({ x: 1, y: 2 }, [])).to.eql({});
    });

    it('omit', function () {
        expect(_.omit({ x: 1, y: 2 }, ['x'])).to.eql({ y: 2 });
        expect(_.omit({ x: 1, y: 2 }, ['x', 'y', 'z'])).to.eql({});
        expect(_.omit({ x: 1, y: 2 }, ['z'])).to.eql({ x: 1, y: 2 });
        expect(_.omit({ x: 1, y: 2 }, [])).to.eql({ x: 1, y: 2 });
    });

    it('intersection', function () {
        expect(_.intersection([], [2,3])).to.eql([]);
        expect(_.intersection([1,2], [2,3])).to.eql([2]);
        expect(_.intersection([1,2], [3])).to.eql([]);
        expect(_.intersection([1,2], [1,2])).to.eql([1,2]);
    });

    it('isEqual', function () {
        expect(_.isEqual(null, null)).to.be(true);

        expect(_.isEqual(1, 1)).to.be(true);
        expect(_.isEqual(1, 2)).to.be(false);

        expect(_.isEqual([], [])).to.be(true);
        expect(_.isEqual([2,3,4], [3,4,2])).to.be(false);

        expect(_.isEqual({x:1}, {y:2})).to.be(false);
        expect(_.isEqual({x:1}, {x:1})).to.be(true);
        expect(_.isEqual({x:1,z:2}, {z:1,x:1})).to.be(false);
        expect(_.isEqual({x:1,y:3,z:2}, {y:3,z:2,x:1})).to.be(true);
    });

    it('difference', function () {
        expect(_.difference([1,2], [2,3])).to.eql([1]);
        expect(_.difference([1,2], [3])).to.eql([1,2]);
        expect(_.difference([1,2], [1,2])).to.eql([]);
        expect(_.difference([], [1,2])).to.eql([]);
    });
});
