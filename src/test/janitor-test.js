/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    expect = require('expect.js'),
    janitor = require('../janitor.js'),
    tokens = require('../tokens.js');

describe('janitor', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    const token1 = {
        name: 'token1',
        identifier: '1',
        clientId: 'clientid-1',
        expires: Number.MAX_SAFE_INTEGER,
        lastUsedTime: null,
        scope: { '*': 'rw' },
        allowedIpRanges: '#this'
    };
    const token2 = {
        name: 'token2',
        identifier: '2',
        clientId: 'clientid-2',
        expires: Date.now(),
        lastUsedTime: null,
        scope: null, //{ '*': 'rw '}
        allowedIpRanges: '1.2.3.5'
    };

    it('can cleanupTokens', async function () {
        let result = await tokens.add(token1);
        token1.id = result.id;
        token1.accessToken = result.accessToken;

        result = await tokens.add(token2);
        token2.id = result.id;
        token2.accessToken = result.accessToken;

        await janitor.cleanupTokens();
    });

    it('did not remove the non-expired token', async function () {
        const result = await tokens.getByAccessToken(token1.accessToken);
        expect(result).to.be.eql(token1);
    });

    it('did remove the non-expired token', async function () {
        const result = await tokens.getByAccessToken(token2.accessToken);
        expect(result).to.be(null);
    });
});

