/* jslint node:true */
/* global it:false */
/* global describe:false */

'use strict';

const BoxError = require('../boxerror.js'),
    expect = require('expect.js'),
    path = require('path'),
    safe = require('safetydance'),
    shell = require('../shell.js')('test');

describe('shell', function () {
    describe('spawn', function () {
        it('can run valid program', async function () {
            await shell.spawn('ls', [ '-l' ], {});
        });

        it('fails on invalid program', async function () {
            const [error] = await safe(shell.spawn('randomprogram', [], {}));
            expect(error.reason).to.be(BoxError.SHELL_ERROR);
        });

        it('fails on failing program', async function () {
            const [error] = await safe(shell.spawn('/usr/bin/false', [], {}));
            expect(error.reason).to.be(BoxError.SHELL_ERROR);
        });
    });

    describe('maxLines', function () {
        it('maxLines=0 means unlimited', async function () {
            await shell.bash('for i in {1..10}; do echo $i; sleep 1; done', { encoding: 'utf8', maxLines: 0});
        });

        it('maxLines=2 kills the process (stdout)', async function () {
            const [error] = await safe(shell.bash('for i in {1..10}; do echo $i; sleep 1; done', { encoding: 'utf8', maxLines: 2}));
            expect(error).to.be.ok();
            expect(error.stdoutLineCount).to.be(2);
        });

        it('maxLines=2 kills the process (stderr)', async function () {
            const [error] = await safe(shell.bash('for i in {1..10}; do echo $i >&2; sleep 1; done', { encoding: 'utf8', maxLines: 2}));
            expect(error).to.be.ok();
            expect(error.stderrLineCount).to.be(2);
        });
    });

    describe('sudo', function () {
        it('cannot sudo invalid program', function (done) {
            shell.sudo([ 'randomprogram' ], {}, function (error) {
                expect(error).to.be.ok();
                done();
            });
        });

        it('can sudo valid program', function (done) {
            const RELOAD_NGINX_CMD = path.join(__dirname, '../src/scripts/restartservice.sh');
            shell.sudo([ RELOAD_NGINX_CMD, 'nginx' ], {}, function (error) {
                expect(error).to.be.ok();
                done();
            });
        });

        it('can run valid program (promises)', async function () {
            const RELOAD_NGINX_CMD = path.join(__dirname, '../src/scripts/restartservice.sh');
            await safe(shell.promises.sudo([ RELOAD_NGINX_CMD, 'nginx' ], {}));
        });
    });

    describe('spawn', function () {
        it('spawn throws for invalid program', async function () {
            const [error] = await safe(shell.spawn('cannotexist', [], {}));
            expect(error.reason).to.be(BoxError.SHELL_ERROR);
        });

        it('spawn throws for failed program', async function () {
            const [error] = await safe(shell.spawn('false', [], {}));
            expect(error.reason).to.be(BoxError.SHELL_ERROR);
        });

        it('spawn times out properly', async function () {
            const [error] = await safe(shell.spawn('sleep', ['20'], { timeout: 1000 }));
            expect(error.reason).to.be(BoxError.SHELL_ERROR);
        });
    });

    describe('bash', function () {
        it('can bash a shell program', async function () {
            const out = await shell.bash('ls -l | wc -c', {});
            expect(Buffer.isBuffer(out)).to.be(true);
        });

        it('can bash a shell program', async function () {
            const out = await shell.bash('ls -l | wc -c', { encoding: 'utf8' });
            expect(out).to.be.a('string');
        });
    });
});
