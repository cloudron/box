/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';


const child_process = require('child_process'),
    common = require('./common.js'),
    constants = require('../constants.js'),
    dockerProxy = require('../dockerproxy.js'),
    expect = require('expect.js'),
    nock = require('nock'),
    syslogServer = require('../../syslog.js');

const DOCKER = `docker -H tcp://172.18.0.1:${constants.DOCKER_PROXY_PORT} `;

async function exec(cmd) {
    return new Promise((resolve, reject) => {
        child_process.exec(cmd, { encoding: 'utf8' }, function (error, stdout) {
            if (error) return reject(error);

            resolve(stdout);
        });
    });
}

const BASE_IMAGE = 'registry.docker.com/cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4';

describe('Dockerproxy', function () {
    let containerId;
    const { setup, cleanup } = common;

    // create a container to test against
    before(async function () {
        if (nock.isActive()) nock.restore(); // required to connect to the docker socket
        await setup();
        await syslogServer.start();
        await dockerProxy.start();

        const stdout = await exec(`${DOCKER} run -d ${BASE_IMAGE} "bin/bash" "-c" "while true; do echo 'perpetual walrus'; sleep 1; done"`);
        containerId = stdout.slice(0, -1); // removes the trailing \n
    });

    after(async function () {
        await exec(`${DOCKER} rm -f ${containerId}`);
        await dockerProxy.stop();
        await syslogServer.stop();
        await cleanup();
    });

    // uncomment this to run the proxy for manual testing
    // it('wait', function (done) {} );

    it('can get info', async function () {
        const stdout = await exec(DOCKER + ' info');
        expect(stdout).to.contain('Containers:');
        // expect(stderr).to.be.empty(); // on some machines, i get 'No swap limit support'
    });

    it('can create container', async function () {
        const cmd = `${DOCKER} run ${BASE_IMAGE} "/bin/bash" "-c" "echo 'hello'"`;
        const stdout = await exec(cmd);
        expect(stdout).to.contain('hello');
    });

    it('proxy overwrites the container network option', async function () {
        const cmd = `${DOCKER} run --network ifnotrewritethiswouldfail ${BASE_IMAGE} "/bin/bash" "-c" "echo 'hello'"`;
        const stdout = await exec(cmd);
        expect(stdout).to.contain('hello');
    });

    xit('cannot see logs through docker logs, since syslog is configured', function (done) {
        exec(`${DOCKER} logs ${containerId}`, function (error, stdout, stderr) {
            expect(error.message).to.contain('configured logging driver does not support reading');
            expect(stderr).to.contain('configured logging driver does not support reading');
            expect(stdout).to.be.empty();

            done();
        });
    });

    it('can use PUT to upload archive into a container', async function () {
        const stdout = await exec(`${DOCKER} cp ${__dirname}/proxytestarchive.tar ${containerId}:/tmp/`);
        expect(stdout).to.be.empty();
    });

    it('can exec into a container', async function () {
        const stdout = await exec(`${DOCKER} exec ${containerId} ls`);
        expect(stdout).to.equal('bin\nboot\ndev\netc\nhome\nlib\nlib32\nlib64\nlibx32\nmedia\nmnt\nopt\nproc\nroot\nrun\nsbin\nsrv\nsys\ntmp\nusr\nvar\n');
    });
});
