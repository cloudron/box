/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    safe = require('safetydance'),
    updater = require('../updater.js');

describe('updater', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    it('can get default autoupdate_pattern', async function () {
        const pattern = await updater.getAutoupdatePattern();
        expect(pattern).to.be('00 00 1,3,5,23 * * *');
    });

    it('cannot set invalid autoupdate_pattern', async function () {
        const [error] = await safe(updater.setAutoupdatePattern('02 * 1 *'));
        expect(error.reason).to.be(BoxError.BAD_FIELD);
    });

    it('can set default autoupdate_pattern', async function () {
        await updater.setAutoupdatePattern('02 * 1-5 * * *');
    });
});
