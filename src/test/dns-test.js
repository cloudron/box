/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    dns = require('../dns.js'),
    expect = require('expect.js');

describe('DNS', function () {
    const { setup, cleanup, app, domain:domainObject } = common;

    const domain = domainObject.domain;

    before(setup);
    after(cleanup);

    describe('validateHostname', function () {
        it('cannot have >63 length subdomains', function () {
            const s = Array(64).fill('s').join('');
            expect(dns.validateHostname(s, domain)).to.be.an(Error);
            expect(dns.validateHostname(`dev.${s}`, domain)).to.be.an(Error);
        });

        it('allows only alphanumerics and hypen', function () {
            expect(dns.validateHostname('#2r',   domain)).to.be.an(Error);
            expect(dns.validateHostname('a%b',   domain)).to.be.an(Error);
            expect(dns.validateHostname('ab_',   domain)).to.be.an(Error);
            expect(dns.validateHostname('ab.',   domain)).to.be.an(Error);
            expect(dns.validateHostname('ab..c', domain)).to.be.an(Error);
            expect(dns.validateHostname('.ab',   domain)).to.be.an(Error);
            expect(dns.validateHostname('-ab',   domain)).to.be.an(Error);
            expect(dns.validateHostname('ab-',   domain)).to.be.an(Error);
        });

        it('total length cannot exceed 255', function () {
            let s = '';
            for (let i = 0; i < (255 - 'example.com'.length); i++) s += 's';

            expect(dns.validateHostname(s, domain)).to.be.an(Error);
        });

        it('allow valid domains', function () {
            expect(dns.validateHostname('a',        domain)).to.be(null);
            expect(dns.validateHostname('a0-x',     domain)).to.be(null);
            expect(dns.validateHostname('a0.x',     domain)).to.be(null);
            expect(dns.validateHostname('a0.x.y',   domain)).to.be(null);
            expect(dns.validateHostname('01',       domain)).to.be(null);
        });
    });

    describe('getName', function () {
        it('works with zoneName==domain', function () {
            const d = {
                domain: 'example.com',
                zoneName: 'example.com',
                config: {}
            };

            expect(dns.getName(d, '', 'A')).to.be('');
            expect(dns.getName(d, 'www', 'A')).to.be('www');
            expect(dns.getName(d, 'www.dev', 'A')).to.be('www.dev');

            expect(dns.getName(d, '', 'MX')).to.be('');

            expect(dns.getName(d, '', 'TXT')).to.be('');
            expect(dns.getName(d, 'www', 'TXT')).to.be('www');
            expect(dns.getName(d, 'www.dev', 'TXT')).to.be('www.dev');
        });

        it('works when zoneName!=domain', function () {
            const d = {
                domain: 'dev.example.com',
                zoneName: 'example.com',
                config: {}
            };

            expect(dns.getName(d, '', 'A')).to.be('dev');
            expect(dns.getName(d, 'www', 'A')).to.be('www.dev');
            expect(dns.getName(d, 'www.dev', 'A')).to.be('www.dev.dev');

            expect(dns.getName(d, '', 'MX')).to.be('dev');

            expect(dns.getName(d, '', 'TXT')).to.be('dev');
            expect(dns.getName(d, 'www', 'TXT')).to.be('www.dev');
            expect(dns.getName(d, 'www.dev', 'TXT')).to.be('www.dev.dev');
        });
    });

    describe('register', function () {
        it('registers subdomain', async function () {
            await dns.registerLocations([ { subdomain: app.subdomain, domain: app.domain } ], { overwriteDns: true }, (/*progress*/) => {});
        });

        it('unregisters subdomain', async function () {
            await dns.unregisterLocations([ { subdomain: app.subdomain, domain: app.domain } ], (/*progress*/) => {});
        });
    });
});
