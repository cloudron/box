/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    database = require('../database.js'),
    eventlog = require('../eventlog.js'),
    expect = require('expect.js'),
    notifications = require('../notifications.js'),
    timers = require('timers/promises');

describe('Eventlog', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    let eventId;

    it('clear', async function () {
        await eventlog._clear();
    });

    it('add succeeds', async function () {
        const id = await eventlog.add('some.event', { ip: '1.2.3.4' }, { appId: 'thatapp' });
        expect(id).to.be.a('string');

        eventId = id;
    });

    it('get succeeds', async function () {
        const result = await eventlog.get(eventId);
        expect(result.id).to.be(eventId);
        expect(result.action).to.be('some.event');
        expect(result.creationTime).to.be.a(Date);

        expect(result.source).to.be.eql({ ip: '1.2.3.4' });
        expect(result.data).to.be.eql({ appId: 'thatapp' });
    });

    it('get of unknown id fails', async function () {
        const result = await eventlog.get('notfoundid');
        expect(result).to.be(null);
    });

    it('listPaged succeeds', async function () {
        const results = await eventlog.listPaged([], null, 1, 1);
        expect(results).to.be.an(Array);
        expect(results.length).to.be(1);

        expect(results[0].id).to.be(eventId);
        expect(results[0].action).to.be('some.event');
        expect(results[0].source).to.be.eql({ ip: '1.2.3.4' });
        expect(results[0].data).to.be.eql({ appId: 'thatapp' });
    });

    it('listPaged succeeds with source search', async function () {
        const results = await eventlog.listPaged([], '1.2.3.4', 1, 1);
        expect(results).to.be.an(Array);
        expect(results.length).to.be(1);
        expect(results[0].id).to.be(eventId);
        expect(results[0].action).to.be('some.event');
        expect(results[0].source).to.be.eql({ ip: '1.2.3.4' });
        expect(results[0].data).to.be.eql({ appId: 'thatapp' });
    });

    it('listPaged succeeds with data search', async function () {
        const results = await eventlog.listPaged([], 'thatapp', 1, 1);
        expect(results).to.be.an(Array);
        expect(results.length).to.be(1);
        expect(results[0].id).to.be(eventId);
        expect(results[0].action).to.be('some.event');
        expect(results[0].source).to.be.eql({ ip: '1.2.3.4' });
        expect(results[0].data).to.be.eql({ appId: 'thatapp' });
    });

    let loginEventId;
    it('upsert with no existing entry succeeds', async function () {
        const result = await eventlog.upsertLoginEvent('user.login', { ip: '1.2.3.4' }, { appId: 'thatapp' });
        expect(result).to.be.a('string');
        loginEventId = result;
    });

    it('upsert with existing entry succeeds', async function () {
        let result = await eventlog.get(loginEventId);
        const oldCreationTime = result.creationTime;

        await timers.setTimeout(2000);
        result = await eventlog.upsertLoginEvent('user.login', { ip: '1.2.3.4' }, { appId: 'thatapp' });
        expect(result).to.equal(loginEventId);

        result = await eventlog.get(loginEventId);
        // should have changed
        expect(oldCreationTime).to.not.equal(result.creationTime);
    });

    it('upsert with existing old entry succeeds', async function () {
        let yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);

        await database.query('INSERT INTO eventlog (id, action, sourceJson, dataJson, creationTime) VALUES (?, ?, ?, ?, ?)', [ 'anotherid', 'user.login2', JSON.stringify({ ip: '1.2.3.4' }), JSON.stringify({ appId: 'thatapp' }), yesterday ]);

        const result = await eventlog.upsertLoginEvent('user.login2', { ip: '1.2.3.4' }, { appId: 'thatapp' });
        expect(result).to.not.equal('anotherid');
    });

    it('cleans up', async function () {
        await eventlog._clear();

        for (const e of [ eventlog.ACTION_USER_LOGIN, eventlog.ACTION_USER_LOGIN_GHOST, eventlog.ACTION_USER_LOGOUT, eventlog.ACTION_USER_LOGIN ]) {
            const eventId = await eventlog.add(e, { ip: '1.2.3.4' }, { appId: 'thatapp' });

            await notifications._add(notifications.TYPE_APP_UPDATED, 'title', 'some message', { eventId });
        }

        await timers.setTimeout(3000);

        const id = await eventlog.add(eventlog.ACTION_USER_LOGIN, { ip: '1.2.3.4' }, { appId: 'thatapp' });

        await eventlog.cleanup({ creationTime: new Date(Date.now() - 1000) }); // 1 second ago
        let results = await eventlog.listPaged([], null, 1, 100);
        expect(results.length).to.be(1);

        expect(results[0].id).to.be(id);
        expect(results[0].action).to.be(eventlog.ACTION_USER_LOGIN);
        expect(results[0].creationTime).to.be.a(Date);

        results = await notifications.list({}, 1, 10);
        expect(results.length).to.be(0);
    });
});
