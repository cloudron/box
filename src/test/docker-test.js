/* jslint node:true */
/* global it:false */
/* global before:false */
/* global after:false */
/* global describe:false */

'use strict';

const common = require('./common.js'),
    docker = require('../docker.js'),
    expect = require('expect.js'),
    nock = require('nock');

describe('docker', function () {
    const { setup, cleanup } = common;

    before(async function () {
        if (nock.isActive()) nock.restore(); // required to connect to the docker socket
        await setup();
    });

    after(cleanup);

    it('can df', async function () {
        const output = await docker.df();
        expect(output).to.be.ok();
    });

    it('parseImageRef', async function () {
        expect(docker.parseImageRef('cloudron/base')).to.eql({ fullRepositoryName: 'cloudron/base', registry: null, tag: null, digest: null });
        expect(docker.parseImageRef('cloudron/base:4.2.0')).to.eql({ fullRepositoryName: 'cloudron/base', registry: null, tag: '4.2.0', digest: null });
        expect(docker.parseImageRef('cloudron/base@sha256:xx')).to.eql({ fullRepositoryName: 'cloudron/base', registry: null, tag: null, digest: 'xx' });
        expect(docker.parseImageRef('cloudron/base:4.2.0@sha256:xx')).to.eql({ fullRepositoryName: 'cloudron/base', registry: null, tag: '4.2.0', digest: 'xx' });
        expect(docker.parseImageRef('registry.com/cloudron/base:4.2.0@sha256:xx')).to.eql({ fullRepositoryName: 'registry.com/cloudron/base', registry: 'registry.com', tag: '4.2.0', digest: 'xx' });
        expect(docker.parseImageRef('registry.com/base:4.2.0@sha256:xx')).to.eql({ fullRepositoryName: 'registry.com/base', registry: 'registry.com', tag: '4.2.0', digest: 'xx' }); // optional namespace
    });
});
