/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */
/* global beforeEach:false */

'use strict';

const appstore = require('../appstore.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    nock = require('nock');

const APP_ID = 'appid';
const APPSTORE_APP_ID = 'appstoreappid';

describe('Appstore', function () {
    const { setup, cleanup, appstoreToken, mockApiServerOrigin } = common;

    before(setup);
    before(() => { if (!nock.isActive()) nock.activate(); });
    after(cleanup);

    beforeEach(nock.cleanAll);

    it('can purchase an app', async function () {
        const scope1 = nock(mockApiServerOrigin)
            .post(`/api/v1/cloudronapps?accessToken=${appstoreToken}`, function () { return true; })
            .reply(201, {});

        await appstore.purchaseApp({ appId: APP_ID, appstoreId: APPSTORE_APP_ID, manifestId: APPSTORE_APP_ID });
        expect(scope1.isDone()).to.be.ok();
    });

    it('unpurchase succeeds if app was never purchased', async function () {
        const scope1 = nock(mockApiServerOrigin)
            .get(`/api/v1/cloudronapps/${APP_ID}?accessToken=${appstoreToken}`)
            .reply(404, {});

        const scope2 = nock(mockApiServerOrigin)
            .delete(`/api/v1/cloudronapps/${APP_ID}?accessToken=${appstoreToken}`, function () { return true; })
            .reply(204, {});

        await appstore.unpurchaseApp(APP_ID, { appstoreId: APPSTORE_APP_ID, manifestId: APPSTORE_APP_ID });
        expect(scope1.isDone()).to.be.ok();
        expect(scope2.isDone()).to.not.be.ok();
    });

    it('can unpurchase an app', async function () {
        const scope1 = nock(mockApiServerOrigin)
            .get(`/api/v1/cloudronapps/${APP_ID}?accessToken=${appstoreToken}`)
            .reply(200, {});

        const scope2 = nock(mockApiServerOrigin)
            .delete(`/api/v1/cloudronapps/${APP_ID}?accessToken=${appstoreToken}`, function () { return true; })
            .reply(204, {});

        await appstore.unpurchaseApp(APP_ID, { appstoreId: APPSTORE_APP_ID, manifestId: APPSTORE_APP_ID });
        expect(scope1.isDone()).to.be.ok();
        expect(scope2.isDone()).to.be.ok();
    });
});
