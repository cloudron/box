/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    notifications = require('../notifications.js'),
    safe = require('safetydance'),
    timers = require('timers/promises');

const EVENT_0 = {
    id: 'event_0',
    action: 'action',
    source: {},
    data: {}
};

describe('Notifications', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    const notificationIds = [];

    it('can add notifications', async function () {
        for (let i = 0; i < 3; i++) {
            const [error, id] = await safe(notifications._add(notifications.TYPE_APP_UPDATED, `title ${i}`, `message ${i}`, { eventId: EVENT_0.id }));
            expect(error).to.equal(null);
            expect(id).to.be.a('string');
            notificationIds.push(id);
            await timers.setTimeout(1000);
        }
    });

    it('can get by id', async function () {
        const [error, result] = await safe(notifications.get(notificationIds[0]));
        expect(error).to.be(null);
        expect(result.title).to.be('title 0');
        expect(result.type).to.be(notifications.TYPE_APP_UPDATED);
        expect(result.message).to.be('message 0');
        expect(result.acknowledged).to.be(false);
    });

    it('cannot get non-existent id', async function () {
        const result = await notifications.get('random');
        expect(result).to.be(null);
    });

    it('can list notifications', async function () {
        const result = await notifications.list({}, 1, 10);
        expect(result.length).to.be(3);
        expect(result[0].title).to.be('title 2');
        expect(result[1].title).to.be('title 1');
        expect(result[2].title).to.be('title 0');
    });

    it('can update notification', async function () {
        await notifications.update({ id: notificationIds[0] }, { type: notifications.TYPE_APP_OOM, title: 'updated title 0', message: 'updated message 0', acknowledged: true });

        const result = await notifications.get(notificationIds[0]);
        expect(result.title).to.be('updated title 0');
        expect(result.type).to.be(notifications.TYPE_APP_OOM);
        expect(result.message).to.be('updated message 0');
        expect(result.acknowledged).to.be(true);
    });

    it('cannot update non-existent notification', async function () {
        const [error] = await safe(notifications.update({ id: '1245' }, { title: 'updated title 0', message: 'updated message 0', acknowledged: true }));
        expect(error.reason).to.be(BoxError.NOT_FOUND);

    });

    it('can delete', async function () {
        await notifications.del(notificationIds[0]);
    });

    it('cannot delete non-existent notification', async function () {
        const [error] = await safe(notifications.del('5213'));
        expect(error.reason).to.be(BoxError.NOT_FOUND);
    });

    let pinId;
    describe('pin with context', function () {
        it('can add pin', async function () {
            pinId = await notifications.pin(notifications.TYPE_BOX_UPDATE, 'Cloudron xx is available', 'Awesome changelog', { context: 'xx' });

            const result = await notifications.get(pinId);
            expect(result.title).to.be('Cloudron xx is available');
            expect(result.message).to.be('Awesome changelog');
            expect(result.acknowledged).to.be(false);
        });

        it('updating pin with same message does nothing', async function () {
            await notifications.update({ id: pinId }, { acknowledged: true }); // ack the alert

            const id = await notifications.pin(notifications.TYPE_BOX_UPDATE, 'Cloudron xx is available', 'Awesome changelog', { context: 'xx' });
            expect(id).to.be(pinId);

            const result = await notifications.get(pinId);
            expect(result.title).to.be('Cloudron xx is available');
            expect(result.message).to.be('Awesome changelog');
            expect(result.acknowledged).to.be(true); // notification does not resurface
        });

        it('updating pin with new message resurfaces', async function () {
            await notifications.update({ id: pinId }, { acknowledged: true }); // ack the alert

            const id = await notifications.pin(notifications.TYPE_BOX_UPDATE, 'Cloudron xy is available', 'Awesome new changelog', { context: 'xx' });
            expect(id).to.be(pinId);

            const result = await notifications.get(pinId);
            expect(result.title).to.be('Cloudron xy is available');
            expect(result.message).to.be('Awesome new changelog');
            expect(result.acknowledged).to.be(false); // notification resurfaces
        });

        it('can unpin', async function () {
            await notifications.unpin(notifications.TYPE_BOX_UPDATE, { context: 'xx' });

            const result = await notifications.get(pinId);
            expect(result.acknowledged).to.be(true);
        });
    });

    describe('pin without context', function () {
        it('can add pin', async function () {
            pinId = await notifications.pin(notifications.TYPE_REBOOT, 'Reboot required', 'Do it now', {});

            const result = await notifications.get(pinId);
            expect(result.title).to.be('Reboot required');
            expect(result.message).to.be('Do it now');
            expect(result.acknowledged).to.be(false);
        });

        it('can update the alert', async function () {
            await notifications.update({ id: pinId }, { acknowledged: true }); // ack the alert

            const id = await notifications.pin(notifications.TYPE_REBOOT, 'Reboot required', 'Do it now', {});
            expect(id).to.be(pinId);

            const result = await notifications.get(pinId);
            expect(result.title).to.be('Reboot required');
            expect(result.message).to.be('Do it now');
            expect(result.acknowledged).to.be(false); // resurfaces
        });

        it('can unpin', async function () {
            await notifications.unpin(notifications.TYPE_REBOOT, {});

            const result = await notifications.get(pinId);
            expect(result.acknowledged).to.be(true);
        });
    });
});
