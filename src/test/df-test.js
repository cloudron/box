/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    df = require('../df.js'),
    expect = require('expect.js');

describe('df', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    it('can get filesystems', async function () {
        // does not work on archlinux 8!
        if (require('child_process').execSync('uname -a').toString().indexOf('-arch') !== -1) return;

        const disks = await df.filesystems();
        expect(disks).to.be.ok();
        expect(disks.some(d => d.mountpoint === '/')).to.be.ok();
    });

    it('can get file', async function () {
        // does not work on archlinux 8!
        if (require('child_process').execSync('uname -a').toString().indexOf('-arch') !== -1) return;

        const disks = await df.file(__dirname);
        expect(disks).to.be.ok();
        expect(disks.mountpoint).to.be('/home');
    });
});
