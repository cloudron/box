/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const appPasswords = require('../apppasswords.js'),
    BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    safe = require('safetydance'),
    users = require('../users.js');

describe('App passwords', function () {
    const { setup, cleanup, admin } = common;

    before(setup);
    after(cleanup);

    let id, password;
    it('cannot add bad app password', async function () {
        const [error] = await safe(appPasswords.add(admin.id, 'appid', 'x'.repeat(201)));
        expect(error.reason).to.be(BoxError.BAD_FIELD);
    });

    it('can add app password', async function () {
        const result = await appPasswords.add(admin.id, 'appid', 'spark');
        expect(result.id).to.be.a('string');
        expect(result.password).to.be.a('string');
        id = result.id;
        password = result.password;
    });

    it('can get app password', async function () {
        const result = await appPasswords.get(id);
        expect(result.hashedPassword).to.be.a('string');
        expect(result.name).to.be('spark');
        expect(result.identifier).to.be('appid');
    });

    it('cannot get random app password', async function () {
        const result = await appPasswords.get('random');
        expect(result).to.be(null);
    });

    it('can get app passwords', async function () {
        const results = await appPasswords.list(admin.id);
        expect(results.length).to.be(1);
        expect(results[0].hashedPassword).to.be.a('string');
        expect(results[0].name).to.be('spark');
        expect(results[0].identifier).to.be('appid');
    });

    it('can verify app password', async function () {
        const result = await users.verify(admin.id, password, 'appid', {});
        expect(result).to.be.ok();
        expect(result.appPassword).to.be(true);
    });

    it('can verify non-app password', async function () {
        const result = await users.verify(admin.id, admin.password, 'appid', {});
        expect(result).to.be.ok();
        expect(result.appPassword).to.be(undefined);
    });

    it('cannot verify bad password', async function () {
        const [error, result] = await safe(users.verify(admin.id, 'bad', 'appid', {}));
        expect(result).to.not.be.ok();
        expect(error.reason).to.be(BoxError.INVALID_CREDENTIALS);
    });

    it('cannot verify password for another app', async function () {
        const [error, result] = await safe(users.verify(admin.id, password, 'appid2', {}));
        expect(result).to.not.be.ok();
        expect(error.reason).to.be(BoxError.INVALID_CREDENTIALS);
    });

    it('can del app password', async function () {
        await appPasswords.del(id);
    });

    it('cannot verify deleted app password', async function () {
        const [error] = await safe(users.verify(admin.id, password, 'appid', {}));
        expect(error.reason).to.be(BoxError.INVALID_CREDENTIALS);
    });

    it('cannot del random app password', async function () {
        const [error] = await safe(appPasswords.del('random'));
        expect(error.reason).to.be(BoxError.NOT_FOUND);
    });
});
