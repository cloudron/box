/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const addonConfigs = require('../addonconfigs.js'),
    async = require('async'),
    common = require('./common.js'),
    constants = require('../constants.js'),
    expect = require('expect.js'),
    groups = require('../groups.js'),
    ldap = require('ldapjs'),
    ldapServer = require('../ldapserver.js'),
    mail = require('../mail.js'),
    safe = require('safetydance');

async function ldapBind(dn, password) {
    return new Promise((resolve, reject) => {
        const client = ldap.createClient({ url: 'ldap://127.0.0.1:' + constants.LDAP_PORT });

        client.on('error', reject);

        client.bind(dn, password, function (error) {
            client.unbind();

            if (error) reject(error);
            resolve();
        });
    });
}

// ldapsearch -LLL -E pr=10/noprompt -x -h localhost -p 3002 -b cn=userName0@example.com,ou=mailboxes,dc=cloudron objectclass=mailbox
async function ldapSearch(dn, opts) {
    return new Promise((resolve, reject) => {
        const client = ldap.createClient({ url: 'ldap://127.0.0.1:' + constants.LDAP_PORT });

        function done(error, result) {
            client.unbind();
            if (error) reject(error); else resolve(result);
        }

        client.search(dn, opts, function (error, result) {
            if (error) return done(error);

            let entries = [];

            result.on('searchEntry', function (entry) { entries.push(entry.object); });

            result.on('error', done);

            result.on('end', function (result) {
                if (result.status !== 0) return done(new Error(`Unexpected status: ${result.status}`));
                done(null, entries);
            });
        });
    });
}

describe('Ldap Server', function () {
    const { setup, cleanup, admin, user, app, domain, auditSource } = common;
    let group, group2;
    const mockApp = Object.assign({}, app);

    const mailboxName = 'support';
    const mailbox = `support@${domain.domain}`;
    const mailAliasName = 'alsosupport';
    const mailAlias = `alsosupport@${domain.domain}`;
    const mailAliasWildcardName = 'help';
    const mailAliasWildcard = `helpmeplz@${domain.domain}`;

    before(function (done) {
        async.series([
            setup,
            async () => await mail.addMailbox(mailboxName, domain.domain, { ownerId: user.id, ownerType: mail.OWNERTYPE_USER, active: true, storageQuota: 0, messagesQuota: 0 }, auditSource),
            async () => await mail.setAliases(mailboxName, domain.domain, [ { name: mailAliasName, domain: domain.domain}, { name: mailAliasWildcardName + '*', domain: domain.domain } ], auditSource),
            ldapServer.start.bind(null),
            async () => {
                group = await groups.add({ name: 'ldap-test-1' }, auditSource);
                await groups.setMembers(group, [ admin.id, user.id ], {}, auditSource);
            },
            async () => {
                group2 = await groups.add({ name: 'ldap-test-2' }, auditSource);
                await groups.setMembers(group2, [ admin.id ], {}, auditSource);
            }
        ], done);

        ldapServer._MOCK_APP = mockApp;
    });

    after(function (done) {
        async.series([
            ldapServer.stop,
            cleanup
        ], done);
    });

    describe('root DSE', function () {
        it('can get root DSE', async function () {
            const entries = await ldapSearch('', { filter: 'objectcategory=person' });
            expect(entries.length).to.be(1);
            expect(entries[0].dn).to.be('');
            expect(entries[0].supportedLDAPVersion).to.be('3');
            expect(entries[0].vendorName).to.be('Cloudron LDAP');
            expect(entries[0].vendorVersion).to.be('1.0.0');
        });
    });

    describe('admin bind', function () {
        it('cn= fails for nonexisting user', async function () {
            const [error] = await safe(ldapBind('cn=doesnotexist,ou=users,dc=cloudron', 'password'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('cn= fails with wrong password', async function () {
            const [error] = await safe(ldapBind(`cn=${admin.id},ou=users,dc=cloudron`, 'wrongpassword'));
            expect(error).to.be.a(ldap.InvalidCredentialsError);
        });

        it('cn= succeeds with id', async function () {
            await ldapBind(`cn=${admin.id},ou=users,dc=cloudron`, admin.password);
        });

        it('cn= succeeds with username', async function () {
            await ldapBind(`cn=${admin.username},ou=users,dc=cloudron`, admin.password);
        });

        it('cn= succeeds with email', async function () {
            await ldapBind(`cn=${admin.email},ou=users,dc=cloudron`, admin.password);
        });

        it('mail= fails with bad email', async function () {
            const [error] = await safe(ldapBind('mail=random,ou=users,dc=cloudron', admin.password));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('mail= succeeds with email', async function () {
            await ldapBind(`mail=${admin.email},ou=users,dc=cloudron`, admin.password);
        });
    });

    describe('non-admin bind', function () {
        it('succeeds with null accessRestriction', async function () {
            mockApp.accessRestriction = null;
            await ldapBind(`cn=${user.id},ou=users,dc=cloudron`, user.password);
        });

        it('fails without accessRestriction', async function () {
            mockApp.accessRestriction = { users: [], groups: [] };
            const [error] = await safe(ldapBind(`cn=${user.id},ou=users,dc=cloudron`, user.password));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('succeeds with accessRestriction', async function () {
            mockApp.accessRestriction = { users: [ user.id ], groups: [] };
            await ldapBind(`cn=${user.id},ou=users,dc=cloudron`, user.password);
        });
    });

    describe('search users', function () {
        it('fails for non existing tree', async function () {
            const [error] = await safe(ldapSearch('o=example', { filter: '(&(l=Seattle)(email=*@' + domain.domain + '))' }));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('succeeds with basic filter', async function () {
            const entries = await ldapSearch('ou=users,dc=cloudron', { filter: 'objectcategory=person' });
            expect(entries.length).to.equal(2);
            entries.sort(function (a, b) { return a.username > b.username; });
            expect(entries[0].username).to.equal(admin.username.toLowerCase());
            expect(entries[0].mail).to.equal(admin.email.toLowerCase());
            expect(entries[1].username).to.equal(user.username.toLowerCase());
            expect(entries[1].mail).to.equal(user.email.toLowerCase());
        });

        it('succeeds with pagination', async function () {
            const entries = await ldapSearch('ou=users,dc=cloudron', { filter: 'objectcategory=person', paged: true });
            expect(entries.length).to.equal(2);
            entries.sort(function (a, b) { return a.username > b.username; });
            expect(entries[0].username).to.equal(admin.username.toLowerCase());
            expect(entries[0].mail).to.equal(admin.email.toLowerCase());
            expect(entries[1].username).to.equal(user.username.toLowerCase());
            expect(entries[1].mail).to.equal(user.email.toLowerCase());
        });

        it('succeeds with username wildcard filter', async function () {
            const entries = await ldapSearch('ou=users,dc=cloudron', { filter: '&(objectcategory=person)(username=*)' });
            expect(entries.length).to.equal(2);
            entries.sort(function (a, b) { return a.username > b.username; });
            expect(entries[0].username).to.equal(admin.username.toLowerCase());
            expect(entries[1].username).to.equal(user.username.toLowerCase());
        });

        it('succeeds with username filter', async function () {
            const entries = await ldapSearch('ou=users,dc=cloudron', { filter: '&(objectcategory=person)(username=' + admin.username + ')' });
            expect(entries.length).to.equal(1);
            expect(entries[0].username).to.equal(admin.username.toLowerCase());
            expect(entries[0].memberof.length).to.equal(2);
        });

        it('can always lists admins', async function () {
            mockApp.accessRestriction = { users: [], groups: [] };
            const entries = await ldapSearch('ou=users,dc=cloudron', { filter: 'objectcategory=person' });
            expect(entries.length).to.equal(1);
            expect(entries[0].username).to.equal(admin.username.toLowerCase());
            expect(entries[0].memberof.length).to.equal(2);
        });

        it ('does only list users who have access', async function () {
            mockApp.accessRestriction = { users: [], groups: [ group.id ] };
            const entries = await ldapSearch('ou=users,dc=cloudron', { filter: 'objectcategory=person' });
            expect(entries.length).to.equal(2);
            entries.sort(function (a, b) { return a.username > b.username; });

            expect(entries[0].username).to.equal(admin.username.toLowerCase());
            expect(entries[1].username).to.equal(user.username.toLowerCase());
        });
    });

    describe('group search', function () {
        it('succeeds with basic filter', async function () {
            mockApp.accessRestriction = null;

            const entries = await ldapSearch('ou=groups,dc=cloudron', { filter: 'objectclass=group' });
            expect(entries.length).to.equal(2);

            // ensure order for testability
            entries.sort(function (a, b) { return a.cn < b.cn; });

            expect(entries[0].cn).to.equal('ldap-test-1');
            expect(entries[0].memberuid.length).to.equal(2);
            expect(entries[0].memberuid).to.contain(admin.id);
            expect(entries[0].memberuid).to.contain(user.id);

            expect(entries[1].cn).to.equal('ldap-test-2');
            expect(entries[1].memberuid).to.equal(admin.id);
        });

        it ('succeeds with cn wildcard filter', async function () {
            const entries = await ldapSearch('ou=groups,dc=cloudron', { filter: '&(objectclass=group)(cn=*)' });
            expect(entries.length).to.equal(2);

            // ensure order for testability
            entries.sort(function (a, b) { return a.cn < b.cn; });

            expect(entries[0].cn).to.equal('ldap-test-1');
            expect(entries[0].memberuid.length).to.equal(2);
            expect(entries[0].memberuid).to.contain(admin.id);
            expect(entries[0].memberuid).to.contain(user.id);

            expect(entries[1].cn).to.equal('ldap-test-2');
            expect(entries[1].memberuid).to.equal(admin.id);
        });

        it('succeeds with memberuid filter', async function () {
            const entries = await ldapSearch('ou=groups,dc=cloudron', { filter: '&(objectclass=group)(memberuid=' + user.id + ')' });
            expect(entries.length).to.equal(1);

            // ensure order for testability
            entries.sort(function (a, b) { return a.cn < b.cn; });

            expect(entries[0].cn).to.equal('ldap-test-1');
            expect(entries[0].memberuid.length).to.equal(2);
            expect(entries[0].memberuid).to.contain(admin.id);
            expect(entries[0].memberuid).to.contain(user.id);
        });

        it ('does only list groups who have access', async function () {
            mockApp.accessRestriction = { users: [], groups: [ group.id ] };
            const entries = await ldapSearch('ou=groups,dc=cloudron', { filter: '&(objectclass=group)(cn=*)' });
            expect(entries.length).to.equal(1);

            // ensure order for testability
            entries.sort(function (a, b) { return a.cn < b.cn; });

            expect(entries[0].cn).to.equal('ldap-test-1');
            expect(entries[0].memberuid.length).to.equal(2);
            expect(entries[0].memberuid).to.contain(admin.id);
            expect(entries[0].memberuid).to.contain(user.id);
        });

        it ('succeeds with pagination', async function () {
            mockApp.accessRestriction = null;
            const entries = await ldapSearch('ou=groups,dc=cloudron', { filter: 'objectclass=group', paged: true });
            expect(entries.length).to.equal(2);

            // ensure order for testability
            entries.sort(function (a, b) { return a.cn < b.cn; });

            expect(entries[0].cn).to.equal('ldap-test-1');
            expect(entries[0].memberuid.length).to.equal(2);
            expect(entries[0].memberuid).to.contain(admin.id);
            expect(entries[0].memberuid).to.contain(user.id);

            expect(entries[1].cn).to.equal('ldap-test-2');
            expect(entries[1].memberuid).to.equal(admin.id);
        });
    });

    describe('mailbox search', function () {
        it('get specific mailbox by email', async function () {
            const entries = await ldapSearch(`cn=${mailbox},ou=mailboxes,dc=cloudron`, 'objectclass=mailbox');
            expect(entries.length).to.equal(1);
            expect(entries[0].cn).to.equal(mailbox);
        });

        it('cannot get mailbox with just name', async function () {
            const [error] = await safe(ldapSearch(`cn=${mailboxName},ou=mailboxes,dc=cloudron`, 'objectclass=mailbox'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('cannot get alias as a mailbox', async function () {
            const [error] = await safe(ldapSearch(`cn=${mailAlias},ou=mailboxes,dc=cloudron`, 'objectclass=mailbox'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('non-existent mailbox', async function () {
            const [error] = await safe(ldapSearch(`cn=random@${domain.domain},ou=mailboxes,dc=cloudron`, 'objectclass=mailbox'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('cannot get inactive mailbox', async function () {
            await mail.updateMailbox(mailboxName, domain.domain, { ownerId: user.id, ownerType: mail.OWNERTYPE_USER, active: false, enablePop3: false }, auditSource);
            const [error] = await safe(ldapSearch(`cn=${mailbox},ou=mailboxes,dc=cloudron`, 'objectclass=mailbox'));
            expect(error).to.be.a(ldap.NoSuchObjectError);

            await mail.updateMailbox(mailboxName, domain.domain, { ownerId: user.id, ownerType: mail.OWNERTYPE_USER, active: true, enablePop3: false }, auditSource);
        });
    });

    describe('search aliases', function () {
        it('get specific alias', async function () {
            const entries = await ldapSearch(`cn=${mailAlias},ou=mailaliases,dc=cloudron`, 'objectclass=nismailalias');
            expect(entries.length).to.equal(1);
            expect(entries[0].cn).to.equal(mailAlias);
            expect(entries[0].rfc822MailMember).to.equal(mailbox);
        });

        it('get alias matching wildcard', async function () {
            const entries = await ldapSearch(`cn=${mailAliasWildcard},ou=mailaliases,dc=cloudron`, 'objectclass=nismailalias');
            expect(entries.length).to.equal(1);
            expect(entries[0].cn).to.equal(mailAliasWildcard);
            expect(entries[0].rfc822MailMember).to.equal(mailbox);
        });

        it('cannot get mailbox as alias', async function () {
            const [error] = await safe(ldapSearch(`cn=${mailbox},ou=mailaliases,dc=cloudron`, 'objectclass=nismailalias'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('non-existent alias', async function () {
            const [error] = await safe(ldapSearch(`cn=random@${domain.domain},ou=mailaliases,dc=cloudron`, 'objectclass=mailbox'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });
    });

    describe('search mailing list', function () {
        const LIST_NAME = 'devs', LIST = `devs@${domain.domain}`;

        before(async function () {
            await mail.addList(LIST_NAME, domain.domain, { members: [ mailbox , 'outsider@external.com' ], membersOnly: false, active: true }, auditSource);
        });

        it('get specific list', async function () {
            const entries = await ldapSearch(`cn=${LIST},ou=mailinglists,dc=cloudron`, 'objectclass=mailGroup');
            expect(entries.length).to.equal(1);
            expect(entries[0].cn).to.equal(LIST);
            expect(entries[0].mgrpRFC822MailMember).to.eql([ mailbox, 'outsider@external.com' ]);
        });

        it('non-existent list', async function () {
            const [error] = await safe(ldapSearch('cn=random@example.com,ou=mailinglists,dc=cloudron', 'objectclass=mailGroup'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('inactive list', async function () {
            await mail.updateList(LIST_NAME, domain.domain, { members: [ mailbox , 'outsider@external.com' ], membersOnly: false, active: false }, auditSource);
            const [error] = await safe(ldapSearch('cn=devs@example.com,ou=mailinglists,dc=cloudron', 'objectclass=mailGroup'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });
    });

    describe('sogo mailbox bind', function () {
        it('email disabled - cannot auth', async function () {
            const [error] = await safe(ldapBind(`cn=${mailbox},domain=example.com,ou=mailboxes,dc=cloudron`, 'badpassword'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('email enabled - does not allow with invalid password', async function () {
            await mail._updateDomain(domain.domain, { enabled: true });
            const [error] = await safe(ldapBind(`cn=${mailbox},domain=example.com,ou=mailboxes,dc=cloudron`, 'badpassword'));
            expect(error).to.be.a(ldap.InvalidCredentialsError);
            await mail._updateDomain(domain.domain, { enabled: false });
        });

        it('email enabled - allows with valid password', async function () {
            await mail._updateDomain(domain.domain, { enabled: true });
            await ldapBind(`cn=${mailbox},domain=example.com,ou=mailboxes,dc=cloudron`, user.password);
            await mail._updateDomain(domain.domain, { enabled: false });
        });

        it('email enabled - cannot auth with alias', async function () {
            await mail._updateDomain(domain.domain, { enabled: true });
            const [error] = await safe(ldapBind(`cn=${mailAlias},domain=example.com,ou=mailboxes,dc=cloudron`, 'badpassword'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
            await mail._updateDomain(domain.domain, { enabled: false });
        });
    });

    describe('user msa bind', function () {
        it('email disabled - cannot find domain email', async function () {
            await mail._updateDomain(domain.domain, { enabled: false });
            const [error] = await safe(ldapBind(`cn=${mailbox},ou=msa,dc=cloudron`, 'badpassword'));
            expect(error).to.be.a(ldap.InvalidCredentialsError);
        });

        it('email enabled - allows with valid email', async function () {
            await mail._updateDomain(domain.domain, { enabled: true });
            await ldapBind(`cn=${mailbox},ou=msa,dc=cloudron`, user.password);
            await mail._updateDomain(domain.domain, { enabled: false });
        });

        it('email enabled - does not allow with invalid password', async function () {
            await mail._updateDomain(domain.domain, { enabled: true });
            const [error] = await safe(ldapBind(`cn=${mailbox},ou=msa,dc=cloudron`, 'badpassword'));
            expect(error).to.be.a(ldap.InvalidCredentialsError);
            await mail._updateDomain(domain.domain, { enabled: false });
        });

        it('does not allow for inactive mailbox', async function () {
            await mail._updateDomain(domain.domain, { enabled: true });
            await mail.updateMailbox(mailboxName, domain.domain, { ownerId: user.id, ownerType: mail.OWNERTYPE_USER, active: false, enablePop3: false }, auditSource);
            const [error] = await safe(ldapBind(`cn=${mailbox},ou=msa,dc=cloudron`, 'badpassword'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
            await mail.updateMailbox(mailboxName, domain.domain, { ownerId: user.id, ownerType: mail.OWNERTYPE_USER, active: true, enablePop3: false }, auditSource);
        });
    });

    describe('app msa bind', function () {
        // these tests should work even when email is disabled
        before(async function () {
            await mail._updateDomain(domain.domain, { enabled: false });
        });

        it('does not allow with invalid app', async function () {
            const [error] = await safe(ldapBind(`cn=hacker.app@${domain.domain},ou=msa,dc=cloudron`, 'nope'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('does not allow with invalid password', async function () {
            const [error] = await safe(ldapBind(`cn=${app.subdomain}.app@${domain.domain},ou=msa,dc=cloudron`, 'nope'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('allows with valid password', async function () {
            await addonConfigs.set(app.id, 'sendmail', [{ name: 'MAIL_SMTP_USERNAME', value : `${app.subdomain}.app@${domain.domain}` }, { name: 'MAIL_SMTP_PASSWORD', value : 'sendmailpassword' }]),

            await ldapBind(`cn=${app.subdomain}.app@${domain.domain},ou=msa,dc=cloudron`, 'sendmailpassword');
        });
    });

    describe('user imap bind', function () {
        it('email disabled - cannot find domain email', async function () {
            await mail._updateDomain(domain.domain, { enabled: false });
            const [error] = await safe(ldapBind(`cn=${mailbox},ou=imap,dc=cloudron`, 'badpassword'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('email enabled - allows with valid email', async function () {
            await mail._updateDomain(domain.domain, { enabled: true });
            await ldapBind(`cn=${mailbox},ou=imap,dc=cloudron`, user.password);
        });

        it('email enabled - does not allow with invalid password', async function () {
            await mail._updateDomain(domain.domain, { enabled: true });
            const [error] = await safe(ldapBind(`cn=${mailbox},ou=imap,dc=cloudron`, 'badpassword'));
            expect(error).to.be.a(ldap.InvalidCredentialsError);
        });

        it('does not allow for inactive mailbox', async function () {
            await mail._updateDomain(domain.domain, { enabled: true });
            await mail.updateMailbox(mailboxName, domain.domain, { ownerId: user.id, ownerType: mail.OWNERTYPE_USER, active: false, enablePop3: false }, auditSource);
            const [error] = await safe(ldapBind(`cn=${mailbox},ou=imap,dc=cloudron`, 'badpassword'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
            await mail._updateDomain(domain.domain, { enabled: false });
            await mail.updateMailbox(mailboxName, domain.domain, { ownerId: user.id, ownerType: mail.OWNERTYPE_USER, active: true, enablePop3: false }, auditSource);
        });
    });

    describe('app imap bind', function () {
        before(async function () {
            await mail._updateDomain(domain.domain, { enabled: true });
        });

        it('does not allow with invalid app', async function () {
            const [error] = await safe(ldapBind(`cn=hacker.app@${domain.domain},ou=imap,dc=cloudron`, 'nope'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('does not allow with valid password (missing mailbox)', async function () {
            await addonConfigs.set(app.id, 'recvmail', [{ name: 'MAIL_IMAP_USERNAME', value : `${app.subdomain}.app@${domain.domain}` }, { name: 'MAIL_IMAP_PASSWORD', value : 'imappassword' }]);
            const [error] = await safe(ldapBind(`cn=${app.subdomain}.app@${domain.domain},ou=imap,dc=cloudron`, 'imappassword'));
            expect(error).to.be.a(ldap.NoSuchObjectError);
        });

        it('does not allow with invalid password', async function () {
            await addonConfigs.set(app.id, 'recvmail', [{ name: 'MAIL_IMAP_USERNAME', value : `${mailboxName}@${domain.domain}` }, { name: 'MAIL_IMAP_PASSWORD', value : 'imappassword' }]);
            const [error] = await safe(ldapBind(`cn=${mailboxName}@${domain.domain},ou=imap,dc=cloudron`, 'nope'));
            expect(error).to.be.a(ldap.InvalidCredentialsError);
        });

        it('allows with valid password', async function () {
            await addonConfigs.set(app.id, 'recvmail', [{ name: 'MAIL_IMAP_USERNAME', value : `${mailboxName}@${domain.domain}` }, { name: 'MAIL_IMAP_PASSWORD', value : 'imappassword' }]);
            await ldapBind(`cn=${mailboxName}@${domain.domain},ou=imap,dc=cloudron`, 'imappassword');
        });
    });
});
