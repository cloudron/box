/* global it:false */
/* global describe:false */

'use strict';

const expect = require('expect.js'),
    safe = require('safetydance'),
    superagent = require('../superagent.js');

describe('Superagent', function () {
    it('can get URL', async function () {
        await superagent.get('https://www.cloudron.io').set('User-Agent', 'Mozilla').timeout(10*1000);
    });

    it('cannot get invalid URL', async function () {
        const [error] = await safe(superagent.get('htt://www.cloudron.io'));
        expect(error).to.be.ok();
    });

    it('cannot get non-existent domain', async function () {
        const [error] = await safe(superagent.get('https://www.cloudron.io.nxdomain'));
        expect(error).to.be.ok();
    });

    it('throws for 404', async function () {
        const [error] = await safe(superagent.get('https://www.cloudron.io/no-such-page'));
        expect(error).to.be.ok();
        expect(error.status).to.be(404);
        expect(error.text).to.be.a('string');
    });

    it('can catch a 404', async function () {
        const response = await superagent.get('https://www.cloudron.io/no-such-page').ok(({status}) => status === 404);
        expect(response.status).to.be(404);
        expect(response.text).to.be.a('string');
    });

    it('did parse json', async function () {
        const response = await superagent.get('https://ipv4.api.cloudron.io/api/v1/helper/public_ip');
        expect(response.body.ip).to.be.ok();
    });

    it('follows redirect', async function () {
        const response = await superagent.get('https://cloudron.io').set('User-Agent', 'Mozilla').timeout(10*1000);
        expect(response.url.toString()).to.be('https://www.cloudron.io/');
    });

    it('can disable redirects', async function () {
        const [error] = await safe(superagent.get('https://cloudron.io').set('User-Agent', 'Mozilla').timeout(10*1000).redirects(0));
        expect(error).to.be.ok();
    });

    it('can disable certs', async function () {
        await superagent.get('https://www.cloudron.io').set('User-Agent', 'Mozilla').disableTLSCerts();
    });
});
