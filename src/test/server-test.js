/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const constants = require('../constants.js'),
    expect = require('expect.js'),
    safe = require('safetydance'),
    server = require('../server.js'),
    superagent = require('../superagent.js');

const SERVER_URL = 'http://localhost:' + constants.PORT;

describe('Server', function () {
    describe('startup', function () {
        after(server.stop);

        it('succeeds', async function () {
            await server.start();
        });

        it('is reachable', async function () {
            const response = await superagent.get(SERVER_URL + '/api/v1/cloudron/status');
            expect(response.status).to.equal(200);
        });

        it('should fail because already running', async function () {
            const [error] = await safe(server.start());
            expect(error).to.be.ok();
        });
    });

    describe('runtime', function () {
        before(server.start);
        after(server.stop);

        it('random bad superagents', async function () {
            const response = await superagent.get(SERVER_URL + '/random').ok(() => true);
            expect(response.status).to.equal(404);
        });

        it('version', async function () {
            const response = await superagent.get(SERVER_URL + '/api/v1/cloudron/status');
            expect(response.status).to.equal(200);
            expect(response.body.version).to.contain('-test');
        });

        it('status route is GET', async function () {
            const response = await superagent.post(SERVER_URL + '/api/v1/cloudron/status').ok(() => true);
            expect(response.status).to.equal(404);

            const response2 = await superagent.get(SERVER_URL + '/api/v1/cloudron/status');
            expect(response2.status).to.equal(200);
        });
    });

    describe('config', function () {
        before(server.start);
        after(server.stop);

        it('config fails due missing token', async function () {
            const response = await superagent.get(SERVER_URL + '/api/v1/dashboard/config').ok(() => true);
            expect(response.status).to.equal(401);
        });

        it('config fails due wrong token', async function () {
            const response = await superagent.get(SERVER_URL + '/api/v1/dashboard/config').query({ access_token: 'somewrongtoken' }).ok(() => true);
            expect(response.status).to.equal(401);
        });
    });

    describe('shutdown', function () {
        before(server.stop);

        it('is not reachable anymore', async function () {
            const [error] = await safe(superagent.get(SERVER_URL + '/api/v1/cloudron/status').ok(() => true));
            expect(error).to.not.be(null);
        });
    });

    describe('cors', function () {
        before(server.start);
        after(server.stop);

        it('responds to OPTIONS', async function () {
            const response = await superagent.options(SERVER_URL + '/api/v1/cloudron/status')
                .set('Access-Control-Request-Method', 'GET')
                .set('Access-Control-Request-Headers', 'accept, origin, x-superagented-with')
                .set('Origin', 'http://localhost');

            expect(response.headers['access-control-allow-methods']).to.be('GET, PUT, DELETE, POST, OPTIONS');
            expect(response.headers['access-control-allow-credentials']).to.be('false');
            expect(response.headers['access-control-allow-headers']).to.be('accept, origin, x-superagented-with'); // mirrored from superagent
            expect(response.headers['access-control-allow-origin']).to.be('http://localhost'); // mirrors from superagent
        });

        it('does not crash for malformed origin', async function () {
            const response = await superagent.options(SERVER_URL + '/api/v1/cloudron/status')
                .set('Origin', 'foobar')
                .ok(() => true);
            expect(response.status).to.be(405);
        });
    });
});
