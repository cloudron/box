'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */

'use strict';

const DataLayout = require('../datalayout.js'),
    expect = require('expect.js');

describe('DataLayout', function () {
    describe('no dirMap', function () {
        const BOX_DATA_DIR = '/home/yellowtent/boxdata/box';
        const dataLayout = new DataLayout(BOX_DATA_DIR, []);

        it('localRoot', function () {
            expect(dataLayout.localRoot()).to.be(BOX_DATA_DIR);
        });

        it('getBasename', function () {
            expect(dataLayout.getBasename()).to.be('box');
        });

        it('localPaths', function () {
            expect(dataLayout.localPaths()).to.eql([ BOX_DATA_DIR ]);
        });

        it('toLocalPath', function () {
            expect(dataLayout.toLocalPath('./s1')).to.be(`${BOX_DATA_DIR}/s1`);
            expect(dataLayout.toLocalPath('./s1/')).to.be(`${BOX_DATA_DIR}/s1/`);
            expect(dataLayout.toLocalPath('./s1/s2')).to.be(`${BOX_DATA_DIR}/s1/s2`);
        });

        it('toRemotePath', function () {
            expect(dataLayout.toRemotePath(`${BOX_DATA_DIR}`)).to.be('./');
            expect(dataLayout.toRemotePath(`${BOX_DATA_DIR}/s1`)).to.be('./s1');
            expect(dataLayout.toRemotePath(`${BOX_DATA_DIR}/s1/`)).to.be('./s1/');
            expect(dataLayout.toRemotePath(`${BOX_DATA_DIR}/s1/s2`)).to.be('./s1/s2');
        });
    });

    describe('with dirMap', function () {
        const APP_DATA_DIR = '/home/yellowtent/appsdata/appid';
        const EXT_DIR = '/srv/hsb';
        const dataLayout = new DataLayout(APP_DATA_DIR, [ { localDir: EXT_DIR, remoteDir: 'data' }]);

        it('localRoot', function () {
            expect(dataLayout.localRoot()).to.be(APP_DATA_DIR);
        });

        it('getBasename', function () {
            expect(dataLayout.getBasename()).to.be('appid');
        });

        it('localPaths', function () {
            expect(dataLayout.localPaths()).to.eql([ APP_DATA_DIR, '/srv/hsb' ]);
        });

        it('toLocalPath - root', function () {
            expect(dataLayout.toLocalPath('./s1')).to.be(`${APP_DATA_DIR}/s1`);
            expect(dataLayout.toLocalPath('./s1/')).to.be(`${APP_DATA_DIR}/s1/`);
            expect(dataLayout.toLocalPath('./s1/s2')).to.be(`${APP_DATA_DIR}/s1/s2`);
        });

        it('toLocalPath - extdir', function () {
            expect(dataLayout.toLocalPath('./data/s1')).to.be(`${EXT_DIR}/s1`);
            expect(dataLayout.toLocalPath('./data/s1/')).to.be(`${EXT_DIR}/s1/`);
            expect(dataLayout.toLocalPath('./data/s1/s2')).to.be(`${EXT_DIR}/s1/s2`);
        });

        it('toRemotePath - root', function () {
            expect(dataLayout.toRemotePath(`${APP_DATA_DIR}`)).to.be('./');
            expect(dataLayout.toRemotePath(`${APP_DATA_DIR}/s1`)).to.be('./s1');
            expect(dataLayout.toRemotePath(`${APP_DATA_DIR}/s1/`)).to.be('./s1/');
            expect(dataLayout.toRemotePath(`${APP_DATA_DIR}/s1/s2`)).to.be('./s1/s2');
        });

        it('toRemotePath - extdir', function () {
            expect(dataLayout.toRemotePath(`${EXT_DIR}`)).to.be('./data/');
            expect(dataLayout.toRemotePath(`${EXT_DIR}/s1`)).to.be('./data/s1');
            expect(dataLayout.toRemotePath(`${EXT_DIR}/s1/`)).to.be('./data/s1/');
            expect(dataLayout.toRemotePath(`${EXT_DIR}/s1/s2`)).to.be('./data/s1/s2');
        });
    });

});