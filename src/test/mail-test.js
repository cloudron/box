/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    BoxError = require('../boxerror.js'),
    expect = require('expect.js'),
    mail = require('../mail.js'),
    safe = require('safetydance');

describe('Mail', function () {
    const { setup, cleanup, domain, auditSource } = common;

    before(setup);
    after(cleanup);

    describe('settings', function () {
        it('can get default', async function () {
            const mailConfig = await mail.getDomain(domain.domain);
            expect(mailConfig.enabled).to.be(false);
            expect(mailConfig.mailFromValidation).to.be(true);
            expect(mailConfig.catchAll).to.eql([]);
            expect(mailConfig.relay).to.eql({ provider: 'cloudron-smtp' });
        });

        it('can get all domains', async function () {
            const result = await mail.listDomains();
            expect(result).to.be.an(Array);
            expect(result[0]).to.be.an('object');
            expect(result[0].domain).to.eql(domain.domain);
        });

        it('can set mail from validation', async function () {
            await mail.setMailFromValidation(domain.domain, false);

            const mailConfig = await mail.getDomain(domain.domain);
            expect(mailConfig.mailFromValidation).to.be(false);
        });

        it('cannot set invalid catch all address', async function () {
            const [error] = await safe(mail.setCatchAllAddress(domain.domain, [ 'user1' ]));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('can set invalid catch all address', async function () {
            await mail.setCatchAllAddress(domain.domain, [ `user1@${domain.domain}`, `user2@${domain.domain}` ]);

            const mailConfig = await mail.getDomain(domain.domain);
            expect(mailConfig.catchAll).to.eql([ `user1@${domain.domain}`, `user2@${domain.domain}` ]);
        });

        it('can set mail relay', async function () {
            const relay = { provider: 'external-smtp', host: 'mx.foo.com', port: 25 };

            await mail.setMailRelay(domain.domain, relay, { skipVerify: true });

            const mailConfig = await mail.getDomain(domain.domain);
            expect(mailConfig.relay).to.eql(relay);
        });

        it('can set banner', async function () {
            const banner = { text: 'text', html: 'html' };

            await mail.setBanner(domain.domain, banner);

            const mailConfig = await mail.getDomain(domain.domain);
            expect(mailConfig.banner).to.eql(banner);
        });

        it('can enable mail', async function () {
            await mail.setMailEnabled(domain.domain, true, auditSource);

            const mailConfig = await mail.getDomain(domain.domain);
            expect(mailConfig.enabled).to.be(true);
        });
    });

    describe('mailbox name', function () {
        it('allows valid names', function () {
            expect(mail.validateName('1')).to.be(null); // single char
            expect(mail.validateName('ap')).to.be(null); // alpha
            expect(mail.validateName('aP')).to.be(null); // caps
            expect(mail.validateName('0P')).to.be(null); // number
            expect(mail.validateName('a.p.x')).to.be(null); // dot
            expect(mail.validateName('a-p-x')).to.be(null); // hyphen
            expect(mail.validateName('a-p_x')).to.be(null); // underscore
        });

        it('disallows invalid names', function () {
            expect(mail.validateName('@')).to.be.an(Error);
            expect(mail.validateName('a+p')).to.be.an(Error);
            expect(mail.validateName('a#p')).to.be.an(Error);
            expect(mail.validateName('a!')).to.be.an(Error);
        });
    });

    describe('mailbox display name', function () {
        it('allows valid names', function () {
            expect(mail.validateDisplayName('1')).to.be(null); // single char
            expect(mail.validateDisplayName('ap')).to.be(null); // alpha
            expect(mail.validateDisplayName('aP')).to.be(null); // caps
            expect(mail.validateDisplayName('0P')).to.be(null); // number
            expect(mail.validateDisplayName('a p.x')).to.be(null); // space
            expect(mail.validateDisplayName('a-p-x')).to.be(null); // hyphen
            expect(mail.validateDisplayName('a-p_x')).to.be(null); // underscore
        });

        it('disallows invalid names', function () {
            expect(mail.validateDisplayName('@')).to.be.an(Error);
            expect(mail.validateDisplayName('a<p')).to.be.an(Error);
            expect(mail.validateDisplayName('a>p')).to.be.an(Error);
            expect(mail.validateDisplayName('a"x"')).to.be.an(Error);
        });
    });


    describe('mailboxes', function () {
        it('add user mailbox succeeds', async function () {
            await mail.addMailbox('girish', domain.domain, { ownerId: 'uid-0', ownerType: mail.OWNERTYPE_USER, active: true, storageQuota: 0, messagesQuota: 0 }, auditSource);
        });

        it('cannot add dup entry', async function () {
            const [error] = await safe(mail.addMailbox('girish', domain.domain, { ownerId: 'uid-1', ownerType: mail.OWNERTYPE_GROUP, active: true, storageQuota: 0, messagesQuota: 0 }, auditSource));
            expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
        });

        it('add app mailbox succeeds', async function () {
            await mail.addMailbox('support', domain.domain, { ownerId: 'osticket', ownerType: 'user', active: true, storageQuota: 10, messagesQuota: 20}, auditSource);
        });

        it('get succeeds', async function () {
            const mailbox = await mail.getMailbox('support', domain.domain);
            expect(mailbox.name).to.equal('support');
            expect(mailbox.ownerId).to.equal('osticket');
            expect(mailbox.domain).to.equal(domain.domain);
            expect(mailbox.creationTime).to.be.a(Date);
            expect(mailbox.storageQuota).to.be(10);
            expect(mailbox.messagesQuota).to.be(20);
        });

        it('get non-existent mailbox', async function () {
            const mailbox = await mail.getMailbox('random', domain.domain);
            expect(mailbox).to.be(null);
        });

        it('update app mailbox succeeds', async function () {
            await mail.updateMailbox('support', domain.domain, { ownerId: 'osticket', ownerType: 'user', active: true, storageQuota: 20, messagesQuota: 30}, auditSource);

            const mailbox = await mail.getMailbox('support', domain.domain);
            expect(mailbox.storageQuota).to.be(20);
            expect(mailbox.messagesQuota).to.be(30);
        });

        it('list mailboxes succeeds', async function () {
            const mailboxes = await mail.listMailboxes(domain.domain, null /* search */, 1, 10);
            expect(mailboxes.length).to.be(2);
            expect(mailboxes[0].name).to.be('girish');
            expect(mailboxes[1].name).to.be('support');
        });

        it('list all mailboxes succeeds', async function () {
            const mailboxes = await mail.listAllMailboxes(1, 10);
            expect(mailboxes.length).to.be(2);
            expect(mailboxes[0].name).to.be('girish');
            expect(mailboxes[0].domain).to.be(domain.domain);
            expect(mailboxes[1].name).to.be('support');
            expect(mailboxes[1].domain).to.be(domain.domain);
        });

        it('mailbox count succeeds', async function () {
            const count = await mail.getMailboxCount(domain.domain);
            expect(count).to.be(2);
        });

        it('can set alias', async function () {
            await mail.setAliases('support', domain.domain, [ { name: 'support2', domain: domain.domain }, { name: 'help', domain: domain.domain } ], auditSource);
        });

        it('can get aliases of name', async function () {
            const results = await mail.getAliases('support', domain.domain);
            expect(results.length).to.be(2);
            expect(results[0].name).to.be('help');
            expect(results[0].domain).to.be(domain.domain);
            expect(results[1].name).to.be('support2');
            expect(results[1].domain).to.be(domain.domain);
        });

        it('can set wildcard alias', async function () {
            await mail.setAliases('support', domain.domain, [ { name: 'support*', domain: domain.domain }, { name: 'help', domain: domain.domain } ], auditSource);
        });

        it('can get aliases of name', async function () {
            const results = await mail.getAliases('support', domain.domain);
            expect(results.length).to.be(2);
            expect(results[0].name).to.be('help');
            expect(results[0].domain).to.be(domain.domain);
            expect(results[1].name).to.be('support*');
            expect(results[1].domain).to.be(domain.domain);
        });

        it('unset aliases', async function () {
            await mail.setAliases('support', domain.domain, [], auditSource);

            const results = await mail.getAliases('support', domain.domain);
            expect(results.length).to.be(0);
        });

        it('add list succeeds', async function () {
            await mail.addList('people', domain.domain, { members: [ 'test@cloudron.io' ], membersOnly: false, active: true }, auditSource);
        });

        it('cannot add dup list', async function () {
            const [error] = await safe(mail.addList('people', domain.domain, { members: [ 'admin@cloudron.io' ], membersOnly: false, active: true }, auditSource));
            expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
        });

        it('cannot get non-existing list', async function () {
            const result = await mail.getList('random', domain.domain);
            expect(result).to.be(null);
        });

        it('del list succeeds', async function () {
            await mail.delList('people', domain.domain, auditSource);
            const result = await mail.getList('people', domain.domain);
            expect(result).to.be(null);
        });

        it('del non-existent list fails', async function () {
            const [error] = await safe(mail.delList('people', domain.domain, auditSource));
            expect(error.reason).to.be(BoxError.NOT_FOUND);
        });

        it('del mailbox succeeds', async function () {
            await mail.delMailbox('girish', domain.domain, {/*options*/}, auditSource);
            const result = await mail.getMailbox('girish', domain.domain);
            expect(result).to.be(null);
        });

        it('del non-existent mailbox fails', async function () {
            const [error] = await safe(mail.delMailbox('girish', domain.domain, {/*options*/}, auditSource));
            expect(error.reason).to.be(BoxError.NOT_FOUND);
        });
    });
});
