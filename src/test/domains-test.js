/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const apps = require('../apps.js'),
    BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    domains = require('../domains.js'),
    expect = require('expect.js'),
    safe = require('safetydance');

describe('Domains', function () {
    const { setup, cleanup, domain, app, auditSource } = common;

    before(setup);
    after(cleanup);

    const DOMAIN_0 = {
        domain: 'z0.com',
        zoneName: 'z0.com',
        provider: 'noop',
        config: { },
        fallbackCertificate: null,
        tlsConfig: {
            provider: 'fallback'
        },
        wellKnown: null
    };

    it('can add domain', async function () {
        await domains.add(DOMAIN_0.domain, DOMAIN_0, auditSource);
    });

    it('cannot add same domain twice', async function () {
        const [error] = await safe(domains.add(DOMAIN_0.domain, DOMAIN_0, auditSource));
        expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
    });

    it('can get domain', async function () {
        const result = await domains.get(DOMAIN_0.domain);
        expect(result.domain).to.equal(DOMAIN_0.domain);
        expect(result.zoneName).to.equal(DOMAIN_0.zoneName);
        expect(result.config).to.eql(DOMAIN_0.config);
    });

    it('cannot get non-existent domain', async function () {
        const result = await domains.get('random');
        expect(result).to.be(null);
    });

    it('can set domain config', async function () {
        const newConfig = {};
        const newTlsConfig = { provider: 'letsencrypt-staging' };
        const newDomain = Object.assign({}, DOMAIN_0, { config: newConfig, tlsConfig: newTlsConfig });

        await domains.setConfig(DOMAIN_0.domain, newDomain, auditSource);

        const result = await domains.get(DOMAIN_0.domain);
        expect(result.domain).to.equal(DOMAIN_0.domain);
        expect(result.zoneName).to.equal(DOMAIN_0.zoneName);
        expect(result.provider).to.equal(DOMAIN_0.provider);
        expect(result.config).to.eql(newConfig);
        expect(result.tlsConfig).to.eql(newTlsConfig);

        DOMAIN_0.config = newConfig;
        DOMAIN_0.tlsConfig = newTlsConfig;
    });

    it('can set domain wellknown', async function () {
        await domains.setWellKnown(DOMAIN_0.domain, { service: 'some.service' }, auditSource);
        let result = await domains.get(DOMAIN_0.domain);
        expect(result.wellKnown).to.eql({ service: 'some.service' });

        await domains.setWellKnown(DOMAIN_0.domain, null, auditSource);
        result = await domains.get(DOMAIN_0.domain);
        expect(result.wellKnown).to.eql(null);
    });

    it('can get all domains', async function () {
        const result = await domains.list();
        expect(result.length).to.equal(2);

        // sorted by domain
        expect(result[0].domain).to.equal(domain.domain);
        expect(result[0].zoneName).to.equal(domain.zoneName);
        expect(result[0].provider).to.equal(domain.provider);
        expect(result[0].config).to.eql(domain.config);
        expect(result[0].tlsConfig).to.eql(domain.tlsConfig);

        expect(result[1].domain).to.equal(DOMAIN_0.domain);
        expect(result[1].zoneName).to.equal(DOMAIN_0.zoneName);
        expect(result[1].provider).to.equal(DOMAIN_0.provider);
        expect(result[1].config).to.eql(DOMAIN_0.config);
        expect(result[1].tlsConfig).to.eql(DOMAIN_0.tlsConfig);
    });

    it('cannot delete non-existing domain', async function () {
        const [error] = await safe(domains.del('not.exists', auditSource));
        expect(error).to.be.a(BoxError);
        expect(error.reason).to.equal(BoxError.NOT_FOUND);
    });

    it('cannot delete dashboard domain', async function () {
        const [error] = await safe(domains.del(domain.domain, auditSource));
        expect(error).to.be.a(BoxError);
        expect(error.reason).to.equal(BoxError.CONFLICT);
        expect(error.message).to.equal('Cannot remove admin domain');
    });

    it('cannot delete referenced domain', async function () {
        const appCopy = Object.assign({}, app, { id: 'into', subdomain: 'xx', domain: DOMAIN_0.domain, portBindings: {} });

        await apps.add(appCopy.id, appCopy.appStoreId, appCopy.manifest, appCopy.subdomain, appCopy.domain, appCopy.portBindings, appCopy);

        const [error] = await safe(domains.del(DOMAIN_0.domain, auditSource));
        expect(error.reason).to.equal(BoxError.CONFLICT);
        expect(error.message).to.contain('Domain is in use in an app\'s location');

        await apps.del(appCopy.id);
    });

    it('can delete existing domain', async function () {
        await domains.del(DOMAIN_0.domain, auditSource);

        const result = await domains.get(DOMAIN_0.domain);
        expect(result).to.be(null);
    });
});
