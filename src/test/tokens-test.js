/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    safe = require('safetydance'),
    tokens = require('../tokens.js');

describe('Tokens', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    const TOKEN_0 = {
        id: null,
        name: 'token0',
        accessToken: null,
        identifier: '0',
        clientId: 'clientid-0',
        expires: Date.now() + 60 * 60000,
        lastUsedTime: null,
        scope: { '*': 'rw' },
        allowedIpRanges: '#this is our server\n3.4.5.6\nfe80::42:5ff:fe1b:2d9e/64\n\n172.17.0.1/16\nfe80::ec78:50ff:fecc:50a4/64'
    };

    it('add fails with bad name', async function () {
        const badToken = Object.assign({}, TOKEN_0);
        badToken.name = new Array(100).fill('x').join('');
        const [error] = await safe(tokens.add(badToken));
        expect(error.reason).to.be(BoxError.BAD_FIELD);
    });

    it('add fails with unknown scope', async function () {
        const badToken = Object.assign({}, TOKEN_0);
        badToken.scope = { 'foobar': 'rw', '*': 'r' };
        const [error] = await safe(tokens.add(badToken));
        expect(error.reason).to.be(BoxError.BAD_FIELD);
    });

    it('add fails with invalid scope rule', async function () {
        const badToken = Object.assign({}, TOKEN_0);
        badToken.scope = { '*': 'rw ' };
        const [error] = await safe(tokens.add(badToken));
        expect(error.reason).to.be(BoxError.BAD_FIELD);
    });

    it('add fails with bad name', async function () {
        const badToken = Object.assign({}, TOKEN_0);
        badToken.name = new Array(100).fill('x').join('');
        const [error] = await safe(tokens.add(badToken));
        expect(error.reason).to.be(BoxError.BAD_FIELD);
    });

    it('add fails for bad allowed ips', async function () {
        const badToken = Object.assign({}, TOKEN_0);
        badToken.allowedIpRanges = '1.2.3./4';
        const [error] = await safe(tokens.add(badToken));
        expect(error.reason).to.be(BoxError.BAD_FIELD);
    });

    it('add succeeds', async function () {
        const { id, accessToken } = await tokens.add(TOKEN_0);
        TOKEN_0.id = id;
        TOKEN_0.accessToken = accessToken;
    });

    it('get succeeds', async function () {
        const result = await tokens.get(TOKEN_0.id);
        expect(result).to.be.eql(TOKEN_0);
    });

    it('getByAccessToken succeeds', async function () {
        const result = await tokens.getByAccessToken(TOKEN_0.accessToken);
        expect(result).to.be.eql(TOKEN_0);
    });

    it('get of nonexisting token fails', async function () {
        const result = await tokens.getByAccessToken('somerandomaccesstoken');
        expect(result).to.be(null);
    });

    it('listByUserId succeeds', async function () {
        const result = await tokens.listByUserId(TOKEN_0.identifier);
        expect(result).to.be.an(Array);
        expect(result.length).to.equal(1);
        expect(result[0]).to.be.an('object');
        expect(result[0]).to.be.eql(TOKEN_0);
    });

    it('delete fails', async function () {
        const [error] = await safe(tokens.del(TOKEN_0.id + 'x'));
        expect(error).to.be.a(BoxError);
        expect(error.reason).to.be(BoxError.NOT_FOUND);
    });

    it('delete succeeds', async function () {
        await tokens.del(TOKEN_0.id);
    });

    it('get returns null after token deletion', async function () {
        const result = await tokens.get(TOKEN_0.id);
        expect(result).to.be(null);
    });

    it('cannot delete previously delete record', async function () {
        const [error] = await safe(tokens.del(TOKEN_0.id));
        expect(error).to.be.a(BoxError);
        expect(error.reason).to.be(BoxError.NOT_FOUND);
    });

    it('delExpired succeeds', async function () {
        const token1 = {
            name: 'token1',
            identifier: '1',
            clientId: 'clientid-1',
            expires: Number.MAX_SAFE_INTEGER,
            lastUsedTime: null,
            scope: { '*': 'rw' },
            allowedIpRanges: '#this is our server\n3.4.5.6'
        };
        const token2 = {
            name: 'token2',
            identifier: '2',
            clientId: 'clientid-2',
            expires: Date.now(),
            lastUsedTime: null,
            allowedIpRanges: '#this'
        };

        let result = await tokens.add(token1);
        token1.id = result.id;
        token1.accessToken = result.accessToken;

        result = await tokens.add(token2);
        token2.id = result.id;
        token2.accessToken = result.accessToken;

        await tokens.delExpired();

        result = await tokens.getByAccessToken(token2.accessToken);
        expect(result).to.be(null);

        result = await tokens.getByAccessToken(token1.accessToken);
        expect(result).to.eql(token1);
    });

    it('delByUserIdAndType succeeds', async function () {
        const token1 = {
            name: 'token1',
            identifier: 'user1',
            clientId: tokens.ID_WEBADMIN,
            expires: Number.MAX_SAFE_INTEGER,
            lastUsedTime: null,
            scope: { '*': 'rw' },
            allowedIpRanges: '#this is our server\n3.4.5.6'
        };
        const token2 = {
            name: 'token2',
            identifier: 'user1',
            clientId: tokens.ID_SDK,
            expires: Date.now(),
            lastUsedTime: null,
            allowedIpRanges: '#this'
        };

        await tokens.add(token1);
        await tokens.add(token2);

        await tokens.delByUserIdAndType('user2', tokens.ID_WEBADMIN);
        let result = await tokens.listByUserId('user1');
        expect(result.length).to.be(2); // should not have deleted user1 tokens

        await tokens.delByUserIdAndType('user1', tokens.ID_WEBADMIN);
        result = await tokens.listByUserId('user1');
        expect(result.length).to.be(1);
        expect(result[0].name).to.be(token2.name);
    });
});
