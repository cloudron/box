/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const BoxError = require('../boxerror.js'),
    branding = require('../branding.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    safe = require('safetydance');

describe('Branding', function () {
    const { setup, cleanup, auditSource } = common;

    before(setup);
    after(cleanup);

    it ('can get default cloudron name', async function () {
        const name = await branding.getCloudronName();
        expect(name).to.be('Cloudron');
    });

    it('can set name', async function () {
        await branding.setCloudronName('Dolomites', auditSource);
        const name = await branding.getCloudronName();
        expect(name).to.be('Dolomites');
    });

    it('cannot set large name', async function () {
        const [error] = await safe(branding.setCloudronName('x'.repeat(65), auditSource));
        expect(error.reason).to.be(BoxError.BAD_FIELD);
    });

    it('can get default cloudron avatar', async function () {
        const avatar = await branding.getCloudronAvatar();
        expect(avatar).to.be.a(Buffer);
    });

    it('can render default footer', async function () {
        expect(await branding.renderFooter()).to.contain('(https://cloudron.io)');
    });

    it('can render footer', async function () {
        await branding.setFooter('BigFoot Inc', auditSource);
        expect(await branding.renderFooter()).to.be('BigFoot Inc');
    });

    it('can render footer with YEAR', async function () {
        await branding.setFooter('BigFoot Inc %YEAR%', auditSource);
        expect(await branding.renderFooter()).to.be('BigFoot Inc 2025');
    });
});
