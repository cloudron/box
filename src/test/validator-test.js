'use strict';

/* global it, describe, before, after */

const expect = require('expect.js'),
    validator = require('../validator.js');

describe('Validator', function () {
    const goodEmails = [
        'a@b.com',
        '1@2.com',
        'a_v-d@b.com',
        'a_v-d@b.what-is.de.com',
        'a_v-d+gee@b.what-is.de.com',
        'a_v-d+gee@b.what-is.de.university'
    ];

    for (const goodEmail of goodEmails) {
        it(`isEmail returns false ${goodEmail}`, () => expect(validator.isEmail(goodEmail)).to.be(true));
    }

    const badEmails = [
        'apple',
        'a@',
        '@b.com',
        'user name@example.com',
        'user@example#com'
    ];

    for (const badEmail of badEmails) {
        it(`isEmail returns false ${badEmail}`, () => expect(validator.isEmail(badEmail)).to.be(false));
    }
});
