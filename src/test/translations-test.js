/* jslint node:true */
/* global it:false */
/* global describe:false */

'use strict';

const expect = require('expect.js'),
    translations = require('../translations.js');

describe('translation', function () {

    describe('translate', function () {
        it('nonexisting token', function () {
            const assets = { translations: {}, fallback: {} };
            const  out = translations.translate('Foo {{ bar }}', assets);
            expect(out).to.contain('{{ bar }}');
        });

        it('existing token', function () {
            const assets = { translations: { bar: 'here' }, fallback: {} };
            const  out = translations.translate('Foo {{ bar }}', assets);
            expect(out).to.contain('here');
        });

        it('existing token as fallback', function () {
            const assets = { translations: {}, fallback: {  bar: 'here' } };
            const  out = translations.translate('Foo {{ bar }}', assets);
            expect(out).to.contain('here');
        });

        it('existing token deep', function () {
            const assets = { translations: { bar: { baz: { foo: 'here' }}}, fallback: {} };
            const  out = translations.translate('Foo {{ bar.baz.foo }}', assets);
            expect(out).to.contain('here');
        });

        it('existing token deep as fallback', function () {
            const assets = { translations: { bar: '' }, fallback: { bar: { baz: { foo: 'here' } } } };
            const  out = translations.translate('Foo {{ bar.baz.foo }}', assets);
            expect(out).to.contain('here');
        });

        it('with whitespace tokens', function () {
            const assets = {
                translations: {
                    something: {
                        missing: {
                            there: '1'
                        }
                    },
                    here: '2',
                    there: '3',
                    foo: '4',
                    bar: '5'
                },
                fallback: {}
            };
            const  input = 'Hello {{ something.missing.there}} and some more {{here}} and {{ there }} with odd spacing {{foo }} lots of{{       bar     }}';

            const  out = translations.translate(input, assets);
            expect(out).to.contain('1');
            expect(out).to.contain('2');
            expect(out).to.contain('3');
            expect(out).to.contain('4');
            expect(out).to.contain('5');
        });
    });
});
