/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    paths = require('../paths.js'),
    safe = require('safetydance'),
    volumes = require('../volumes.js');

describe('Volumes', function () {
    const { setup, cleanup, auditSource } = common;

    before(setup);
    after(cleanup);

    it('cannot add bad name', async function () {
        const [error] = await safe(volumes.add({ name: 'music/is', hostPath: '/tmp/music', mountType: 'filesystem', mountOptions: {} }, auditSource));
        if (!error) throw new Error('Expecting bad field error');
        expect(error.reason).to.be(BoxError.BAD_FIELD);
    });

    it('cannot add bad path', async function () {
        const [error] = await safe(volumes.add({ name: 'music', hostPath: '/tmp/music', mountType: 'filesystem', mountOptions: {} }, auditSource));
        if (!error) throw new Error('Expecting bad field error');
        expect(error.reason).to.be(BoxError.BAD_FIELD);
    });

    let volume;
    it('can add volume', async function () {
        const id = await volumes.add({ name: 'music', mountType: 'filesystem', mountOptions: { hostPath: '/mnt/cloudron-test-music' } }, auditSource);
        expect(id).to.be.a('string');
        volume = { id, name: 'music', hostPath: '/mnt/cloudron-test-music' };
    });

    it('cannot add duplicate path', async function () {
        const [error] = await safe(volumes.add({ name: 'music-dup', mountType: 'filesystem', mountOptions: { hostPath: '/mnt/cloudron-test-music' } }, auditSource));
        expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
    });

    it('cannot add duplicate name', async function () {
        const [error] = await safe(volumes.add({ name: 'music', mountType: 'filesystem', mountOptions: { hostPath: '/mnt/cloudron-test-music2' } }, auditSource));
        expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
    });

    it('can get volume', async function () {
        const result = await volumes.get(volume.id);
        expect(result.hostPath).to.be('/mnt/cloudron-test-music');
    });

    it('cannot get random volume', async function () {
        const result = await volumes.get('randomvolume');
        expect(result).to.be(null);
    });

    it('can list volumes', async function () {
        const result = await volumes.list();
        expect(result).to.be.an(Array);
        expect(result.length).to.be(1);
        expect(result[0].id).to.be(volume.id);
        expect(result[0].hostPath).to.be('/mnt/cloudron-test-music');
    });

    it('cannot del random volume', async function () {
        const [error] = await safe(volumes.del({ id: 'randomvolume' }, auditSource));
        expect(error.reason).to.be(BoxError.NOT_FOUND);
    });

    it('can del volume', async function () {
        await volumes.del(volume, auditSource);
    });

    const badPaths = [
        '/opt/data/../bar', // not normalized
        'opt/data/bar', // not absolute
        '/', // root
        '/mnt', // top level itself is reserved
        '/usr/bin', // reserved
        paths.VOLUMES_MOUNT_DIR, // reserved
        paths.VOLUMES_MOUNT_DIR + '/', // also reserved
        paths.VOLUMES_MOUNT_DIR + '/something', // also reserved
        '/media/cloudron-test-music/root', // realpath won't match
        '/media/cloudron-test-music/root/', // realpath won't match
        '/media/cloudron-test-music/file', // need directory
        '/media/cloudron-test-music/randompath', // need directory
    ];

    for (const p of badPaths) {
        it(`validateHostPath - ${p}`, function () {
            const error = volumes._validateHostPath(p);
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });
    }
});
