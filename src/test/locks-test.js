'use strict';

/* global it, describe, before, after */

const BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    safe = require('safetydance'),
    locks = require('../locks.js');

describe('Locks', function () {
    this.timeout(20000);

    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    it('can release all locks', async function () {
        await locks.releaseAll();
    });

    it('acquire lock foo', async function () {
        await locks.acquire('foo');
    });

    it('cannot reacquire lock foo', async function () {
        const [error] = await safe(locks.acquire('foo'));
        expect(error.reason).to.be(BoxError.BAD_STATE);
    });

    it('cannot reacquire after release', async function () {
        await locks.release('foo');
        await locks.acquire('foo');
    });

    it('task gets a lock', async function () {
        locks.setTaskId('42');
        await locks.acquire('tasklock');
    });

    it('can wait for a lock', async function () {
        setTimeout(() => locks.releaseByTaskId('42'), 3000); // release in 3 seconds

        const startTime = Date.now();
        await locks.wait('tasklock'); // retries only in 10s
        const endTime = Date.now();
        expect(endTime - startTime).to.be.greaterThan(9900);
        expect(endTime - startTime).to.be.lessThan(10100);
    });
});
