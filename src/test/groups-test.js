/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    groups = require('../groups.js'),
    safe = require('safetydance');

describe('Groups', function () {
    const { setup, cleanup, admin, user, auditSource, app } = common;

    before(setup);
    after(cleanup);

    const group0Name = 'administrators';
    let group0Object;
    const group1Name = 'externs';
    let group1Object;

    describe('add', function () {
        it('cannot add group - too small', async function () {
            const [error] = await safe(groups.add({ name: '' }, auditSource));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot add group - too big', async function () {
            const [error] = await safe(groups.add({ name: new Array(256).join('a') }, auditSource));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot add group - bad name', async function () {
            const [error] = await safe(groups.add({ name: 'bad:name' }, auditSource));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot add group - reserved', async function () {
            const [error] = await safe(groups.add({ name: 'users' }, auditSource));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot add group - invalid', async function () {
            const [error] = await safe(groups.add({ name: 'cloudron+admin' }, auditSource));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot add group - invalid source', async function () {
            const [error] = await safe(groups.add({ name: 'somegroup', source: 'unknownsource' }, auditSource));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('can add valid groups', async function () {
            let [error, result] = await safe(groups.add({ name: group0Name }, auditSource));
            expect(error).to.be(null);
            group0Object = result;

            [error, result] = await safe(groups.add({ name: group1Name}, auditSource));
            expect(error).to.be(null);
            group1Object = result;
        });

        it('cannot add existing group with mixed case', async function () {
            const name = group0Name[0].toUpperCase() + group0Name.slice(1);
            const [error] = await safe(groups.add({ name }, auditSource));
            expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
        });

        it('cannot add existing group', async function () {
            const [error] = await safe(groups.add({name: group0Name }, auditSource));
            expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
        });
    });

    describe('get', function () {
        it('cannot get invalid group', async function () {
            const result = await groups.get('sometrandom');
            expect(result).to.be(null);
        });

        it('can get valid group', async function () {
            const result = await groups.get(group0Object.id);
            expect(result.name).to.equal(group0Name);
        });
    });

    describe('members', function () {
        it('isMember returns false', async function () {
            const isMember = await groups.isMember(group0Object.id, admin.id);
            expect(isMember).to.be(false);
        });

        it('can set members', async function () {
            await groups.setMembers(group0Object, [ admin.id, user.id ], {}, auditSource);
        });

        it('cannot set duplicate members', async function () {
            const [error] = await safe(groups.setMembers(group0Object, [ admin.id, user.id, admin.id ], {}, auditSource));
            expect(error.reason).to.be(BoxError.CONFLICT);
        });

        it('can list users of group', async function () {
            const result = await groups.getMemberIds(group0Object.id);
            expect(result.sort()).to.eql([ admin.id, user.id ].sort());
        });

        it('cannot list members of non-existent group', async function () {
            const result = await groups.getMemberIds('randomgroup');
            expect(result.length).to.be(0); // currently, we cannot differentiate invalid groups and empty groups
        });

        it('can getWithMembers', async function () {
            const result = await groups.getWithMembers(group0Object.id);
            expect(result.name).to.be(group0Name);
            expect(result.userIds.sort()).to.eql([ admin.id, user.id ].sort());
        });

        it('can set group membership', async function () {
            await groups.setLocalMembership(admin, [ group0Object.id ], auditSource);
            const groupIds = await groups._getMembership(admin.id);
            expect(groupIds.length).to.be(1);
        });

        it('cannot set user to same group twice', async function () {
            const [error] = await safe(groups.setLocalMembership(admin, [ group0Object.id, group0Object.id ], auditSource));
            expect(error.reason).to.be(BoxError.CONFLICT);
        });

        it('can set user to multiple groups', async function () {
            await groups.setLocalMembership(admin, [ group0Object.id, group1Object.id ], auditSource);
        });

        it('can get groups membership', async function () {
            const groupIds = await groups._getMembership(admin.id);
            expect(groupIds.length).to.be(2);
            expect(groupIds.sort()).to.eql([ group0Object.id, group1Object.id ].sort());
        });
    });

    describe('list', function () {
        it('can list', async function () {
            const result = await groups.list();
            expect(result.length).to.be(2);
            expect(result[0].name).to.be(group0Name);
            expect(result[1].name).to.be(group1Name);
        });

        it('can listWithMembers', async function () {
            const result = await groups.listWithMembers();
            expect(result.length).to.be(2);
            expect(result[0].name).to.be(group0Name);
            expect(result[1].userIds).to.eql([ admin.id ]);
            expect(result[1].name).to.be(group1Name);
        });
    });

    describe('delete', function () {
        it('cannot delete invalid group', async function () {
            const [error] = await safe(groups.del({ id: 'random' }, auditSource));
            expect(error.reason).to.be(BoxError.NOT_FOUND);
        });

        it('can delete valid group', async function () {
            await groups.setMembers(group0Object, [ admin.id, user.id ], {}, auditSource); // ensure group has some members
            await groups.del(group0Object, auditSource);
        });
    });

    describe('update', function () {
        let groupObject;

        before(async function () {
            const [error, result] = await safe(groups.add({ name: 'kootam' }, auditSource));
            expect(error).to.be(null);
            groupObject = result;
        });

        it('cannot set empty group name', async function () {
            const [error] = await safe(groups.setName(groupObject, '', auditSource));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot set bad group name', async function () {
            const [error] = await safe(groups.setName(groupObject, '!kootam', auditSource));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('can set group name', async function () {
            await groups.setName(groupObject, 'kootam2', auditSource);
            groupObject = await groups.get(groupObject.id);
            expect(groupObject.name).to.be('kootam2');
        });
    });

    describe('app access', function () {
        let groupObject;

        before(async function () {
            const [error, result] = await safe(groups.add({ name: 'kootam' }, auditSource));
            expect(error).to.be(null);
            groupObject = result;
        });

        it('has no app access', async function () {
            expect(groupObject.appIds).to.eql([]);

            const g1 = await groups.get(groupObject.id);
            expect(g1.appIds).to.eql([]);

            const g2 = await groups.getByName(groupObject.name);
            expect(g2.appIds).to.eql([]);

            const g3 = await groups.getWithMembers(groupObject.id);
            expect(g3.appIds).to.eql([]);
        });

        it('set app access', async function () {
            await groups.setAllowedApps(groupObject, [ app.id ], auditSource);

            const g1 = await groups.get(groupObject.id);
            expect(g1.appIds).to.eql([ app.id ]);

            const g2 = await groups.getByName(groupObject.name);
            expect(g2.appIds).to.eql([ app.id ]);

            const g3 = await groups.getWithMembers(groupObject.id);
            expect(g3.appIds).to.eql([ app.id ]);

            const allGroups = await groups.listWithMembers();
            const g4 = allGroups.filter(g => g.id === groupObject.id)[0];
            expect(g4.appIds).to.eql([ app.id ]);
        });

        it('cleared app access', async function () {
            await groups.setAllowedApps(groupObject, [ ], auditSource);

            const g1 = await groups.get(groupObject.id);
            expect(g1.appIds).to.eql([ ]);

            const g2 = await groups.getByName(groupObject.name);
            expect(g2.appIds).to.eql([ ]);

            const g3 = await groups.getWithMembers(groupObject.id);
            expect(g3.appIds).to.eql([ ]);

            const allGroups = await groups.listWithMembers();
            const g4 = allGroups.filter(g => g.id === groupObject.id)[0];
            expect(g4.appIds).to.eql([ ]);
        });
    });

    describe('ldap group', function () {
        let ldapGroup;

        before(async function () {
            ldapGroup = await groups.add({ name: 'ldap-kootam', source: 'ldap' }, auditSource);
        });

        it('cannot change name', async function () {
            const [error] = await safe(groups.setName(ldapGroup, 'ldap-kootam2', auditSource));
            expect(error.reason).to.be(BoxError.BAD_STATE);
        });

        it('cannot set members', async function () {
            const [error] = await safe(groups.setMembers(ldapGroup, [ admin.id ], { skipSourceSkip: false }, auditSource));
            expect(error.reason).to.be(BoxError.BAD_STATE);
        });

        it('cannot set membership', async function () {
            const [error] = await safe(groups.setLocalMembership(admin, [ ldapGroup.id ], auditSource));
            expect(error.reason).to.be(BoxError.BAD_STATE);
        });

        it('does not clear remote membership', async function () {
            await groups.setMembers(ldapGroup, [ admin.id ], { skipSourceCheck: true }, auditSource); // would be called by ldap syncer
            await groups.setLocalMembership(admin, [ group1Object.id ], auditSource);

            const groupIds = await groups._getMembership(admin.id);
            expect(groupIds.length).to.be(2);
            expect(groupIds.sort()).to.eql([ group1Object.id, ldapGroup.id ].sort());
        });
    });
});
