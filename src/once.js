'use strict';

exports = module.exports = once;

const debug = require('debug')('box:once');

// https://github.com/isaacs/once/blob/main/LICENSE (ISC)
function once (fn) {
    const f = function () {
        if (f.called) {
            debug(`${f.name} was already called, returning previous return value`);
            return f.value;
        }
        f.called = true;
        return f.value = fn.apply(this, arguments);
    };
    f.called = false;
    return f;
}
