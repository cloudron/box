'use strict';

exports = module.exports = {
    initialize,
    uninitialize,
    query,
    transaction,

    importFromFile,
    exportToFile,

    _clear: clear
};

const assert = require('assert'),
    async = require('async'),
    BoxError = require('./boxerror.js'),
    constants = require('./constants.js'),
    debug = require('debug')('box:database'),
    fs = require('fs'),
    mysql = require('mysql'),
    safe = require('safetydance'),
    shell = require('./shell.js')('database');

let gConnectionPool = null;

const gDatabase = {
    hostname: '127.0.0.1',
    username: 'root',
    password: 'password',
    port: 3306,
    name: 'box'
};

async function initialize() {
    if (gConnectionPool !== null) return;

    if (constants.TEST) {
        // see setupTest script how the mysql-server is run
        gDatabase.hostname = require('child_process').execSync('docker inspect -f "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}" mysql-server').toString().trim();
    }

    // https://github.com/mysqljs/mysql#pool-options
    gConnectionPool  = mysql.createPool({
        connectionLimit: 5,
        acquireTimeout: 60000,
        connectTimeout: 60000,
        host: gDatabase.hostname,
        user: gDatabase.username,
        password: gDatabase.password,
        port: gDatabase.port,
        database: gDatabase.name,
        multipleStatements: false,
        waitForConnections: true, // getConnection() will wait until a connection is avaiable
        ssl: false,
        timezone: 'Z' // mysql follows the SYSTEM timezone. on Cloudron, this is UTC
    });

    gConnectionPool.on('connection', function (connection) {
        // connection objects are re-used. so we have to attach to the event here (once) to prevent crash
        // note the pool also has an 'acquire' event but that is called whenever we do a getConnection()
        connection.on('error', (error) => debug(`Connection ${connection.threadId} error: ${error.message} ${error.code}`));

        connection.query(`USE ${gDatabase.name}`);
        connection.query('SET SESSION sql_mode = \'strict_all_tables\'');
        // GROUP_CONCAT has only 1024 default. we use it in the groups API which doesn't support pagination yet
        // a uuid v4 is 36 in length. so the value below provides for roughly 10k users
        connection.query('SET SESSION group_concat_max_len = 360000');
    });
}

async function uninitialize() {
    if (!gConnectionPool) return;

    gConnectionPool.end();
    gConnectionPool = null;
}

async function clear() {
    await fs.promises.writeFile('/tmp/extra.cnf', `[client]\nhost=${gDatabase.hostname}\nuser=${gDatabase.username}\npassword=${gDatabase.password}\ndatabase=${gDatabase.name}`, 'utf8');

    const cmd = 'mysql --defaults-extra-file=/tmp/extra.cnf -Nse "SHOW TABLES" | grep -v "^migrations$" | while read table; do mysql --defaults-extra-file=/tmp/extra.cnf -e "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE $table"; done';
    await shell.bash(cmd, {});
}

async function query() {
    assert.notStrictEqual(gConnectionPool, null);

    return new Promise((resolve, reject) => {
        const args = Array.prototype.slice.call(arguments);

        args.push(function queryCallback(error, result) {
            if (error) return reject(new BoxError(BoxError.DATABASE_ERROR, error, { code: error.code, sqlMessage: error.sqlMessage || null }));

            resolve(result);
        });

        gConnectionPool.query.apply(gConnectionPool, args); // this is same as getConnection/query/release
    });
}

async function transaction(queries) {
    assert(Array.isArray(queries));

    return new Promise((resolve, reject) => {
        gConnectionPool.getConnection(function (error, connection) {
            if (error) return reject(new BoxError(BoxError.DATABASE_ERROR, error, { code: error.code, sqlMessage: error.sqlMessage }));

            const releaseConnection = (error) => {
                connection.release();
                reject(new BoxError(BoxError.DATABASE_ERROR, error, { code: error.code, sqlMessage: error.sqlMessage || null }));
            };

            connection.beginTransaction(function (error) {
                if (error) return releaseConnection(error);

                async.mapSeries(queries, function iterator(query, done) {
                    connection.query(query.query, query.args, done);
                }, function seriesDone(error, results) {
                    if (error) return connection.rollback(() => releaseConnection(error));

                    connection.commit(function (error) {
                        if (error) return connection.rollback(() => releaseConnection(error));

                        connection.release();

                        resolve(results);
                    });
                });
            });
        });
    });
}

async function importFromFile(file) {
    assert.strictEqual(typeof file, 'string');

    const cmd = `/usr/bin/mysql -h "${gDatabase.hostname}" -u ${gDatabase.username} -p${gDatabase.password} ${gDatabase.name} < ${file}`;
    await query('CREATE DATABASE IF NOT EXISTS box');
    const [error] = await safe(shell.bash(cmd, {}));
    if (error) throw new BoxError(BoxError.DATABASE_ERROR, error);
}

async function exportToFile(file) {
    assert.strictEqual(typeof file, 'string');

    // latest mysqldump enables column stats by default which is not present in 5.7 util
    const mysqlDumpHelp = await shell.spawn('/usr/bin/mysqldump', ['--help'], { encoding: 'utf8' });
    const hasColStats = mysqlDumpHelp.includes('column-statistics');
    const colStats = hasColStats ? '--column-statistics=0' : '';

    const cmd = `/usr/bin/mysqldump -h "${gDatabase.hostname}" -u root -p${gDatabase.password} ${colStats} --single-transaction --routines --triggers ${gDatabase.name} > "${file}"`;

    const [error] = await safe(shell.bash(cmd, {}));
    if (error) throw new BoxError(BoxError.DATABASE_ERROR, error);
}
