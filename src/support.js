'use strict';

exports = module.exports = {
    getRemoteSupport,
    enableRemoteSupport,

    _sshInfo: sshInfo
};

const assert = require('assert'),
    BoxError = require('./boxerror.js'),
    constants = require('./constants.js'),
    eventlog = require('./eventlog.js'),
    path = require('path'),
    paths = require('./paths.js'),
    safe = require('safetydance'),
    shell = require('./shell.js')('support');

// the logic here is also used in the cloudron-support tool
const AUTHORIZED_KEYS_CMD = path.join(__dirname, 'scripts/remotesupport.sh');

function sshInfo() {
    let filePath, user;

    if (constants.TEST) {
        filePath = path.join(paths.baseDir(), 'authorized_keys');
        user = process.getuid();
    } else {
        filePath = `/home/${constants.SUPPORT_USERNAME}/.ssh/authorized_keys`;
        user = constants.SUPPORT_USERNAME;
    }

    return { filePath, user };
}

async function getRemoteSupport() {
    const [error, stdoutResult] = await safe(shell.promises.sudo([ AUTHORIZED_KEYS_CMD, 'is-enabled', sshInfo().filePath ], { captureStdout: true }));
    if (error) throw new BoxError(BoxError.FS_ERROR, error);

    return stdoutResult.trim() === 'true';
}

async function enableRemoteSupport(enable, auditSource) {
    assert.strictEqual(typeof enable, 'boolean');
    assert.strictEqual(typeof auditSource, 'object');

    const si = sshInfo();
    const [error] = await safe(shell.promises.sudo([ AUTHORIZED_KEYS_CMD, enable ? 'enable' : 'disable', si.filePath, si.user ], {}));
    if (error) throw new BoxError(BoxError.FS_ERROR, error);

    await eventlog.add(eventlog.ACTION_SUPPORT_SSH, auditSource, { enable });
}
