'use strict';

exports = module.exports = {
    removePrivateFields,
    injectPrivateFields,
    upsert,
    get,
    del,
    wait,
    verifyDomainConfig
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    constants = require('../constants.js'),
    debug = require('debug')('box:dns/bunny'),
    dig = require('../dig.js'),
    dns = require('../dns.js'),
    safe = require('safetydance'),
    superagent = require('../superagent.js'),
    waitForDns = require('./waitfordns.js');

const BUNNY_API = 'https://api.bunny.net';
const RECORD_TYPES = [ 'A', 'AAAA', 'CNAME', 'TXT', 'MX', 'RDR', '???', 'PZ', 'SRV', 'CAA', 'PTR', 'SCR', 'NS' ];

function recordTypeToString(value) {
    return RECORD_TYPES[value];
}

function recordTypeToInt(value) {
    return RECORD_TYPES.indexOf(value);
}

function formatError(response) {
    return `Bunny DNS error ${response.status} ${response.text}`;
}

function removePrivateFields(domainObject) {
    domainObject.config.accessKey = constants.SECRET_PLACEHOLDER;
    return domainObject;
}

function injectPrivateFields(newConfig, currentConfig) {
    if (newConfig.accessKey === constants.SECRET_PLACEHOLDER) newConfig.accessKey = currentConfig.accessKey;
}

async function getZoneId(domainConfig, zoneName) {
    assert.strictEqual(typeof domainConfig, 'object');
    assert.strictEqual(typeof zoneName, 'string');

    const [error, response] = await safe(superagent.get(`${BUNNY_API}/dnszone?page=1&perPage=1000&search=${zoneName}`)
        .set('AccessKey', domainConfig.accessKey)
        .retry(5)
        .timeout(30 * 1000)
        .ok(() => true));
    if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
    if (response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
    if (response.status !== 200) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));

    if (!Array.isArray(response.body.Items)) throw new BoxError(BoxError.EXTERNAL_ERROR, `Invalid records in response: ${response.text}`);
    const item = response.body.Items.filter(item => item.Domain === zoneName);
    if (item.length === 0) throw new BoxError(BoxError.NOT_FOUND, 'Domain not found');

    return item[0].Id;
}

async function getDnsRecords(domainConfig, zoneName, name, type) {
    assert.strictEqual(typeof domainConfig, 'object');
    assert.strictEqual(typeof zoneName, 'string');
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof type, 'string');

    debug(`get: ${name} in zone ${zoneName} of type ${type}`);

    const zoneId = await getZoneId(domainConfig, zoneName);
    const [error, response] = await safe(superagent.get(`${BUNNY_API}/dnszone/${zoneId}`)
        .set('AccessKey', domainConfig.accessKey)
        .retry(5)
        .timeout(30 * 1000)
        .ok(() => true));
    if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
    if (response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
    if (response.status !== 200) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));
    if (!Array.isArray(response.body.Records)) throw new BoxError(BoxError.EXTERNAL_ERROR, `Invalid records in response: ${response.text}`);

    return response.body.Records.filter(r => recordTypeToString(r.Type) === type && r.Name === name);
}

async function upsert(domainObject, location, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '';

    debug(`upsert: ${name} in zone ${zoneName} of type ${type} with values ${JSON.stringify(values)}`);

    const zoneId = await getZoneId(domainConfig, zoneName);
    const records = await getDnsRecords(domainConfig, zoneName, name, type);

    // used to track available records to update instead of create
    let i = 0;
    const recordIds = [];

    for (let value of values) {
        let priority = 0;

        if (type === 'MX') {
            priority = parseInt(value.split(' ')[0], 10);
            value = value.split(' ')[1];
        } else if (type === 'TXT') {
            value = value.replace(/^"(.*)"$/, '$1'); // strip any double quotes
        }

        const data = {
            Type: recordTypeToInt(type),
            Name: name,
            Value: value,
            Priority: priority,
            Ttl: 10
        };

        if (i >= records.length) {
            const [error, response] = await safe(superagent.put(`${BUNNY_API}/dnszone/${zoneId}/records`)
                .set('AccessKey', domainConfig.accessKey)
                .send(data)
                .retry(5)
                .timeout(30 * 1000)
                .ok(() => true));
            if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
            if (response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
            if (response.status !== 201) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));
        } else {
            const [error, response] = await safe(superagent.post(`${BUNNY_API}/dnszone/${zoneId}/records/${records[i].Id}`)
                .set('AccessKey', domainConfig.accessKey)
                .send(data)
                .retry(5)
                .timeout(30 * 1000)
                .ok(() => true));
            ++i;

            if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
            if (response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
            if (response.status !== 204) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));

            recordIds.push(safe.query(records.body, 'domain_record.id'));
        }
    }

    for (let j = values.length + 1; j < records.length; j++) {
        const [error] = await safe(superagent.del(`${BUNNY_API}/dnszone/${zoneId}/records/${records[j].Id}`)
            .set('AccessKey', domainConfig.accessKey)
            .retry(5)
            .timeout(30 * 1000)
            .ok(() => true));

        if (error) debug(`upsert: error removing record ${records[j].id}: ${error.message}`);
    }

    debug('upsert: completed with recordIds:%j', recordIds);
}

async function get(domainObject, location, type) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '';

    const records = await getDnsRecords(domainConfig, zoneName, name, type);
    return records.map(r => r.Value);
}

async function del(domainObject, location, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '';

    debug(`del: ${name} in zone ${zoneName} of type ${type} with values ${JSON.stringify(values)}`);

    const zoneId = await getZoneId(domainConfig, zoneName);
    const records = await getDnsRecords(domainConfig, zoneName, name, type);
    const ids = records.filter(r => values.includes(r.Value)).map(r => r.Id);

    for (const id of ids) {
        const [error, response] = await safe(superagent.del(`${BUNNY_API}/dnszone/${zoneId}/records/${id}`)
            .set('AccessKey', domainConfig.accessKey)
            .retry(5)
            .timeout(30 * 1000)
            .ok(() => true));

        if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
        if (response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
        if (response.status === 400) continue;
        if (response.status !== 204) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));
    }
}

async function wait(domainObject, subdomain, type, value, options) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof subdomain, 'string');
    assert.strictEqual(typeof type, 'string');
    assert.strictEqual(typeof value, 'string');
    assert(options && typeof options === 'object'); // { interval: 5000, times: 50000 }

    const fqdn = dns.fqdn(subdomain, domainObject.domain);

    await waitForDns(fqdn, domainObject.zoneName, type, value, options);
}

async function verifyDomainConfig(domainObject) {
    assert.strictEqual(typeof domainObject, 'object');

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName;

    if (!domainConfig.accessKey || typeof domainConfig.accessKey !== 'string') throw new BoxError(BoxError.BAD_FIELD, 'accessKey must be a non-empty string');
    if ('customNameservers' in domainConfig && typeof domainConfig.customNameservers !== 'boolean') throw new BoxError(BoxError.BAD_FIELD, 'customNameservers must be a boolean');

    const ip = '127.0.0.1';

    const credentials = {
        accessKey: domainConfig.accessKey,
        customNameservers: !!domainConfig.customNameservers
    };

    if (constants.TEST) return credentials; // this shouldn't be here

    const [error, nameservers] = await safe(dig.resolve(zoneName, 'NS', { timeout: 5000 }));
    if (error && error.code === 'ENOTFOUND') throw new BoxError(BoxError.BAD_FIELD, 'Unable to resolve nameservers for this domain');
    if (error || !nameservers) throw new BoxError(BoxError.BAD_FIELD, error ? error.message : 'Unable to get nameservers');

    if (!nameservers.every(function (n) { return n.toLowerCase().indexOf('.bunny.net') !== -1; })) {
        debug('verifyDomainConfig: %j does not contain Bunny NS', nameservers);
        if (!domainConfig.customNameservers) throw new BoxError(BoxError.BAD_FIELD, 'Domain nameservers are not set to Bunny');
    }

    const location = 'cloudrontestdns';

    await upsert(domainObject, location, 'A', [ ip ]);
    debug('verifyDomainConfig: Test A record added');

    await del(domainObject, location, 'A', [ ip ]);
    debug('verifyDomainConfig: Test A record removed again');

    return credentials;
}
