'use strict';

exports = module.exports = {
    removePrivateFields,
    injectPrivateFields,
    upsert,
    get,
    del,
    wait,
    verifyDomainConfig
};

const assert = require('assert'),
    constants = require('../constants.js'),
    BoxError = require('../boxerror.js'),
    debug = require('debug')('box:dns/vultr'),
    dig = require('../dig.js'),
    dns = require('../dns.js'),
    safe = require('safetydance'),
    superagent = require('../superagent.js'),
    waitForDns = require('./waitfordns.js');

const VULTR_ENDPOINT = 'https://api.vultr.com/v2';

function formatError(response) {
    return `Vultr DNS error [${response.status}] ${response.text}`;
}

function removePrivateFields(domainObject) {
    domainObject.config.token = constants.SECRET_PLACEHOLDER;
    return domainObject;
}

function injectPrivateFields(newConfig, currentConfig) {
    if (newConfig.token === constants.SECRET_PLACEHOLDER) newConfig.token = currentConfig.token;
}

async function getZoneRecords(domainConfig, zoneName, name, type) {
    assert.strictEqual(typeof domainConfig, 'object');
    assert.strictEqual(typeof zoneName, 'string');
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof type, 'string');

    debug(`getInternal: getting dns records of ${zoneName} with ${name} and type ${type}`);

    const per_page = 100;
    let cursor = null, records = [];

    do {
        const url = `${VULTR_ENDPOINT}/domains/${zoneName}/records?per_page=${per_page}` + (cursor ? `&cursor=${cursor}` : '');

        const [error, response] = await safe(superagent.get(url).set('Authorization', 'Bearer ' + domainConfig.token).timeout(30 * 1000).retry(5).ok(() => true));
        if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
        if (response.status === 404) throw new BoxError(BoxError.NOT_FOUND, formatError(response));
        if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
        if (response.status !== 200) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));

        records = records.concat(response.body.records.filter(function (record) {
            return (record.type === type && record.name === name);
        }));

        cursor = safe.query(response.body, 'meta.links.next');
    } while (cursor);

    return records;
}

async function get(domainObject, location, type) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '';

    const records = await getZoneRecords(domainConfig, zoneName, name, type);
    const tmp = records.map(function (record) { return record.data; });
    return tmp;
}

async function upsert(domainObject, location, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '';

    debug('upsert: %s for zone %s of type %s with values %j', name, zoneName, type, values);

    const records = await getZoneRecords(domainConfig, zoneName, name, type);

    let i = 0;
    const recordIds = []; // used to track available records to update instead of create

    for (const value of values) {
        const data = {
            type,
            ttl: 120 // lowest
        };

        if (type === 'MX') {
            data.priority = parseInt(value.split(' ')[0], 10);
            data.data = value.split(' ')[1];
        } else if (type === 'TXT') {
            data.data = value.replace(/^"(.*)"$/, '$1'); // strip any double quotes
        } else {
            data.data = value;
        }

        if (i >= records.length) {
            data.name = name; // only set for new records

            const [error, response] = await safe(superagent.post(`${VULTR_ENDPOINT}/domains/${zoneName}/records`)
                .set('Authorization', 'Bearer ' + domainConfig.token)
                .send(data)
                .timeout(30 * 1000)
                .retry(5)
                .ok(() => true));

            if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
            if (response.status === 400) throw new BoxError(BoxError.BAD_FIELD, formatError(response));
            if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
            if (response.status !== 201) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));

            recordIds.push(response.body.record.id);
        } else {
            const [error, response] = await safe(superagent.patch(`${VULTR_ENDPOINT}/domains/${zoneName}/records/${records[i].id}`)
                .set('Authorization', 'Bearer ' + domainConfig.token)
                .send(data)
                .timeout(30 * 1000)
                .retry(5)
                .ok(() => true));

            ++i;

            if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
            if (response.status === 400) throw new BoxError(BoxError.BAD_FIELD, formatError(response));
            if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
            if (response.status !== 204) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));

            recordIds.push(records[i-1].id);
        }
    }

    for (let j = values.length + 1; j < records.length; j++) {
        const [error] = await safe(superagent.del(`${VULTR_ENDPOINT}/domains/${zoneName}/records/${records[j].id}`)
            .set('Authorization', 'Bearer ' + domainConfig.token)
            .timeout(30 * 1000)
            .retry(5));

        if (error) debug(`upsert: error removing record ${records[j].id}: ${error.message}`);
    }

    debug('upsert: completed with recordIds:%j', recordIds);
}

async function del(domainObject, location, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '';

    const records = await getZoneRecords(domainConfig, zoneName, name, type);
    if (records.length === 0) return;

    const tmp = records.filter(function (record) { return values.some(function (value) { return value === record.data; }); });
    if (tmp.length === 0) return;

    for (const r of tmp) {
        const [error, response] = await safe(superagent.del(`${VULTR_ENDPOINT}/domains/${zoneName}/records/${r.id}`)
            .set('Authorization', 'Bearer ' + domainConfig.token)
            .timeout(30 * 1000)
            .retry(5)
            .ok(() => true));

        if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
        if (response.status === 404) continue;
        if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
        if (response.status !== 204) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));
    }
}

async function wait(domainObject, subdomain, type, value, options) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof subdomain, 'string');
    assert.strictEqual(typeof type, 'string');
    assert.strictEqual(typeof value, 'string');
    assert(options && typeof options === 'object'); // { interval: 5000, times: 50000 }

    const fqdn = dns.fqdn(subdomain, domainObject.domain);

    await waitForDns(fqdn, domainObject.zoneName, type, value, options);
}

async function verifyDomainConfig(domainObject) {
    assert.strictEqual(typeof domainObject, 'object');

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName;

    if (!domainConfig.token || typeof domainConfig.token !== 'string') throw new BoxError(BoxError.BAD_FIELD, 'token must be a non-empty string');
    if ('customNameservers' in domainConfig && typeof domainConfig.customNameservers !== 'boolean') throw new BoxError(BoxError.BAD_FIELD, 'customNameservers must be a boolean');

    const ip = '127.0.0.1';

    const credentials = {
        token: domainConfig.token,
        customNameservers: !!domainConfig.customNameservers
    };

    if (constants.TEST) credentials; // this shouldn't be here

    const [error, nameservers] = await safe(dig.resolve(zoneName, 'NS', { timeout: 5000 }));
    if (error && error.code === 'ENOTFOUND') throw new BoxError(BoxError.BAD_FIELD, 'Unable to resolve nameservers for this domain');
    if (error || !nameservers) throw new BoxError(BoxError.BAD_FIELD, error ? error.message : 'Unable to get nameservers');

    if (nameservers.map(function (n) { return n.toLowerCase(); }).indexOf('ns1.vultr.com') === -1) {
        debug('verifyDomainConfig: %j does not contains vultr NS', nameservers);
        if (!domainConfig.customNameservers) throw new BoxError(BoxError.BAD_FIELD, 'Domain nameservers are not set to Vultr');
    }

    const location = 'cloudrontestdns';

    await upsert(domainObject, location, 'A', [ ip ]);
    debug('verifyDomainConfig: Test A record added');

    await del(domainObject, location, 'A', [ ip ]);
    debug('verifyDomainConfig: Test A record removed again');

    return credentials;
}
