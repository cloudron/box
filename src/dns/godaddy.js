'use strict';

exports = module.exports = {
    removePrivateFields,
    injectPrivateFields,
    upsert,
    get,
    del,
    wait,
    verifyDomainConfig
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    constants = require('../constants.js'),
    debug = require('debug')('box:dns/godaddy'),
    dig = require('../dig.js'),
    dns = require('../dns.js'),
    safe = require('safetydance'),
    superagent = require('../superagent.js'),
    waitForDns = require('./waitfordns.js');

// const GODADDY_API_OTE = 'https://api.ote-godaddy.com/v1/domains';
const GODADDY_API = 'https://api.godaddy.com/v1/domains';

function formatError(response) {
    return `GoDaddy DNS error [${response.status}] ${response.text}`;
}

function removePrivateFields(domainObject) {
    domainObject.config.apiSecret = constants.SECRET_PLACEHOLDER;
    return domainObject;
}

function injectPrivateFields(newConfig, currentConfig) {
    if (newConfig.apiSecret === constants.SECRET_PLACEHOLDER) newConfig.apiSecret = currentConfig.apiSecret;
}

async function upsert(domainObject, location, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '@';

    debug(`upsert: ${name} in zone ${zoneName} of type ${type} with values ${JSON.stringify(values)}`);

    const records = [];
    for (const value of values) {
        const record = { ttl: 600 }; // 600 is the min ttl

        if (type === 'MX') {
            record.priority = parseInt(value.split(' ')[0], 10);
            record.data = value.split(' ')[1];
        } else {
            record.data = value;
        }

        records.push(record);
    }

    const [error, response] = await safe(superagent.put(`${GODADDY_API}/${zoneName}/records/${type}/${name}`)
        .set('Authorization', `sso-key ${domainConfig.apiKey}:${domainConfig.apiSecret}`)
        .timeout(30 * 1000)
        .send(records)
        .ok(() => true));

    if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
    if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
    if (response.status === 400) throw new BoxError(BoxError.BAD_FIELD, formatError(response)); // no such zone
    if (response.status === 422) throw new BoxError(BoxError.BAD_FIELD, formatError(response)); // conflict
    if (response.status !== 200) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));
}

async function get(domainObject, location, type) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '@';

    debug(`get: ${name} in zone ${zoneName} of type ${type}`);

    const [error, response] = await safe(superagent.get(`${GODADDY_API}/${zoneName}/records/${type}/${name}`)
        .set('Authorization', `sso-key ${domainConfig.apiKey}:${domainConfig.apiSecret}`)
        .timeout(30 * 1000)
        .ok(() => true));

    if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
    if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
    if (response.status === 404) return [];
    if (response.status !== 200) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));

    const values = response.body.map(function (record) { return record.data; });

    if (values.length === 1) {
        // legacy: this was a workaround for godaddy not having a delete API
        // https://stackoverflow.com/questions/39347464/delete-record-libcloud-godaddy-api
        const GODADDY_INVALID_IP = '0.0.0.0';
        const GODADDY_INVALID_IPv6 = '0:0:0:0:0:0:0:0';
        const GODADDY_INVALID_TXT = '""';

        if ((type === 'A' && values[0] === GODADDY_INVALID_IP)
            || (type === 'AAAA' && values[0] === GODADDY_INVALID_IPv6)
            || (type === 'TXT' && values[0] === GODADDY_INVALID_TXT)) return []; // pretend this record doesn't exist
    }

    return values;
}

async function del(domainObject, location, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '@';

    debug(`del: ${name} in zone ${zoneName} of type ${type} with values ${JSON.stringify(values)}`);

    const result = await get(domainObject, location, type);
    if (result.length === 0) return;

    const tmp = result.filter(r => !values.includes(r));

    if (tmp.length) return await upsert(domainObject, location, type, tmp); // only remove 'values'

    const [error, response] = await safe(superagent.del(`${GODADDY_API}/${zoneName}/records/${type}/${name}`)
        .set('Authorization', `sso-key ${domainConfig.apiKey}:${domainConfig.apiSecret}`)
        .timeout(30 * 1000)
        .ok(() => true));

    if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
    if (response.status === 404) return;
    if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
    if (response.status !== 204) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));
}

async function wait(domainObject, subdomain, type, value, options) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof subdomain, 'string');
    assert.strictEqual(typeof type, 'string');
    assert.strictEqual(typeof value, 'string');
    assert(options && typeof options === 'object'); // { interval: 5000, times: 50000 }

    const fqdn = dns.fqdn(subdomain, domainObject.domain);

    await waitForDns(fqdn, domainObject.zoneName, type, value, options);
}

async function verifyDomainConfig(domainObject) {
    assert.strictEqual(typeof domainObject, 'object');

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName;

    if (!domainConfig.apiKey || typeof domainConfig.apiKey !== 'string') throw new BoxError(BoxError.BAD_FIELD, 'apiKey must be a non-empty string');
    if (!domainConfig.apiSecret || typeof domainConfig.apiSecret !== 'string') throw new BoxError(BoxError.BAD_FIELD, 'apiSecret must be a non-empty string');

    const ip = '127.0.0.1';

    const credentials = {
        apiKey: domainConfig.apiKey,
        apiSecret: domainConfig.apiSecret
    };

    if (constants.TEST) return credentials; // this shouldn't be here

    const [error, nameservers] = await safe(dig.resolve(zoneName, 'NS', { timeout: 5000 }));
    if (error && error.code === 'ENOTFOUND') throw new BoxError(BoxError.BAD_FIELD, 'Unable to resolve nameservers for this domain');
    if (error || !nameservers) throw new BoxError(BoxError.BAD_FIELD, error ? error.message : 'Unable to get nameservers');

    if (!nameservers.every(function (n) { return n.toLowerCase().indexOf('.domaincontrol.com') !== -1 || n.toLowerCase().indexOf('.secureserver.net') !== -1; })) {
        debug('verifyDomainConfig: %j does not contain GoDaddy NS', nameservers);
        throw new BoxError(BoxError.BAD_FIELD, 'Domain nameservers are not set to GoDaddy');
    }

    const location = 'cloudrontestdns';

    await upsert(domainObject, location, 'A', [ ip ]);
    debug('verifyDomainConfig: Test A record added');

    await del(domainObject, location, 'A', [ ip ]);
    debug('verifyDomainConfig: Test A record removed again');

    return credentials;
}
