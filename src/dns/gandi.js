'use strict';

exports = module.exports = {
    removePrivateFields,
    injectPrivateFields,
    upsert,
    get,
    del,
    wait,
    verifyDomainConfig
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    constants = require('../constants.js'),
    debug = require('debug')('box:dns/gandi'),
    dig = require('../dig.js'),
    dns = require('../dns.js'),
    safe = require('safetydance'),
    superagent = require('../superagent.js'),
    waitForDns = require('./waitfordns.js');

const GANDI_API = 'https://dns.api.gandi.net/api/v5';

function formatError(response) {
    return `Gandi DNS error [${response.status}] ${response.text}`;
}

function removePrivateFields(domainObject) {
    domainObject.config.token = constants.SECRET_PLACEHOLDER;
    return domainObject;
}

function injectPrivateFields(newConfig, currentConfig) {
    if (newConfig.token === constants.SECRET_PLACEHOLDER) newConfig.token = currentConfig.token;
}

function createRequest(method, url, domainConfig) {
    assert.strictEqual(typeof method, 'string');
    assert.strictEqual(typeof url, 'string');
    assert.strictEqual(typeof domainConfig, 'object');

    const request = superagent.request(method, url).timeout(30 * 1000).ok(() => true);

    // https://api.gandi.net/docs/authentication/
    if (domainConfig.tokenType === 'ApiKey') {
        request.set('X-Api-Key', domainConfig.token);
        request.set('Authorization', `Apikey ${domainConfig.token}`);
    } else { // PAT
        request.set('Authorization', `Bearer ${domainConfig.token}`);
    }

    return request;
}

async function upsert(domainObject, location, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '@';

    debug(`upsert: ${name} in zone ${zoneName} of type ${type} with values ${JSON.stringify(values)}`);

    const data = {
        'rrset_ttl': 300, // this is the minimum allowed
        'rrset_values': values // for mx records, value is already of the '<priority> <server>' format
    };

    const [error, response] = await safe(createRequest('PUT', `${GANDI_API}/domains/${zoneName}/records/${name}/${type}`, domainConfig)
        .send(data));

    if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
    if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
    if (response.status === 400) throw new BoxError(BoxError.BAD_FIELD, formatError(response));
    if (response.status !== 201) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));
}

async function get(domainObject, location, type) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '@';

    debug(`get: ${name} in zone ${zoneName} of type ${type}`);

    const [error, response] = await safe(createRequest('GET', `${GANDI_API}/domains/${zoneName}/records/${name}/${type}`, domainConfig));

    if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
    if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
    if (response.status === 404) return [];
    if (response.status !== 200) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));

    return response.body.rrset_values;
}

async function del(domainObject, location, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '@';

    debug(`del: ${name} in zone ${zoneName} of type ${type} with values ${JSON.stringify(values)}`);

    const [error, response] = await safe(createRequest('DELETE', `${GANDI_API}/domains/${zoneName}/records/${name}/${type}`, domainConfig));

    if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
    if (response.status === 404) return;
    if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
    if (response.status !== 204) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));
}

async function wait(domainObject, subdomain, type, value, options) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof subdomain, 'string');
    assert.strictEqual(typeof type, 'string');
    assert.strictEqual(typeof value, 'string');
    assert(options && typeof options === 'object'); // { interval: 5000, times: 50000 }

    const fqdn = dns.fqdn(subdomain, domainObject.domain);

    await waitForDns(fqdn, domainObject.zoneName, type, value, options);
}

async function verifyDomainConfig(domainObject) {
    assert.strictEqual(typeof domainObject, 'object');

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName;

    if (!domainConfig.token || typeof domainConfig.token !== 'string') throw new BoxError(BoxError.BAD_FIELD, 'token must be a non-empty string');
    if (domainConfig.tokenType !== 'PAT' && domainConfig.tokenType !== 'ApiKey') throw new BoxError(BoxError.BAD_FIELD, 'tokenType is required');
    if ('customNameservers' in domainConfig && typeof domainConfig.customNameservers !== 'boolean') throw new BoxError(BoxError.BAD_FIELD, 'customNameservers must be a boolean');

    const credentials = {
        token: domainConfig.token,
        tokenType: domainConfig.tokenType,
        customNameservers: !!domainConfig.customNameservers
    };

    const ip = '127.0.0.1';

    if (constants.TEST) return credentials; // this shouldn't be here

    const [error, nameservers] = await safe(dig.resolve(zoneName, 'NS', { timeout: 5000 }));
    if (error && error.code === 'ENOTFOUND') throw new BoxError(BoxError.BAD_FIELD, 'Unable to resolve nameservers for this domain');
    if (error || !nameservers) throw new BoxError(BoxError.BAD_FIELD, error ? error.message : 'Unable to get nameservers');

    if (!nameservers.every(function (n) { return n.toLowerCase().indexOf('.gandi.net') !== -1; })) {
        debug('verifyDomainConfig: %j does not contain Gandi NS', nameservers);
        if (!domainConfig.customNameservers) throw new BoxError(BoxError.BAD_FIELD, 'Domain nameservers are not set to Gandi');
    }

    const location = 'cloudrontestdns';

    await upsert(domainObject, location, 'A', [ ip ]);
    debug('verifyDomainConfig: Test A record added');

    await del(domainObject, location, 'A', [ ip ]);
    debug('verifyDomainConfig: Test A record removed again');

    return credentials;
}
