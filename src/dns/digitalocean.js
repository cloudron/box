'use strict';

exports = module.exports = {
    removePrivateFields,
    injectPrivateFields,
    upsert,
    get,
    del,
    wait,
    verifyDomainConfig
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    constants = require('../constants.js'),
    debug = require('debug')('box:dns/digitalocean'),
    dig = require('../dig.js'),
    dns = require('../dns.js'),
    safe = require('safetydance'),
    superagent = require('../superagent.js'),
    waitForDns = require('./waitfordns.js');

const DIGITALOCEAN_ENDPOINT = 'https://api.digitalocean.com';

function formatError(response) {
    return `DigitalOcean DNS error ${response.status} ${response.text}`;
}

function removePrivateFields(domainObject) {
    domainObject.config.token = constants.SECRET_PLACEHOLDER;
    return domainObject;
}

function injectPrivateFields(newConfig, currentConfig) {
    if (newConfig.token === constants.SECRET_PLACEHOLDER) newConfig.token = currentConfig.token;
}

async function getZoneRecords(domainConfig, zoneName, name, type) {
    assert.strictEqual(typeof domainConfig, 'object');
    assert.strictEqual(typeof zoneName, 'string');
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof type, 'string');

    let nextPage = null, matchingRecords = [];

    debug(`getInternal: getting dns records of ${zoneName} with ${name} and type ${type}`);

    do {
        const url = nextPage ? nextPage : DIGITALOCEAN_ENDPOINT + '/v2/domains/' + zoneName + '/records';

        const [error, response] = await safe(superagent.get(url)
            .set('Authorization', 'Bearer ' + domainConfig.token)
            .timeout(30 * 1000)
            .retry(5)
            .ok(() => true));

        if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
        if (response.status === 404) throw new BoxError(BoxError.NOT_FOUND, formatError(response));
        if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
        if (response.status !== 200) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));

        matchingRecords = matchingRecords.concat(response.body.domain_records.filter(function (record) {
            return (record.type === type && record.name === name);
        }));

        nextPage = (response.body.links && response.body.links.pages) ? response.body.links.pages.next : null;
    } while (nextPage);

    return matchingRecords;
}

async function upsert(domainObject, location, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '@';

    debug('upsert: %s for zone %s of type %s with values %j', name, zoneName, type, values);

    const records = await getZoneRecords(domainConfig, zoneName, name, type);

    // used to track available records to update instead of create
    let i = 0;
    const recordIds = [];

    for (let value of values) {
        let priority = null;

        if (type === 'MX') {
            priority = value.split(' ')[0];
            value = value.split(' ')[1];
        } else if (type === 'TXT') {
            value = value.replace(/^"(.*)"$/, '$1'); // strip any double quotes
        }

        const data = {
            type: type,
            name: name,
            data: value,
            priority: priority,
            ttl: 30 // Recent DO DNS API break means this value must atleast be 30
        };

        if (i >= records.length) {
            const [error, response] = await safe(superagent.post(DIGITALOCEAN_ENDPOINT + '/v2/domains/' + zoneName + '/records')
                .set('Authorization', 'Bearer ' + domainConfig.token)
                .send(data)
                .timeout(30 * 1000)
                .retry(5)
                .ok(() => true));

            if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
            if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
            if (response.status === 422) throw new BoxError(BoxError.BAD_FIELD, response.body.message);
            if (response.status !== 201) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));

            recordIds.push(safe.query(records.body, 'domain_record.id'));
        } else {
            const [error, response] = await safe(superagent.put(DIGITALOCEAN_ENDPOINT + '/v2/domains/' + zoneName + '/records/' + records[i].id)
                .set('Authorization', 'Bearer ' + domainConfig.token)
                .send(data)
                .timeout(30 * 1000)
                .retry(5)
                .ok(() => true));

            ++i;

            if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
            if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
            if (response.status === 422) throw new BoxError(BoxError.BAD_FIELD, response.body.message);
            if (response.status !== 200) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));

            recordIds.push(safe.query(records.body, 'domain_record.id'));
        }
    }

    for (let j = values.length + 1; j < records.length; j++) {
        const [error] = await safe(superagent.del(`${DIGITALOCEAN_ENDPOINT}/v2/domains/${zoneName}/records/${records[j].id}`)
            .set('Authorization', 'Bearer ' + domainConfig.token)
            .timeout(30 * 1000)
            .retry(5)
            .ok(() => true));

        if (error) debug(`upsert: error removing record ${records[j].id}: ${error.message}`);
    }

    debug('upsert: completed with recordIds:%j', recordIds);
}

async function get(domainObject, location, type) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '@';

    const result = await getZoneRecords(domainConfig, zoneName, name, type);

    const tmp = result.map(function (record) { return record.data; });
    return tmp;
}

async function del(domainObject, location, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '@';

    const result = await getZoneRecords(domainConfig, zoneName, name, type);
    if (result.length === 0) return;

    const tmp = result.filter(function (record) { return values.some(function (value) { return value === record.data; }); });
    if (tmp.length === 0) return;

    for (const r of tmp) {
        const [error, response] = await safe(superagent.del(`${DIGITALOCEAN_ENDPOINT}/v2/domains/${zoneName}/records/${r.id}`)
            .set('Authorization', 'Bearer ' + domainConfig.token)
            .timeout(30 * 1000)
            .retry(5)
            .ok(() => true));

        if (error) throw new BoxError(BoxError.NETWORK_ERROR, error);
        if (response.status === 404) return;
        if (response.status === 403 || response.status === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
        if (response.status !== 204) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));
    }
}

async function wait(domainObject, subdomain, type, value, options) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof subdomain, 'string');
    assert.strictEqual(typeof type, 'string');
    assert.strictEqual(typeof value, 'string');
    assert(options && typeof options === 'object'); // { interval: 5000, times: 50000 }

    const fqdn = dns.fqdn(subdomain, domainObject.domain);

    await waitForDns(fqdn, domainObject.zoneName, type, value, options);
}

// https://stackoverflow.com/questions/14313183/javascript-regex-how-do-i-check-if-the-string-is-ascii-only
function isASCII(str) {
    // eslint-disable-next-line no-control-regex
    return /^[\x00-\x7F]*$/.test(str);
}

async function verifyDomainConfig(domainObject) {
    assert.strictEqual(typeof domainObject, 'object');

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName;

    if (!domainConfig.token || typeof domainConfig.token !== 'string') throw new BoxError(BoxError.BAD_FIELD, 'token must be a non-empty string');
    if (!isASCII(domainConfig.token)) throw new BoxError(BoxError.BAD_FIELD, 'token contains invalid characters');
    if ('customNameservers' in domainConfig && typeof domainConfig.customNameservers !== 'boolean') throw new BoxError(BoxError.BAD_FIELD, 'customNameservers must be a boolean');

    const ip = '127.0.0.1';

    const credentials = {
        token: domainConfig.token,
        customNameservers: !!domainConfig.customNameservers
    };

    if (constants.TEST) return credentials; // this shouldn't be here

    const [error, nameservers] = await safe(dig.resolve(zoneName, 'NS', { timeout: 5000 }));
    if (error && error.code === 'ENOTFOUND') throw new BoxError(BoxError.BAD_FIELD, 'Unable to resolve nameservers for this domain');
    if (error || !nameservers) throw new BoxError(BoxError.BAD_FIELD, error ? error.message : 'Unable to get nameservers');

    if (nameservers.map(function (n) { return n.toLowerCase(); }).indexOf('ns1.digitalocean.com') === -1) {
        debug('verifyDomainConfig: %j does not contains DO NS', nameservers);
        if (!domainConfig.customNameservers) throw new BoxError(BoxError.BAD_FIELD, 'Domain nameservers are not set to DigitalOcean');
    }

    const location = 'cloudrontestdns';

    await upsert(domainObject, location, 'A', [ ip ]);
    debug('verifyDomainConfig: Test A record added');

    await del(domainObject, location, 'A', [ ip ]);
    debug('verifyDomainConfig: Test A record removed again');

    return credentials;
}
