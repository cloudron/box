#!/bin/bash

set -eu

# root is required for logPaths to work
if [[ ${EUID} -ne 0 ]]; then
    echo "This script should be run as root." > /dev/stderr
    exit 1
fi

if [[ "${1:-}" == "--check" ]]; then
    echo "OK"
    exit 0
fi

args=$(getopt -o "" -l "follow,lines:" -n "$0" -- "$@")
eval set -- "${args}"

follow=""
lines=""

while true; do
    case "$1" in
    --follow) follow="--follow --retry --quiet"; shift;; # same as -F. to make it work if file doesn't exist, --quiet to not output file headers, which are no logs
    --lines) lines="$2"; shift 2;;
    --) break;;
    *) echo "Unknown option $1"; exit 1;;
    esac
done

# first sort the existing log lines
tail --quiet --lines=${lines} "$@"  | sort -k1 || true # ignore error if files are missing

exec tail ${follow} --lines=0 "$@"
