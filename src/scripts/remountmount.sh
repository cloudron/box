#!/bin/bash

set -eu -o pipefail

if [[ ${EUID} -ne 0 ]]; then
    echo "This script should be run as root." > /dev/stderr
    exit 1
fi

if [[ $# -eq 0 ]]; then
    echo "No arguments supplied"
    exit 1
fi

if [[ "$1" == "--check" ]]; then
    echo "OK"
    exit 0
fi

host_path="$1"

# mount units must be named after the mount point directories they control
mount_filename=$(systemd-escape -p --suffix=mount "$host_path")
mount_file="/etc/systemd/system/${mount_filename}"

# stop and start will do the remount
systemctl stop "${mount_filename}"
systemctl start "${mount_filename}"

echo "Remount succeeded"
