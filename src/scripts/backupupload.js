#!/usr/bin/env node

'use strict';

if (process.argv[2] === '--check') {
    console.log('OK');
    process.exit(0);
}

const backuptask = require('../backuptask.js'),
    database = require('../database.js'),
    debug = require('debug')('box:backupupload'),
    safe = require('safetydance');

// Main process starts here
const remotePath = process.argv[2];
const format = process.argv[3];
const dataLayoutString = process.argv[4];

debug(`Backing up ${dataLayoutString} to ${remotePath}`);

process.on('SIGTERM', function () {
    process.exit(0);
});

// this can happen when the backup task is terminated (not box code)
process.on('disconnect', function () {
    debug('parent process died');
    process.exit(0);
});

// send progress every n seconds
function throttledProgressCallback(msecs) {
    let lastProgress = null;

    return function (progress) {
        const now = Date.now();
        if (lastProgress && ((now - lastProgress) < msecs)) return;
        process.send(progress);
        lastProgress = now;
    };
}

(async function main() {
    await database.initialize();

    const [uploadError] = await safe(backuptask.upload(remotePath, format, dataLayoutString, throttledProgressCallback(5000)));
    debug('upload completed. error: %o', uploadError);

    process.send({ result: uploadError ? uploadError.message : '' });

    // https://nodejs.org/api/process.html are exit codes used by node. apps.js uses the value below
    // to check apptask crashes
    process.exit(uploadError ? 50 : 0);
})();
