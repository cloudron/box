#!/bin/bash

set -eu -o pipefail

if [[ ${EUID} -ne 0 ]]; then
    echo "This script should be run as root." > /dev/stderr
    exit 1
fi

if [[ $# -eq 0 ]]; then
    echo "No arguments supplied"
    exit 1
fi

if [[ "$1" == "--check" ]]; then
    echo "OK"
    exit 0
fi

CLOUDRON_SUPPORT_PUBLIC_KEY="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGWS+930b8QdzbchGljt3KSljH9wRhYvht8srrtQHdzg support@cloudron.io"

cmd="$1"
keys_file="$2"
user="${3:-1000}"

if [[ "$1" == "is-enabled" ]]; then
    if grep -q "${CLOUDRON_SUPPORT_PUBLIC_KEY}" "${keys_file}"; then
        echo "true"
    else
        echo "false"
    fi
elif [[ "$1" == "enable" ]]; then
    mkdir -p $(dirname "${keys_file}")       # .ssh does not exist sometimes
    touch "${keys_file}"                # required for concat to work
    if ! grep -q "${CLOUDRON_SUPPORT_PUBLIC_KEY}" "${keys_file}"; then
        echo -e "\n${CLOUDRON_SUPPORT_PUBLIC_KEY}" >> "${keys_file}"
        chmod 600 "${keys_file}"
        chown "${user}" "${keys_file}"
    fi
elif [[ "$1" == "disable" ]]; then
    if [[ -f "${keys_file}" ]]; then
        sed -e "/ support@cloudron.io$/d" -i "${keys_file}"
    fi
fi

