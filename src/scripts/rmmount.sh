#!/bin/bash

set -eu -o pipefail

if [[ ${EUID} -ne 0 ]]; then
    echo "This script should be run as root." > /dev/stderr
    exit 1
fi

if [[ $# -eq 0 ]]; then
    echo "No arguments supplied"
    exit 1
fi

if [[ "$1" == "--check" ]]; then
    echo "OK"
    exit 0
fi

where="$1"

mount_filename=$(systemd-escape -p --suffix=mount "$where")

systemctl stop "${mount_filename}" || true
rm -f "/etc/systemd/system/${mount_filename}"
rmdir "${where}" || true
systemctl daemon-reload
