#!/bin/bash

set -eu -o pipefail

if [[ ${EUID} -ne 0 ]]; then
    echo "This script should be run as root." > /dev/stderr
    exit 1
fi

if [[ $# -eq 0 ]]; then
    echo "No arguments supplied"
    exit 1
fi

if [[ "$1" == "--check" ]]; then
    echo "OK"
    exit 0
fi

[[ "${BOX_ENV}" != "cloudron" ]] && exit

service="$1"

if [[ "${service}" == "unbound" ]]; then
    systemctl restart --no-block unbound
elif [[ "${service}" == "nginx" ]]; then
    if systemctl -q is-active nginx; then
        nginx -s reload
    else
        systemctl restart --no-block nginx
    fi
elif [[ "${service}" == "docker" ]]; then
    systemctl restart --no-block docker
elif [[ "${service}" == "collectd" ]]; then
    systemctl restart --no-block collectd
elif [[ "${service}" == "box" ]]; then
    readonly ubuntu_version=$(lsb_release -rs)
    if [[ "${ubuntu_version}" == "18.04" ]]; then
        pid=$(systemctl show box -p MainPID | sed 's/MainPID=//g')
        kill -HUP $pid
    else
        systemctl reload --no-block box
    fi
else
    echo "Unknown service ${service}"
    exit 1
fi

