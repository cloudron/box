#!/bin/bash

set -eu -o pipefail

if [[ ${EUID} -ne 0 ]]; then
    echo "This script should be run as root." > /dev/stderr
    exit 1
fi

readonly updater_service="cloudron-updater"

if [[ $# == 1 && "$1" == "--check" ]]; then
    echo "OK"
    exit 0
fi

if [[ $# != 2 ]]; then
    echo "Usage: update.sh <sourceDir> <logFile> arguments required"
    exit 1
fi

function log() {
  echo -e "$(date +'%Y-%m-%dT%H:%M:%S')" "==> update: $1"
}

readonly source_dir="${1}"
readonly log_file="${2}"

readonly installer_path="${source_dir}/scripts/installer.sh"
readonly ubuntu_version=$(lsb_release -rs)

log "updating Cloudron with ${source_dir}"

# StandardError will follow StandardOutput in default inherit mode. https://www.freedesktop.org/software/systemd/man/systemd.exec.html
systemctl reset-failed "${updater_service}" 2>/dev/null || true
[[ "${ubuntu_version}" == "18.04" ]] && file_mode="file" || file_mode="append"
if ! systemd-run --property=OOMScoreAdjust=-1000 --unit "${updater_service}" -p StandardOutput=${file_mode}:${log_file} ${installer_path}; then
    log "Failed to install cloudron"
    exit 1
fi

while true; do
    if systemctl is-failed "${updater_service}" >/dev/null 2>&1; then
        log "${updater_service} failed"
        exit 1
    fi

    log "${updater_service} is still active. will check in 5 seconds"

    sleep 5
    # this loop will stop once the update process stopped the box unit and thus terminating this child process
done
