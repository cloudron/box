'use strict';

// WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING
// These constants are used in the installer script as well
// Do not require anything here!

exports = module.exports = {
    // a version change recreates all containers with latest docker config
    'version': '49.8.0',

    // a major version bump in the db containers will trigger the restore logic that uses the db dumps
    // docker inspect --format='{{index .RepoDigests 0}}' $IMAGE to get the sha256 . note this has registry in it because manifest id is registry specific!
    'images': {
        // 'base': 'registry.docker.com/cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c',
        'graphite': 'registry.docker.com/cloudron/graphite:3.5.0@sha256:ee7c9dc49a6507cb3e3cee25495b2044908feb91dac5df87a9633dea38fdeb8a',
        'mail': 'registry.docker.com/cloudron/mail:3.15.0@sha256:c93b5a83fc4e775bda4e05010bd19e5a658936e7a09cf7e51281e3696fde4536',
        'mongodb': 'registry.docker.com/cloudron/mongodb:6.1.0@sha256:e0eae0335546310d9b4fd60b225adbfa07f60204e766275a99c083726f7453fe',
        'mysql': 'registry.docker.com/cloudron/mysql:3.5.0@sha256:969ea5b2f91861940ca6309c7676c52e479d2a864ba3aabd08a4266799707280',
        'postgresql': 'registry.docker.com/cloudron/postgresql:6.0.0@sha256:197ca3747502b8f924134b55362ddd88c9924093de40730b6078858f45c2b020',
        'redis': 'registry.docker.com/cloudron/redis:3.6.0@sha256:cd240086189f4b1467b7ae3496d0fbd14becd5c4280fdcac1e565f82f2f7da62',
        'sftp': 'registry.docker.com/cloudron/sftp:3.9.1@sha256:aa67eb58a957cbaf09fdcb332aa56575821a4246059448cf39aeca8faba1fb4b',
        'turn': 'registry.docker.com/cloudron/turn:1.8.0@sha256:cdbe83c3c83b8f25de3a5814b121eb941b457dca7127d2e6ff446c7a0cfa1570',
    }
};
