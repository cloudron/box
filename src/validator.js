'use strict';

exports = module.exports = {
    isEmail
};

const assert = require('assert');

// this currently does not match: "john.doe"@example.com, user@[192.168.1.1], john.doe(comment)@example.com or 用户@例子.世界
function isEmail(email) {
    assert.strictEqual(typeof email, 'string');

  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  return emailRegex.test(email);
}

