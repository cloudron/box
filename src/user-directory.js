'use strict';

exports = module.exports = {
    getProfileConfig,
    setProfileConfig
};

const assert = require('assert'),
    BoxError = require('./boxerror.js'),
    constants = require('./constants.js'),
    debug = require('debug')('box:user-directory'),
    eventlog = require('./eventlog.js'),
    oidc = require('./oidc.js'),
    settings = require('./settings.js'),
    tokens = require('./tokens.js'),
    users = require('./users.js');

async function getProfileConfig() {
    const value = await settings.getJson(settings.PROFILE_CONFIG_KEY);
    return value || { lockUserProfiles: false, mandatory2FA: false };
}

async function setProfileConfig(profileConfig, options, auditSource) {
    assert.strictEqual(typeof profileConfig, 'object');
    assert.strictEqual(typeof options, 'object');
    assert(auditSource && typeof auditSource === 'object');

    if (constants.DEMO) throw new BoxError(BoxError.BAD_STATE, 'Not allowed in demo mode');

    const oldConfig = await getProfileConfig();
    await settings.setJson(settings.PROFILE_CONFIG_KEY, profileConfig);

    await eventlog.add(eventlog.ACTION_USER_DIRECTORY_PROFILE_CONFIG_UPDATE, auditSource, { oldConfig, config: profileConfig });

    if (profileConfig.mandatory2FA && !oldConfig.mandatory2FA) {
        debug('setProfileConfig: logging out non-2FA users to enforce 2FA');

        const allUsers = await users.list();

        for (const user of allUsers) {
            if (user.twoFactorAuthenticationEnabled) continue;
            if (options.persistUserIdSessions === user.id) continue; // do not logout the API caller

            await tokens.delByUserIdAndType(user.id, tokens.ID_WEBADMIN);
            await oidc.revokeByUserId(user.id);
        }
    }
}
