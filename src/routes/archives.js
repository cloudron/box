'use strict';

exports = module.exports = {
    load,

    list,
    get,
    getIcon,
    del,
    unarchive
};

const apps = require('../apps.js'),
    assert = require('assert'),
    archives = require('../archives.js'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance');

async function load(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    const [error, result] = await safe(archives.get(req.params.id));
    if (error) return next(BoxError.toHttpError(error));
    if (!result) return next(new HttpError(404, 'Backup not found'));

    req.resource = result;

    next();
}

async function list(req, res, next) {
    const page = typeof req.query.page !== 'undefined' ? parseInt(req.query.page) : 1;
    if (!page || page < 0) return next(new HttpError(400, 'page query param has to be a postive number'));

    const perPage = typeof req.query.per_page !== 'undefined'? parseInt(req.query.per_page) : 25;
    if (!perPage || perPage < 0) return next(new HttpError(400, 'per_page query param has to be a postive number'));

    const [error, result] = await safe(archives.list(page, perPage));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { archives: result }));
}

async function get(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    next(new HttpSuccess(200, req.resource));
}

async function getIcon(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');
    assert.strictEqual(typeof req.resource, 'object');

    const [error, icon] = await safe(archives.getIcon(req.params.id, { original: req.query.original }));
    if (error) return next(BoxError.toHttpError(error));

    res.send(icon);
}

async function del(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');
    assert.strictEqual(typeof req.resource, 'object');

    const [error] = await safe(archives.del(req.resource, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204));
}

async function unarchive(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');
    assert.strictEqual(typeof req.resource, 'object');
    assert.strictEqual(typeof req.body, 'object');

    const data = req.body;

    // required
    if (typeof data.subdomain !== 'string') return next(new HttpError(400, 'subdomain is required'));
    if (typeof data.domain !== 'string') return next(new HttpError(400, 'domain is required'));

    // optional
    if (('ports' in data) && typeof data.ports !== 'object') return next(new HttpError(400, 'ports must be an object'));

    if ('secondaryDomains' in data) {
        if (!data.secondaryDomains || typeof data.secondaryDomains !== 'object') return next(new HttpError(400, 'secondaryDomains must be an object'));
        if (Object.keys(data.secondaryDomains).some(function (key) { return typeof data.secondaryDomains[key].domain !== 'string' || typeof data.secondaryDomains[key].subdomain !== 'string'; })) return next(new HttpError(400, 'secondaryDomain object must contain domain and subdomain strings'));
    }

    const [error, result] = await safe(apps.unarchive(req.resource, req.body, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, { id: result.id, taskId: result.taskId }));
}
