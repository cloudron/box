'use strict';

exports = module.exports = {
    getProfileConfig,
    setProfileConfig
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance'),
    userDirectory = require('../user-directory.js');

async function getProfileConfig(req, res, next) {
    const [error, directoryConfig] = await safe(userDirectory.getProfileConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, directoryConfig));
}

async function setProfileConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.lockUserProfiles !== 'boolean') return next(new HttpError(400, 'lockUserProfiles is required'));
    if (typeof req.body.mandatory2FA !== 'boolean') return next(new HttpError(400, 'mandatory2FA is required'));

    const [error] = await safe(userDirectory.setProfileConfig(req.body, { persistUserIdSessions: req.user.id }, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}
