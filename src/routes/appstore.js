'use strict';

exports = module.exports = {
    getApps,
    getApp,
    getAppVersion,

    registerCloudronWithSetupToken,
    registerCloudronWithLogin,
    getSubscription
};

const appstore = require('../appstore.js'),
    assert = require('assert'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance'),
    users = require('../users.js'),
    _ = require('../underscore.js');

async function getApps(req, res, next) {
    const [error, apps] = await safe(appstore.getApps());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { apps }));
}

async function getApp(req, res, next) {
    assert.strictEqual(typeof req.params.appstoreId, 'string');

    const [error, app] = await safe(appstore.getApp(req.params.appstoreId));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, app));
}

async function getAppVersion(req, res, next) {
    assert.strictEqual(typeof req.params.appstoreId, 'string');
    assert.strictEqual(typeof req.params.versionId, 'string');

    const [error, manifest] = await safe(appstore.getAppVersion(req.params.appstoreId, req.params.versionId));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, manifest));
}

async function registerCloudronWithSetupToken(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.setupToken !== 'string') return next(new HttpError(400, 'setupToken must be a string'));

    const [error] = await safe(appstore.registerCloudronWithSetupToken({ setupToken: req.body.setupToken }));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(201, {}));
}

async function registerCloudronWithLogin(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.email !== 'string' || !req.body.email) return next(new HttpError(400, 'email must be string'));
    if (typeof req.body.password !== 'string' || !req.body.password) return next(new HttpError(400, 'password must be string'));
    if ('totpToken' in req.body && typeof req.body.totpToken !== 'string') return next(new HttpError(400, 'totpToken must be string'));
    if (typeof req.body.signup !== 'boolean') return next(new HttpError(400, 'signup must be a boolean'));

    const [error] = await safe(appstore.registerCloudronWithLogin(req.body));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(201, {}));
}

async function getSubscription(req, res, next) {
    const [error, result] = await safe(appstore.getSubscription());
    if (error) return next(BoxError.toHttpError(error));

    // non-owners only get a stripped down version
    if (users.compareRoles(req.user.role, users.ROLE_OWNER) < 0) next(new HttpSuccess(200, _.pick(result, ['plan', 'status'])));
    else next(new HttpSuccess(200, result)); // { email, cloudronId, cloudronCreatedAt, plan, current_period_end, canceled_at, cancel_at, status, features }
}
