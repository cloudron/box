'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js'),
    expect = require('expect.js'),
    safe = require('safetydance'),
    superagent = require('../../superagent.js');

describe('Volumes API', function () {
    const { setup, cleanup, serverUrl, owner } = common;

    before(setup);
    after(cleanup);
    let volumeId;

    it('cannot create volume with bad name', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/volumes`)
            .query({ access_token: owner.token })
            .send({ name: 'music#/ ', mountType: 'filesystem', mountOptions: { hostPath: '/media/cloudron-test-music' } })
            .ok(() => true);
        expect(response.status).to.equal(400);
    });

    it('cannot create volume with bad path', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/volumes`)
            .query({ access_token: owner.token })
            .send({ name: 'music', mountType: 'filesystem', mountOptions: { hostPath: '/tmp/music' } })
            .ok(() => true);
        expect(response.status).to.equal(400);
    });

    it('can create volume', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/volumes`)
            .query({ access_token: owner.token })
            .send({ name: 'music', mountType: 'filesystem', mountOptions: { hostPath: '/media/cloudron-test-music' }  })
            .ok(() => true);

        expect(response.status).to.equal(201);
        expect(response.body.id).to.be.a('string');
        volumeId = response.body.id;
    });

    it('can list volumes', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/volumes`)
            .query({ access_token: owner.token });
        expect(response.status).to.equal(200);
        expect(response.body.volumes.length).to.be(1);
        expect(response.body.volumes[0].id).to.be(volumeId);
        expect(response.body.volumes[0].hostPath).to.be('/media/cloudron-test-music');
    });

    it('cannot get non-existent volume', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/volumes/foobar`)
            .query({ access_token: owner.token })
            .ok(() => true);
        expect(response.status).to.equal(404);
    });

    it('can get volume', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/volumes/${volumeId}`)
            .query({ access_token: owner.token });
        expect(response.status).to.equal(200);
        expect(response.body.id).to.be(volumeId);
        expect(response.body.hostPath).to.be('/media/cloudron-test-music');
    });

    it('cannot update volume', async function () {
        let [error, response] = await safe(superagent.post(`${serverUrl}/api/v1/volumes/${volumeId}`)
            .query({ access_token: owner.token })
            .send({ mountOptions: { hostPath: '/media/cloudron-test-music-2' }})
            .ok(() => true));

        expect(response.status).to.equal(400); // cannot update filesytem
    });

    it('can delete volume', async function () {
        const response = await superagent.del(`${serverUrl}/api/v1/volumes/${volumeId}`)
            .query({ access_token: owner.token });
        expect(response.status).to.equal(204);
    });
});
