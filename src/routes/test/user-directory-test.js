/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

describe('User Directory API', function () {
    const { setup, cleanup, serverUrl, owner, user } = common;

    before(setup);
    after(cleanup);

    describe('profile config', function () {
        it('get default profile config', async function() {
            const response = await superagent.get(`${serverUrl}/api/v1/user_directory/profile_config`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(200);
            expect(response.body.lockUserProfiles).to.be(false);
            expect(response.body.mandatory2FA).to.be(false);
        });

        it('cannot set profile config without mandatory2FA', async function() {
            const response = await superagent.post(`${serverUrl}/api/v1/user_directory/profile_config`)
                .query({ access_token: owner.token })
                .send({ lockUserProfiles: true })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot set as normal user', async function() {
            const response = await superagent.post(`${serverUrl}/api/v1/user_directory/profile_config`)
                .query({ access_token: user.token })
                .send({ lockUserProfiles: true, mandatory2FA: true })
                .ok(() => true);

            expect(response.status).to.equal(403);
        });

        it('can lock user profile', async function() {
            const response = await superagent.post(`${serverUrl}/api/v1/user_directory/profile_config`)
                .query({ access_token: owner.token })
                .send({ lockUserProfiles: true, mandatory2FA: false })
                .ok(() => true);

            expect(response.status).to.equal(200);

            const response2 = await superagent.post(`${serverUrl}/api/v1/profile/email`)
                .query({ access_token: owner.token })
                .send({ email: 'newemail@example.Com', password: owner.password })
                .ok(() => true);

            expect(response2.status).to.equal(403); // profile is locked
        });

        it('can set mandatory 2fa', async function() {
            const response = await superagent.post(`${serverUrl}/api/v1/user_directory/profile_config`)
                .query({ access_token: owner.token })
                .send({ lockUserProfiles: true, mandatory2FA: true })
                .ok(() => true);

            expect(response.status).to.equal(200);

            // token gets revoked!
            const response2 = await superagent.get(`${serverUrl}/api/v1/profile`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response2.status).to.equal(200); // token is not gone, since it is persisted

            const response3 = await superagent.get(`${serverUrl}/api/v1/profile`)
                .query({ access_token: user.token })
                .ok(() => true);

            expect(response3.status).to.equal(401); // token is gone
        });
    });
});
