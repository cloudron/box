'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js'),
    constants = require('../../constants.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

describe('Updater API', function () {
    const { setup, cleanup, serverUrl, owner } = common;

    before(setup);
    after(cleanup);

    describe('autoupdate_pattern', function () {
        it('can get app auto update pattern (default)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/updater/autoupdate_pattern`)
                .query({ access_token: owner.token });
            expect(response.status).to.equal(200);
            expect(response.body.pattern).to.be.ok();
        });

        it('cannot set autoupdate_pattern without pattern', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/updater/autoupdate_pattern`)
                .query({ access_token: owner.token })
                .ok(() => true);
            expect(response.status).to.equal(400);
        });

        it('can set autoupdate_pattern', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/updater/autoupdate_pattern`)
                .query({ access_token: owner.token })
                .send({ pattern: '00 30 11 * * 1-5' });
            expect(response.status).to.equal(200);
        });

        it('can get auto update pattern', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/updater/autoupdate_pattern`)
                .query({ access_token: owner.token });
            expect(response.status).to.equal(200);
            expect(response.body.pattern).to.be('00 30 11 * * 1-5');
        });

        it('can set autoupdate_pattern to never', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/updater/autoupdate_pattern`)
                .query({ access_token: owner.token })
                .send({ pattern: constants.AUTOUPDATE_PATTERN_NEVER });
            expect(response.status).to.equal(200);
        });

        it('can get auto update pattern', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/updater/autoupdate_pattern`)
                .query({ access_token: owner.token });
            expect(response.status).to.equal(200);
            expect(response.body.pattern).to.be(constants.AUTOUPDATE_PATTERN_NEVER);
        });

        it('cannot set invalid autoupdate_pattern', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/updater/autoupdate_pattern`)
                .query({ access_token: owner.token })
                .send({ pattern: '1 3 x 5 6' })
                .ok(() => true);
            expect(response.status).to.equal(400);
        });
    });
});
