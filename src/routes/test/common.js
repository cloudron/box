'use strict';

const apps = require('../../apps.js'),
    appstore = require('../../appstore.js'),
    debug = require('debug')('box:test/common'),
    constants = require('../../constants.js'),
    database = require('../../database.js'),
    expect = require('expect.js'),
    fs = require('fs'),
    mailer = require('../../mailer.js'),
    oidc = require('../../oidc.js'),
    safe = require('safetydance'),
    server = require('../../server.js'),
    settings = require('../../settings.js'),
    support = require('../../support.js'),
    superagent = require('../../superagent.js'),
    tasks = require('../../tasks.js'),
    timers = require('timers/promises'),
    tokens = require('../../tokens.js');

const manifest = {
    'id': 'io.cloudron.test',
    'author': 'The Presidents Of the United States Of America',
    'title': 'test title',
    'description': 'test description',
    'tagline': 'test rocks',
    'website': 'http://test.cloudron.io',
    'contactEmail': 'test@cloudron.io',
    'version': '0.1.0',
    'manifestVersion': 2,
    'dockerImage': 'cloudron/test:25.2.0',
    'healthCheckPath': '/',
    'httpPort': 7777,
    'tcpPorts': {
        'ECHO_SERVER_PORT': {
            'title': 'Echo Server Port',
            'description': 'Echo server',
            'containerPort': 7778
        }
    },
    'addons': {
        'oauth': { },
        'redis': { },
        'mysql': { },
        'postgresql': { }
    }
};

exports = module.exports = {
    setup,
    setupServer,
    cleanup,
    clearMailQueue,
    checkMails,
    waitForTask,

    owner: {
        id: null,
        username: 'superadmin',
        password: 'Foobar?1337',
        email: 'superadmin@cloudron.local',
        displayName: 'Super Admin',
        token: null
    },

    admin: {
        id: null,
        username: 'administrator',
        password: 'Foobar?1339',
        email: 'admin@cloudron.local',
        token: null
    },

    user: {
        id: null,
        username: 'user',
        password: 'Foobar?1338',
        email: 'user@cloudron.local',
        token: null
    },

    app: {
        id: 'appid',
        appStoreId: 'appStoreId',
        installationState: apps.ISTATE_PENDING_INSTALL,
        runState: 'running',
        subdomain: 'app',
        domain: 'test.example.com',
        fqdn: 'app.test.example.com',
        manifest,
        containerId: 'someid',
        portBindings: {},
        accessRestriction: null,
        memoryLimit: 0,
        mailboxDomain: 'test.example.com',
        secondaryDomains: [],
        redirectDomains: [],
        aliasDomains: []
    },

    mockApiServerOrigin: 'http://localhost:6060',
    dashboardDomain: 'test.example.com',
    dashboardFqdn: 'my.test.example.com',
    appstoreToken: 'toktok',
    mailFqdn: 'my.test.example.com',

    serverUrl: `http://localhost:${constants.PORT}`,

    auditSource: { ip: '5.6.7.8' }
};

async function setupServer() {
    debug('Setting up server');
    await database.initialize();
    await database._clear();
    await appstore._setApiServerOrigin(exports.mockApiServerOrigin);
    await oidc.stop();
    await server.start();
    debug('Set up server complete');
}

async function setup() {
    debug('Setting up');

    const owner = exports.owner, serverUrl = exports.serverUrl, user = exports.user, admin = exports.admin;

    await setupServer();
    await safe(fs.promises.unlink(support._sshInfo().filePath));

    // setup
    let response = await superagent.post(`${serverUrl}/api/v1/provision/setup`)
        .send({ domainConfig: { provider: 'noop', domain: exports.dashboardDomain, config: {}, tlsConfig: { provider: 'fallback' } } });
    expect(response.status).to.eql(200);

    await timers.setTimeout(2000);

    // create admin
    response = await superagent.post(`${serverUrl}/api/v1/provision/activate`)
        .query({ setupToken: 'somesetuptoken' })
        .send({ username: owner.username, password: owner.password, email: owner.email });
    expect(response.status).to.eql(201);
    owner.token = response.body.token;
    owner.id = response.body.userId;

    // create an admin
    response = await superagent.post(`${serverUrl}/api/v1/users`)
        .query({ access_token: owner.token })
        .send({ username: admin.username, email: admin.email, password: admin.password });
    expect(response.status).to.equal(201);
    admin.id = response.body.id;
    // HACK to get a token for second user (passwords are generated and the user should have gotten a password setup link...)
    const token1 = await tokens.add({ identifier: admin.id, clientId: tokens.ID_WEBADMIN, expires: Date.now() + (60 * 60 * 1000), name: 'fromtest', allowedIpRanges: '' });
    admin.token = token1.accessToken;

    // create user
    response = await superagent.post(`${serverUrl}/api/v1/users`)
        .query({ access_token: owner.token })
        .send({ username: user.username, email: user.email, password: user.password });
    expect(response.status).to.equal(201);
    user.id = response.body.id;
    // HACK to get a token for second user (passwords are generated and the user should have gotten a password setup link...)
    const token2 = await tokens.add({ identifier: user.id, clientId: tokens.ID_WEBADMIN, expires: Date.now() + (60 * 60 * 1000), name: 'fromtest', allowedIpRanges: '' });
    user.token = token2.accessToken;

    // create app object
    await apps.add(exports.app.id, exports.app.appStoreId, exports.app.manifest, exports.app.subdomain, exports.app.domain, exports.app.portBindings, exports.app);

    await settings._set(settings.APPSTORE_API_TOKEN_KEY, exports.appstoreToken); // appstore token

    debug('Setup complete');
}

async function cleanup() {
    debug('Cleaning up');
    await database._clear();
    await server.stop();
    await oidc.stop();
    debug('Cleaned up');
}

function clearMailQueue() {
    mailer._mailQueue = [];
}

async function checkMails(number) {
    await timers.setTimeout(1000);
    expect(mailer._mailQueue.length).to.equal(number);
    clearMailQueue();
}

async function waitForTask(taskId) {
    debug(`Waiting for task: ${taskId}`);

    // eslint-disable-next-line no-constant-condition
    for (let i = 0; i < 30; i++) {
        const result = await tasks.get(taskId);
        expect(result).to.not.be(null);
        if (!result.active) {
            if (result.success) return result;
            throw new Error(`Task ${taskId} failed: ${result.error.message} - ${result.error.stack}`);
        }
        await timers.setTimeout(2000);
        debug(`Waiting for task to ${taskId} finish`);
    }
    throw new Error(`Task ${taskId} never finished`);
}
