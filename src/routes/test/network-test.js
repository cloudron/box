'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

describe('Network API', function () {
    const { setup, cleanup, serverUrl, owner } = common;

    before(setup);
    after(cleanup);

    describe('dynamic dns', function () {
        it('get default succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/network/dynamic_dns`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.enabled).to.be(false);
        });

        it('cannot set without enabled', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/network/dynamic_dns`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot set', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/network/dynamic_dns`)
                .query({ access_token: owner.token })
                .send({ enabled: true })
                .ok(() => true);

            expect(response.status).to.equal(200);
        });

        it('get succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/network/dynamic_dns`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.enabled).to.be(true);
        });
    });
});
