'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

describe('External LDAP API', function () {
    const { setup, cleanup, serverUrl, owner } = common;

    before(setup);
    after(cleanup);

    describe('external_ldap', function () {
        // keep in sync with defaults in settings.js
        let defaultConfig = { provider: 'noop', autoCreate: false };

        it('can get external_ldap (default)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/external_ldap/config`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body).to.eql(defaultConfig);
        });

        it('can set external_ldap to noop', async function () {
            const config = {
                provider: 'noop'
            };
            const response = await superagent.post(`${serverUrl}/api/v1/external_ldap/config`)
                .query({ access_token: owner.token })
                .send(config);

            expect(response.status).to.equal(200);
        });
    });
});
