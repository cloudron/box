'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js'),
    expect = require('expect.js'),
    mail = require('../../mail.js'),
    superagent = require('../../superagent.js'),
    _ = require('../../underscore.js');

describe('Mail API', function () {
    const { setup, cleanup, serverUrl, owner, dashboardDomain, dashboardFqdn, mailFqdn } = common;

    let publicKey;
    before(async () => {
        await setup();
        const d = await mail.getDomain(dashboardDomain);
        publicKey = d.dkimKey.publicKey.split('\n').slice(1, -2).join(''); // remove header, footer and new lines
    });

    after(cleanup);

    describe('crud', function () {
        it('cannot get non-existing domain', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/doesnotexist.com`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(404);
        });

        it('can get domain', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.domain).to.equal(dashboardDomain);
            expect(response.body.enabled).to.equal(false);
            expect(response.body.mailFromValidation).to.equal(true);
            expect(response.body.catchAll).to.be.an(Array);
            expect(response.body.catchAll.length).to.equal(0);
            expect(response.body.relay).to.be.an('object');
            expect(response.body.relay.provider).to.equal('cloudron-smtp');
        });
    });

    describe('status', function () {
        let resolve = null;
        let dnsAnswerQueue = [];
        let dkimDomain, spfDomain, mxDomain, dmarcDomain;

        before(async function () {
            const dig = require('../../dig.js');

            // replace dns resolveTxt()
            resolve = dig.resolve;
            dig.resolve = async function (hostname, type/*, options*/) {
                expect(hostname).to.be.a('string');

                if (!dnsAnswerQueue[hostname] || !(type in dnsAnswerQueue[hostname])) throw new Error('no mock answer');

                if (dnsAnswerQueue[hostname][type] === null) throw new Error({ code: 'ENODATA'} );

                return dnsAnswerQueue[hostname][type];
            };

            dkimDomain = `cloudron._domainkey.${dashboardDomain}`; // no suffix for provisioned domains
            spfDomain = dashboardDomain;
            mxDomain = dashboardDomain;
            dmarcDomain = '_dmarc.' + dashboardDomain;

            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/enable`)
                .query({ access_token: owner.token })
                .send({ enabled: true });

            expect(response.status).to.be(202);
        });

        after(function (done) {
            const dig = require('../../dig.js');

            dig.resolve = resolve;

            done();
        });

        it('does not fail when dns errors', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}/status`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
        });

        function clearDnsAnswerQueue() {
            dnsAnswerQueue = { };
            dnsAnswerQueue[dkimDomain] =  { };
            dnsAnswerQueue[spfDomain] =  { };
            dnsAnswerQueue[mxDomain] =  { };
            dnsAnswerQueue[dmarcDomain] =  { };
        }

        it('succeeds with dns errors', async function () {
            clearDnsAnswerQueue();

            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}` + '/status')
                .query({ access_token: owner.token });
            expect(response.status).to.equal(200);

            expect(response.body.dns.dkim).to.be.an('object');
            expect(response.body.dns.dkim.domain).to.eql(dkimDomain);
            expect(response.body.dns.dkim.type).to.eql('TXT');
            expect(response.body.dns.dkim.value).to.eql(null);
            expect(response.body.dns.dkim.expected).to.eql(`v=DKIM1; t=s; p=${publicKey}`);
            expect(response.body.dns.dkim.status).to.eql(false);

            expect(response.body.dns.spf).to.be.an('object');
            expect(response.body.dns.spf.domain).to.eql(spfDomain);
            expect(response.body.dns.spf.type).to.eql('TXT');
            expect(response.body.dns.spf.value).to.eql(null);
            expect(response.body.dns.spf.expected).to.eql(`v=spf1 a:${dashboardFqdn} ~all`);
            expect(response.body.dns.spf.status).to.eql(false);

            expect(response.body.dns.dmarc).to.be.an('object');
            expect(response.body.dns.dmarc.type).to.eql('TXT');
            expect(response.body.dns.dmarc.value).to.eql(null);
            expect(response.body.dns.dmarc.expected).to.eql('v=DMARC1; p=reject; pct=100');
            expect(response.body.dns.dmarc.status).to.eql(false);

            expect(response.body.dns.mx).to.be.an('object');
            expect(response.body.dns.mx.type).to.eql('MX');
            expect(response.body.dns.mx.value).to.eql(null);
            expect(response.body.dns.mx.expected).to.eql(`10 ${mailFqdn}.`);
            expect(response.body.dns.mx.status).to.eql(false);

            expect(response.body.dns.ptr4).to.be.an('object');
            expect(response.body.dns.ptr4.type).to.eql('PTR');
            // expect(response.body.ptr.value).to.eql(null); this will be anything random
            expect(response.body.dns.ptr4.expected).to.eql(mailFqdn);
            expect(response.body.dns.ptr4.status).to.eql(false);
        });

        it('succeeds with "undefined" spf, dkim, dmarc, mx, ptr records', async function () {
            clearDnsAnswerQueue();

            dnsAnswerQueue[dkimDomain].TXT = null;
            dnsAnswerQueue[spfDomain].TXT = null;
            dnsAnswerQueue[mxDomain].MX = null;
            dnsAnswerQueue[dmarcDomain].TXT = null;

            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}` + '/status')
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);

            expect(response.body.dns.spf).to.be.an('object');
            expect(response.body.dns.spf.expected).to.eql(`v=spf1 a:${dashboardFqdn} ~all`);
            expect(response.body.dns.spf.status).to.eql(false);
            expect(response.body.dns.spf.value).to.eql(null);

            expect(response.body.dns.dkim).to.be.an('object');
            expect(response.body.dns.dkim.expected).to.eql(`v=DKIM1; t=s; p=${publicKey}`);
            expect(response.body.dns.dkim.status).to.eql(false);
            expect(response.body.dns.dkim.value).to.eql(null);

            expect(response.body.dns.dmarc).to.be.an('object');
            expect(response.body.dns.dmarc.expected).to.eql('v=DMARC1; p=reject; pct=100');
            expect(response.body.dns.dmarc.status).to.eql(false);
            expect(response.body.dns.dmarc.value).to.eql(null);

            expect(response.body.dns.mx).to.be.an('object');
            expect(response.body.dns.mx.status).to.eql(false);
            expect(response.body.dns.mx.expected).to.eql(`10 ${mailFqdn}.`);
            expect(response.body.dns.mx.value).to.eql(null);

            expect(response.body.dns.ptr4).to.be.an('object');
            expect(response.body.dns.ptr4.expected).to.eql(mailFqdn);
            expect(response.body.dns.ptr4.status).to.eql(false);
            // expect(response.body.ptr.value).to.eql(null); this will be anything random
        });

        it('succeeds with all different spf, dkim, dmarc, mx, ptr records', async function () {
            clearDnsAnswerQueue();

            dnsAnswerQueue[mxDomain].MX = [ { priority: '20', exchange: mailFqdn }, { priority: '10', exchange: 'some.other.server' } ];
            dnsAnswerQueue[dmarcDomain].TXT = [['v=DMARC2; p=reject; pct=100']];
            dnsAnswerQueue[dkimDomain].TXT = [['v=DKIM2; t=s; p=' + publicKey]];
            dnsAnswerQueue[spfDomain].TXT = [['v=spf1 a:random.com ~all']];

            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}` + '/status')
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);

            expect(response.body.dns.spf).to.be.an('object');
            expect(response.body.dns.spf.expected).to.eql(`v=spf1 a:${dashboardFqdn} a:random.com ~all`);
            expect(response.body.dns.spf.status).to.eql(false);
            expect(response.body.dns.spf.value).to.eql('v=spf1 a:random.com ~all');

            expect(response.body.dns.dkim).to.be.an('object');
            expect(response.body.dns.dkim.expected).to.eql('v=DKIM1; t=s; p=' + publicKey);
            expect(response.body.dns.dkim.status).to.eql(true); // as long as p= matches we are good
            expect(response.body.dns.dkim.value).to.eql('v=DKIM2; t=s; p=' + publicKey);

            expect(response.body.dns.dmarc).to.be.an('object');
            expect(response.body.dns.dmarc.expected).to.eql('v=DMARC1; p=reject; pct=100');
            expect(response.body.dns.dmarc.status).to.eql(false);
            expect(response.body.dns.dmarc.value).to.eql('v=DMARC2; p=reject; pct=100');

            expect(response.body.dns.mx).to.be.an('object');
            expect(response.body.dns.mx.status).to.eql(true);
            expect(response.body.dns.mx.expected).to.eql(`10 ${mailFqdn}.`);
            expect(response.body.dns.mx.value).to.eql(`20 ${mailFqdn}. 10 some.other.server.`);

            expect(response.body.dns.ptr4).to.be.an('object');
            expect(response.body.dns.ptr4.expected).to.eql(mailFqdn);
            expect(response.body.dns.ptr4.status).to.eql(false);
            // expect(response.body.ptr.value).to.eql(null); this will be anything random

            expect(response.body.relay).to.be.an('object');
        });

        it('succeeds with existing embedded spf', async function () {
            clearDnsAnswerQueue();

            dnsAnswerQueue[spfDomain].TXT = [['v=spf1 a:example.com a:' + mailFqdn + ' ~all']];

            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}/status`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);

            expect(response.body.dns.spf).to.be.an('object');
            expect(response.body.dns.spf.domain).to.eql(spfDomain);
            expect(response.body.dns.spf.type).to.eql('TXT');
            expect(response.body.dns.spf.value).to.eql(`v=spf1 a:example.com a:${mailFqdn} ~all`);
            expect(response.body.dns.spf.expected).to.eql(`v=spf1 a:example.com a:${mailFqdn} ~all`);
            expect(response.body.dns.spf.status).to.eql(true);
        });

        it('succeeds with modified DMARC1 values', async function () {
            clearDnsAnswerQueue();

            dnsAnswerQueue[dmarcDomain].TXT = [['v=DMARC1; p=reject; rua=mailto:rua@example.com; pct=100']];

            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}/status`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);

            expect(response.body.dns.dmarc).to.be.an('object');
            expect(response.body.dns.dmarc.expected).to.eql('v=DMARC1; p=reject; pct=100');
            expect(response.body.dns.dmarc.status).to.eql(true);
            expect(response.body.dns.dmarc.value).to.eql('v=DMARC1; p=reject; rua=mailto:rua@example.com; pct=100');
        });

        it('succeeds with all correct records', async function () {
            clearDnsAnswerQueue();

            dnsAnswerQueue[mxDomain].MX = [ { priority: '10', exchange: mailFqdn } ];
            dnsAnswerQueue[dmarcDomain].TXT = [['v=DMARC1; p=reject; pct=100']];
            dnsAnswerQueue[dkimDomain].TXT = [['v=DKIM1; t=s; p=', publicKey ]];
            dnsAnswerQueue[spfDomain].TXT = [[`v=spf1 a:${dashboardFqdn} ~all`]];

            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}` + '/status')
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);

            expect(response.body.dns.dkim).to.be.an('object');
            expect(response.body.dns.dkim.domain).to.eql(dkimDomain);
            expect(response.body.dns.dkim.type).to.eql('TXT');
            expect(response.body.dns.dkim.value).to.eql(`v=DKIM1; t=s; p=${publicKey}`);
            expect(response.body.dns.dkim.expected).to.eql(`v=DKIM1; t=s; p=${publicKey}`);
            expect(response.body.dns.dkim.status).to.eql(true);

            expect(response.body.dns.spf).to.be.an('object');
            expect(response.body.dns.spf.domain).to.eql(spfDomain);
            expect(response.body.dns.spf.type).to.eql('TXT');
            expect(response.body.dns.spf.value).to.eql(`v=spf1 a:${dashboardFqdn} ~all`);
            expect(response.body.dns.spf.expected).to.eql(`v=spf1 a:${dashboardFqdn} ~all`);
            expect(response.body.dns.spf.status).to.eql(true);

            expect(response.body.dns.dmarc).to.be.an('object');
            expect(response.body.dns.dmarc.expected).to.eql('v=DMARC1; p=reject; pct=100');
            expect(response.body.dns.dmarc.status).to.eql(true);
            expect(response.body.dns.dmarc.value).to.eql('v=DMARC1; p=reject; pct=100');

            expect(response.body.dns.mx).to.be.an('object');
            expect(response.body.dns.mx.status).to.eql(true);
            expect(response.body.dns.mx.expected).to.eql(`10 ${mailFqdn}.`);
            expect(response.body.dns.mx.value).to.eql(`10 ${mailFqdn}.`);
        });
    });

    describe('mail from validation', function () {
        it('get mail from validation succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.mailFromValidation).to.eql(true);
        });

        it('cannot set without enabled field', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/mail_from_validation`)
                .query({ access_token: owner.token })
                .send({ })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('can set with enabled field', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/mail_from_validation`)
                .query({ access_token: owner.token })
                .send({ enabled: false });

            expect(response.status).to.equal(202);
        });
    });

    describe('catch_all', function () {
        it('get catch_all succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}`)
                .query({ access_token: owner.token });
            expect(response.status).to.equal(200);
            expect(response.body.catchAll).to.eql([]);
        });

        it('cannot set without addresses field', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/catch_all`)
                .query({ access_token: owner.token })
                .send({ })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot set with bad addresses field', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/catch_all`)
                .query({ access_token: owner.token })
                .send({ addresses: [ 'user1', 123 ] })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot set with bad addresses field', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/catch_all`)
                .query({ access_token: owner.token })
                .send({ addresses: [ 'user1' ] })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('set succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/catch_all`)
                .query({ access_token: owner.token })
                .send({ addresses: [ `user1@${dashboardDomain}` ] });

            expect(response.status).to.equal(202);
        });

        it('get succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.catchAll).to.eql([ `user1@${dashboardDomain}` ]);
        });
    });

    describe('mail relay', function () {
        it('get mail relay succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}`)
                .query({ access_token: owner.token });
            expect(response.status).to.equal(200);
            expect(response.body.relay).to.eql({ provider: 'cloudron-smtp' });
        });

        it('cannot set without provider field', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/relay`)
                .query({ access_token: owner.token })
                .send({ })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot set with bad host', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/relay`)
                .query({ access_token: owner.token })
                .send({ provider: 'external-smtp', host: true })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('set fails because mail server is unreachable', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/relay`)
                .query({ access_token: owner.token })
                .send({ provider: 'external-smtp', host: 'host', port: 25, username: 'u', password: 'p', tls: true })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('get succeeds', async function () {
            const relay = { provider: 'external-smtp', host: 'host', port: 25, username: 'u', password: 'p', tls: true };

            await mail.setMailRelay(dashboardDomain, relay, { skipVerify: true });

            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(_.omit(response.body.relay, ['password'])).to.eql(_.omit(relay, ['password']));
        });
    });

    describe('mailboxes', function () {
        const MAILBOX_NAME = 'support';

        it('add succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/mailboxes`)
                .send({ name: MAILBOX_NAME, ownerId: owner.id, ownerType: 'user', active: true, storageQuota: 10, messagesQuota: 20 })
                .query({ access_token: owner.token });

            expect(response.status).to.equal(201);
        });

        it('cannot add again', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/mailboxes`)
                .send({ name: MAILBOX_NAME, ownerId: owner.id, ownerType: 'user', active: true, storageQuota: 10, messagesQuota: 20 })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(409);
        });

        it('get fails if not exist', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}/mailboxes/someuserdoesnotexist`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(404);
        });

        it('get succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}/mailboxes/${MAILBOX_NAME}`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.mailbox).to.be.an('object');
            expect(response.body.mailbox.name).to.equal(MAILBOX_NAME);
            expect(response.body.mailbox.ownerId).to.equal(owner.id);
            expect(response.body.mailbox.ownerType).to.equal('user');
            expect(response.body.mailbox.aliasName).to.equal(null);
            expect(response.body.mailbox.aliasDomain).to.equal(null);
            expect(response.body.mailbox.domain).to.equal(dashboardDomain);
            expect(response.body.mailbox.storageQuota).to.equal(10);
            expect(response.body.mailbox.messagesQuota).to.equal(20);
        });

        it('listing succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}/mailboxes`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.mailboxes.length).to.eql(1);
            expect(response.body.mailboxes[0]).to.be.an('object');
            expect(response.body.mailboxes[0].name).to.equal(MAILBOX_NAME);
            expect(response.body.mailboxes[0].ownerId).to.equal(owner.id);
            expect(response.body.mailboxes[0].ownerType).to.equal('user');
            expect(response.body.mailboxes[0].aliases).to.eql([]);
            expect(response.body.mailboxes[0].domain).to.equal(dashboardDomain);
            expect(response.body.mailboxes[0].storageQuota).to.equal(10);
            expect(response.body.mailboxes[0].messagesQuota).to.equal(20);
        });

        it('disable fails even if not exist', async function () {
            const response = await superagent.del(`${serverUrl}/api/v1/mail/${dashboardDomain}/mailboxes/someuserdoesnotexist`)
                .send({ deleteMails: false })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(404);
        });

        it('disable succeeds', async function () {
            const response = await superagent.del(`${serverUrl}/api/v1/mail/${dashboardDomain}/mailboxes/${MAILBOX_NAME}`)
                .send({ deleteMails: false })
                .query({ access_token: owner.token });

            expect(response.status).to.equal(201);

            const response2 = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}` + '/mailboxes/' + MAILBOX_NAME)
                .query({ access_token: owner.token })
                .ok(() => true);
            expect(response2.status).to.equal(404);
        });
    });

    describe('aliases', function () {
        const MAILBOX_NAME = 'support';

        after(async function () {
            await mail._delByDomain(dashboardDomain);
        });

        it('add the mailbox', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/mailboxes`)
                .send({ name: MAILBOX_NAME, ownerId: owner.id, ownerType: 'user', active: true, storageQuota: 10, messagesQuota: 20 })
                .query({ access_token: owner.token });

            expect(response.status).to.equal(201);
        });

        it('set fails if aliases is missing', async function () {
            const response = await superagent.put(`${serverUrl}/api/v1/mail/${dashboardDomain}/mailboxes/${MAILBOX_NAME}/aliases`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('set fails if user does not exist', async function () {
            const response = await superagent.put(`${serverUrl}/api/v1/mail/${dashboardDomain}/mailboxes/randomuser/aliases`)
                .send({ aliases: [{ name: 'hello', domain: dashboardDomain}, {name: 'there', domain: dashboardDomain}] })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(404);
        });

        it('set fails if aliases is the wrong type', async function () {
            const response = await superagent.put(`${serverUrl}/api/v1/mail/${dashboardDomain}/mailboxes/${MAILBOX_NAME}/aliases`)
                .send({ aliases: 'hello, there' })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('set succeeds', async function () {
            const response = await superagent.put(`${serverUrl}/api/v1/mail/${dashboardDomain}/mailboxes/${MAILBOX_NAME}/aliases`)
                .send({ aliases: [{ name: 'hello*', domain: dashboardDomain}, {name: 'there', domain: dashboardDomain}] })
                .query({ access_token: owner.token });

            expect(response.status).to.equal(202);
        });

        it('get succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}` + '/mailboxes/' + MAILBOX_NAME + '/aliases')
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.aliases).to.eql([{ name: 'hello*', domain: dashboardDomain}, {name: 'there', domain: dashboardDomain}]);
        });

        it('get fails if mailbox does not exist', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}` + '/mailboxes/somerandomuser/aliases')
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(404);
        });
    });

    describe('mailinglists', function () {
        const LIST_NAME = 'people';

        after(async function () {
            await mail._delByDomain(dashboardDomain);
        });

        it('add fails without groupId', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/lists`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('add fails with invalid groupId', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/lists`)
                .send({ groupId: {} })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('add fails without members array', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/lists`)
                .send({ name: LIST_NAME })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('add succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/lists`)
                .send({ name: LIST_NAME, members: [ `admin2@${dashboardDomain}`, `${owner.username}@${dashboardDomain}`], membersOnly: false, active: true })
                .query({ access_token: owner.token });

            expect(response.status).to.equal(201);
        });

        it('add twice fails', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/mail/${dashboardDomain}/lists`)
                .send({ name: LIST_NAME, members: [ `admin2@${dashboardDomain}`, `${owner.username}@${dashboardDomain}`], membersOnly: false, active: true })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(409);
        });

        it('get fails if not exist', async function (){
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}/lists/doesnotexist`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(404);
        });

        it('get succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}/lists/${LIST_NAME}`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.list).to.be.an('object');
            expect(response.body.list.name).to.equal(LIST_NAME);
            expect(response.body.list.ownerId).to.equal('admin');
            expect(response.body.list.aliasName).to.equal(null);
            expect(response.body.list.domain).to.equal(dashboardDomain);
            expect(response.body.list.members).to.eql([ `admin2@${dashboardDomain}`, `superadmin@${dashboardDomain}` ]);
            expect(response.body.list.membersOnly).to.be(false);
        });

        it('get all succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}/lists`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.lists).to.be.an(Array);
            expect(response.body.lists.length).to.equal(1);
            expect(response.body.lists[0].name).to.equal(LIST_NAME);
            expect(response.body.lists[0].ownerId).to.equal('admin');
            expect(response.body.lists[0].aliasName).to.equal(null);
            expect(response.body.lists[0].domain).to.equal(dashboardDomain);
            expect(response.body.lists[0].members).to.eql([ `admin2@${dashboardDomain}`, `superadmin@${dashboardDomain}` ]);
            expect(response.body.lists[0].membersOnly).to.be(false);
        });

        it('del fails if list does not exist', async function () {
            const response = await superagent.del(`${serverUrl}/api/v1/mail/${dashboardDomain}` + '/lists/' + 'doesnotexist')
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(404);
        });

        it('del succeeds', async function () {
            const response = await superagent.del(`${serverUrl}/api/v1/mail/${dashboardDomain}/lists/${LIST_NAME}`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(204);

            const response2 = await superagent.get(`${serverUrl}/api/v1/mail/${dashboardDomain}/lists/${LIST_NAME}`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response2.status).to.equal(404);
        });
    });
});
