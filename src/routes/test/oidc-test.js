/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

const CLIENT_0 = {
    id: 'client0',
    name: 'test client 0',
    secret: 'secret0',
    tokenSignatureAlgorithm: 'RS256',
    loginRedirectUri: 'http://foo.bar'
};

const CLIENT_1 = {
    id: 'client1',
    name: 'test client 1',
    secret: 'secret1',
    tokenSignatureAlgorithm: 'EdDSA',
    loginRedirectUri: 'https://cloudron.io/login'
};

describe('OpenID connect clients API', function () {
    const { setup, cleanup, serverUrl, owner, user } = common;

    before(setup);
    after(cleanup);

    it('create fails due to missing token', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/oidc/clients`)
            .send(CLIENT_0)
            .ok(() => true);

        expect(response.status).to.equal(401);
    });

    it('create succeeds', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/oidc/clients`)
            .query({ access_token: owner.token })
            .send(CLIENT_0);

        expect(response.status).to.equal(201);
        CLIENT_0.id = response.body.id;
        CLIENT_0.secret = response.body.secret;
    });

    it('can create another client', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/oidc/clients`)
            .query({ access_token: owner.token })
            .send(CLIENT_1);

        expect(response.status).to.equal(201);
        CLIENT_1.id = response.body.id;
        CLIENT_1.secret = response.body.secret;
    });

    it('cannot get non-existing client', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/oidc/clients/nope`)
            .query({ access_token: owner.token })
            .ok(() => true);

        expect(response.status).to.equal(404);
    });

    it('cannot get existing client with normal user', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/oidc/clients/${CLIENT_0.id}`)
            .query({ access_token: user.token })
            .ok(() => true);

        expect(response.status).to.equal(403);
    });

    it('can get existing client', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/oidc/clients/${CLIENT_1.id}`)
            .query({ access_token: owner.token });

        expect(response.status).to.equal(200);
        expect(response.body.id).to.equal(CLIENT_1.id);
        expect(response.body.secret).to.equal(CLIENT_1.secret);
        expect(response.body.loginRedirectUri).to.equal(CLIENT_1.loginRedirectUri);
        expect(response.body.tokenSignatureAlgorithm).to.equal(CLIENT_1.tokenSignatureAlgorithm);
    });

    it('cannot update non-existent client', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/oidc/clients/nope`)
            .query({ access_token: owner.token })
            .send(CLIENT_0)
            .ok(() => true);

        expect(response.status).to.equal(404);
    });

    it('cannot list clients without token', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/oidc/clients`)
            .ok(() => true);

        expect(response.status).to.equal(401);
    });

    it('cannot list clients as normal user', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/oidc/clients`)
            .query({ access_token: user.token })
            .ok(() => true);

        expect(response.status).to.equal(403);
    });

    it('can list clients', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/oidc/clients`)
            .query({ access_token: owner.token });

        expect(response.status).to.equal(200);
        expect(response.body.clients).to.be.an(Array);
        expect(response.body.clients.length).to.be(2);
        expect(response.body.clients[0].id).to.eql(CLIENT_0.id);
        expect(response.body.clients[1].id).to.eql(CLIENT_1.id);
    });

    it('cann update client', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/oidc/clients/${CLIENT_0.id}`)
            .query({ access_token: owner.token })
            .send({ loginRedirectUri: CLIENT_0.loginRedirectUri })
            .ok(() => true);

        expect(response.status).to.equal(400);
    });

    it('cannot update client without loginRedirectUri', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/oidc/clients/${CLIENT_0.id}`)
            .query({ access_token: owner.token })
            .send({})
            .ok(() => true);

        expect(response.status).to.equal(400);
    });

    it('cannot remove without token', async function () {
        const response = await superagent.del(`${serverUrl}/api/v1/oidc/clients/${CLIENT_0.id}`)
            .ok(() => true);

        expect(response.status).to.equal(401);
    });

    it('can remove empty group', async function () {
        const response = await superagent.del(`${serverUrl}/api/v1/oidc/clients/${CLIENT_0.id}`)
            .query({ access_token: owner.token });

        expect(response.status).to.equal(204);
    });
});
