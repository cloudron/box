'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js'),
    constants = require('../../constants.js'),
    expect = require('expect.js'),
    fs = require('fs'),
    paths = require('../../paths.js'),
    superagent = require('../../superagent.js');

describe('Branding API', function () {
    const { setup, cleanup, serverUrl, owner } = common;

    before(setup);
    after(cleanup);

    describe('cloudron_name', function () {
        let name = 'foobar';

        it('get default succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/branding/cloudron_name`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.name).to.be('Cloudron');
        });

        it('cannot set without name', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/branding/cloudron_name`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot set empty name', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/branding/cloudron_name`)
                .query({ access_token: owner.token })
                .send({ name: '' })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('set succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/branding/cloudron_name`)
                .query({ access_token: owner.token })
                .send({ name });

            expect(response.status).to.equal(200);
        });

        it('get succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/branding/cloudron_name`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.name).to.eql(name);
        });
    });

    describe('cloudron_avatar', function () {
        it('get default succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/branding/cloudron_avatar`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body).to.be.a(Buffer);
        });

        it('cannot set without data', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/branding/cloudron_avatar`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('set succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/branding/cloudron_avatar`)
                .query({ access_token: owner.token })
                .attach('avatar', paths.CLOUDRON_DEFAULT_AVATAR_FILE);

            expect(response.status).to.equal(200);
        });

        it('get succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/branding/cloudron_avatar`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.toString()).to.eql(fs.readFileSync(paths.CLOUDRON_DEFAULT_AVATAR_FILE, 'utf-8'));
        });
    });

    describe('footer', function () {
        it('get default succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/branding/footer`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.footer).to.eql(constants.FOOTER);
        });

        it('cannot set without data', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/branding/footer`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('set succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/branding/footer`)
                .query({ access_token: owner.token })
                .send({ footer: 'BigFoot Inc' });

            expect(response.status).to.equal(200);
        });

        it('get succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/branding/footer`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.footer).to.eql('BigFoot Inc');
        });
    });
});
