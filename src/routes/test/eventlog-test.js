/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const async = require('async'),
    common = require('./common.js'),
    eventlog = require('../../eventlog.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

describe('Eventlog API', function () {
    const { setup, cleanup, serverUrl, owner, user } = common;

    const EVENT_0 = {
        id: null,
        action: 'foobaraction',
        source: {ip: '127.0.0.1' },
        data: { something: 'is there' }
    };

    before(function (done) {
        async.series([
            setup,
            async () => { EVENT_0.id = await eventlog.add(EVENT_0.action, EVENT_0.source, EVENT_0.data); },
        ], done);
    });
    after(cleanup);

    describe('get', function () {
        it('fails due to wrong token', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/eventlog/someid`)
                .query({ access_token: 'badtoken' })
                .ok(() => true);
            expect(response.status).to.be(401);
        });

        it('fails for non-admin', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/eventlog/someid`)
                .query({ access_token: user.token })
                .ok(() => true);
            expect(response.status).to.equal(403);
        });

        it('fails if not exists', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/eventlog/someid`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(404);
        });

        it('succeeds for admin', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/eventlog/${EVENT_0.id}`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.status).to.equal(200);
            delete response.body.event.creationTime;
            expect(response.body.event).to.eql(EVENT_0);
        });
    });

    describe('list', function () {
        it('fails due to wrong token', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/eventlog`)
                .query({ access_token: 'badtoken' })
                .ok(() => true);
            expect(response.status).to.equal(401);
        });

        it('fails for non-admin', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/eventlog`)
                .query({ access_token: user.token, page: 1, per_page: 10 })
                .ok(() => true);

            expect(response.status).to.equal(403);
        });

        it('succeeds for admin', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/eventlog`)
                .query({ access_token: owner.token, page: 1, per_page: 10 });

            expect(response.status).to.equal(200);
            expect(response.body.eventlogs.length >= 2).to.be.ok(); // activate, user.add
        });

        it('succeeds with action', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/eventlog`)
                .query({ access_token: owner.token, page: 1, per_page: 10, action: 'cloudron.activate' });

            expect(response.status).to.equal(200);
            expect(response.body.eventlogs.length).to.equal(1);
        });

        it('succeeds with actions', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/eventlog`)
                .query({ access_token: owner.token, page: 1, per_page: 10, actions: 'cloudron.activate, user.add' });

            expect(response.status).to.equal(200);
            expect(response.body.eventlogs.length).to.equal(4);
        });

        it('succeeds with search', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/eventlog`)
                .query({ access_token: owner.token, page: 1, per_page: 10, search: owner.email });

            expect(response.status).to.equal(200);
            expect(response.body.eventlogs.length).to.equal(1);
        });

        it('succeeds with search and actions', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/eventlog`)
                .query({ access_token: owner.token, page: 1, per_page: 10, search: owner.email, actions: 'cloudron.activate' });

            expect(response.status).to.equal(200);
            expect(response.body.eventlogs.length).to.equal(0);
        });
    });
});
