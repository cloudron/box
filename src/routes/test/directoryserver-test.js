'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

describe('Directory Server API', function () {
    const { setup, cleanup, serverUrl, owner } = common;

    before(setup);
    after(cleanup);

    describe('directory_server config', function () {
        // keep in sync with defaults in settings.js
        let defaultConfig = {
            enabled: false,
            secret: '',
            allowlist: ''
        };

        it('can get directory_server config (default)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/directory_server/config`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body).to.eql(defaultConfig);
        });

        it('cannot set directory_server config without enabled boolean', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            delete tmp.enabled;

            const response = await superagent.post(`${serverUrl}/api/v1/directory_server/config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot set directory_server config without secret', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.enabled = true;
            delete tmp.secret;

            const response = await superagent.post(`${serverUrl}/api/v1/directory_server/config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot enable directory_server config with empty secret', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.enabled = true;

            const response = await superagent.post(`${serverUrl}/api/v1/directory_server/config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot enable directory_server config with empty allowlist', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.enabled = true;
            tmp.secret = 'ldapsecret';

            const response = await superagent.post(`${serverUrl}/api/v1/directory_server/config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('can enable directory_server config', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.enabled = true;
            tmp.secret = 'ldapsecret';
            tmp.allowlist = '1.2.3.4';

            const response = await superagent.post(`${serverUrl}/api/v1/directory_server/config`)
                .query({ access_token: owner.token })
                .send(tmp);

            expect(response.status).to.equal(200);
        });

        it('can get directory_server config', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.enabled = true;

            const response = await superagent.get(`${serverUrl}/api/v1/directory_server/config`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body).to.eql({ enabled: true, secret: 'ldapsecret', allowlist: '1.2.3.4' });
        });

        // keep this last. this ensures directory server is stopped and the tests can exit
        it('can disable directory_server config', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.enabled = false;
            tmp.secret = 'ldapsecret';

            const response = await superagent.post(`${serverUrl}/api/v1/directory_server/config`)
                .query({ access_token: owner.token })
                .send(tmp);

            expect(response.status).to.equal(200);
        });
    });
});
