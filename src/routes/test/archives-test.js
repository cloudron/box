/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const archives = require('../../archives.js'),
    backups = require('../../backups.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

describe('Archives API', function () {
    const { setup, cleanup, serverUrl, owner, auditSource } = common;

    const appBackup = {
        id: null,
        remotePath: 'app_appid_123',
        encryptionVersion: null,
        packageVersion: '1.0.0',
        type: backups.BACKUP_TYPE_APP,
        state: backups.BACKUP_STATE_CREATING,
        identifier: 'appid',
        dependsOn: [ ],
        manifest: { foo: 'bar' },
        format: 'tgz',
        preserveSecs: 0,
        label: '',
        appConfig: { loc: 'loc1' }
    };

    let archiveId;

    before(async function () {
        await setup();
        appBackup.id = await backups.add(appBackup);
        archiveId = await archives.add(appBackup.id, {}, auditSource);
    });
    after(cleanup);

    it('list succeeds', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/archives`)
            .query({ access_token: owner.token });
        expect(response.status).to.equal(200);
        expect(response.body.archives.length).to.be(1);
        expect(response.body.archives[0].id).to.be(archiveId);
        expect(response.body.archives[0].appConfig).to.eql(appBackup.appConfig);
        expect(response.body.archives[0].manifest).to.eql(appBackup.manifest);
    });

    it('get valid archive', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/archives/${archiveId}`)
            .query({ access_token: owner.token });
        expect(response.status).to.equal(200);
        expect(response.body.appConfig).to.eql(appBackup.appConfig);
        expect(response.body.manifest).to.eql(appBackup.manifest);
    });

    it('cannot get invalid archive', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/archives/random`)
            .query({ access_token: owner.token })
            .ok(() => true);
        expect(response.status).to.equal(404);
    });

    it('cannot del invalid archive', async function () {
        const response = await superagent.del(`${serverUrl}/api/v1/archives/random`)
            .query({ access_token: owner.token })
            .ok(() => true);
        expect(response.status).to.equal(404);
    });

    it('del valid archive', async function () {
        const response = await superagent.del(`${serverUrl}/api/v1/archives/${archiveId}`)
            .query({ access_token: owner.token });
        expect(response.status).to.equal(204);

        const response2 = await superagent.get(`${serverUrl}/api/v1/archives`)
            .query({ access_token: owner.token });
        expect(response2.status).to.equal(200);
        expect(response2.body.archives.length).to.be(0);
    });
});
