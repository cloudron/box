/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

const GROUP_NAME = 'externals';
let group0Object, group1Object;

describe('Groups API', function () {
    const { setup, cleanup, serverUrl, owner, user, app } = common;

    before(setup);
    after(cleanup);

    it('create fails due to mising token', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/groups`)
            .send({ name: GROUP_NAME })
            .ok(() => true);

        expect(response.status).to.equal(401);
    });

    it('create succeeds', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/groups`)
            .query({ access_token: owner.token })
            .send({ name: GROUP_NAME });

        expect(response.status).to.equal(201);
        group0Object = response.body;
    });

    it('create fails for already exists', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/groups`)
            .query({ access_token: owner.token })
            .send({ name: GROUP_NAME})
            .ok(() => true);

        expect(response.status).to.equal(409);
    });

    it('can create another group', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/groups`)
            .query({ access_token: owner.token })
            .send({ name: 'group1'});

        expect(response.status).to.equal(201);
        group1Object = response.body;
    });

    it('cannot add user to invalid group', async function () {
        const response = await superagent.put(`${serverUrl}/api/v1/users/${user.id}/groups`)
            .query({ access_token: owner.token })
            .send({ groupIds: [ group0Object.id, 'something' ]})
            .ok(() => true);

        expect(response.status).to.equal(404);
    });

    it('can set groups of a user', async function () {
        const response = await superagent.put(`${serverUrl}/api/v1/users/${user.id}/groups`)
            .query({ access_token: owner.token })
            .send({ groupIds: [ group0Object.id, group1Object.id ]});

        expect(response.status).to.equal(204);
    });

    it('cannot set duplicate groups for a user', async function () {
        const response = await superagent.put(`${serverUrl}/api/v1/users/${user.id}/groups`)
            .query({ access_token: owner.token })
            .send({ groupIds: [ group0Object.id, group1Object.id, group0Object.id ]})
            .ok(() => true);

        expect(response.status).to.equal(409);
    });

    it('can set users of a group', async function () {
        const response = await superagent.put(`${serverUrl}/api/v1/groups/${group0Object.id}/members`)
            .query({ access_token: owner.token })
            .send({ userIds: [ owner.id, user.id ]});

        expect(response.status).to.equal(200);
    });

    it('cannot set duplicate users of a group', async function () {
        const response = await superagent.put(`${serverUrl}/api/v1/groups/${group0Object.id}/members`)
            .query({ access_token: owner.token })
            .send({ userIds: [ owner.id, user.id, owner.id ]})
            .ok(() => true);

        expect(response.status).to.equal(409);
    });

    it('cannot get non-existing group', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/groups/nope`)
            .query({ access_token: owner.token })
            .ok(() => true);

        expect(response.status).to.equal(404);
    });

    it('cannot get existing group with normal user', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/groups/${group0Object.id}`)
            .query({ access_token: user.token })
            .ok(() => true);

        expect(response.status).to.equal(403);
    });

    it('can get existing group', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/groups/${group0Object.id}`)
            .query({ access_token: owner.token });

        expect(response.status).to.equal(200);
        expect(response.body.name).to.be(group0Object.name);
        expect(response.body.userIds.length).to.be(2);
        expect(response.body.userIds).to.contain(owner.id);
        expect(response.body.userIds).to.contain(user.id);
    });

    it('cannot list groups without token', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/groups`)
            .ok(() => true);

        expect(response.status).to.equal(401);
    });

    it('cannot list groups as normal user', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/groups`)
            .query({ access_token: user.token })
            .ok(() => true);

        expect(response.status).to.equal(403);
    });

    it('can list groups', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/groups`)
            .query({ access_token: owner.token });

        expect(response.status).to.equal(200);
        expect(response.body.groups).to.be.an(Array);
        expect(response.body.groups.length).to.be(2);
        expect(response.body.groups[0].name).to.eql(group0Object.name);
        expect(response.body.groups[1].name).to.eql(group1Object.name);
    });

    it('cannot set missing group name', async function () {
        const response = await superagent.put(`${serverUrl}/api/v1/groups/${group1Object.id}/name`)
            .query({ access_token: owner.token })
            .send({ })
            .ok(() => true);

        expect(response.status).to.equal(400);
    });

    it('can set invalid group name', async function () {
        const response = await superagent.put(`${serverUrl}/api/v1/groups/${group1Object.id}/name`)
            .query({ access_token: owner.token })
            .send({ name: '!group1-newname'})
            .ok(() => true);

        expect(response.status).to.equal(400);
    });

    it('can set group name', async function () {
        const response = await superagent.put(`${serverUrl}/api/v1/groups/${group1Object.id}/name`)
            .query({ access_token: owner.token })
            .send({ name: 'group1-newname'});

        expect(response.status).to.equal(200);
    });

    it('cannot set unknown app access', async function () {
        const response = await superagent.put(`${serverUrl}/api/v1/groups/${group1Object.id}/apps`)
            .query({ access_token: owner.token })
            .send({ appIds: ['app-id' ] })
            .ok(() => true);

        expect(response.status).to.equal(200); // bad appId is just ignored
    });

    it('can set app access', async function () {
        const response = await superagent.put(`${serverUrl}/api/v1/groups/${group1Object.id}/apps`)
            .query({ access_token: owner.token })
            .send({ appIds: [app.id] });

        expect(response.status).to.equal(200);
    });

    it('did set app access', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/groups`)
            .query({ access_token: owner.token });

        const group1 = response.body.groups.filter(g => g.id === group1Object.id)[0];
        expect(group1.appIds).to.eql([app.id]);
    });

    it('remove user from group', async function () {
        const response = await superagent.put(`${serverUrl}/api/v1/users/${user.id}/groups`)
            .query({ access_token: owner.token })
            .send({ groupIds: [ group0Object.id ]});

        expect(response.status).to.equal(204);
    });

    it('cannot remove without token', async function () {
        const response = await superagent.del(`${serverUrl}/api/v1/groups/externals`)
            .ok(() => true);

        expect(response.status).to.equal(401);
    });

    it('can clear users of a group', async function () {
        const response = await superagent.put(`${serverUrl}/api/v1/groups/${group1Object.id}/members`)
            .query({ access_token: owner.token })
            .send({ userIds: [ ]});

        expect(response.status).to.equal(200);
    });

    it('can remove empty group', async function () {
        const response = await superagent.del(`${serverUrl}/api/v1/groups/${group1Object.id}`)
            .query({ access_token: owner.token });

        expect(response.status).to.equal(204);
    });

    it('can remove non-empty group', async function () {
        const response = await superagent.del(`${serverUrl}/api/v1/groups/${group0Object.id}`)
            .query({ access_token: owner.token });

        expect(response.status).to.equal(204);
    });
});
