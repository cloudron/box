'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

describe('Tokens API', function () {
    const { setup, cleanup, serverUrl, owner } = common;

    before(setup);
    after(cleanup);

    let token, readOnlyToken;

    describe('CRUD', function () {
        it('cannot create token with bad name', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/tokens`)
                .query({ access_token: owner.token })
                .send({ name: new Array(128).fill('s').join('') })
                .ok(() => true);
            expect(response.status).to.equal(400);
        });

        it('can create token', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/tokens`)
                .query({ access_token: owner.token })
                .send({ name: 'mytoken1' });

            expect(response.status).to.equal(201);
            expect(response.body).to.be.a('object');
            token = response.body;
        });

        it('can create read-only token', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/tokens`)
                .query({ access_token: owner.token })
                .send({ name: 'mytoken1', scope: { '*': 'r' }});

            expect(response.status).to.equal(201);
            expect(response.body).to.be.a('object');
            readOnlyToken = response.body;
        });

        it('cannot create read-only token with invalid scope', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/tokens`)
                .query({ access_token: owner.token })
                .send({ name: 'mytoken1', scope: { 'foobar': 'rw' }})
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('can list tokens', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/tokens`)
                .query({ access_token: owner.token });
            expect(response.status).to.equal(200);
            expect(response.body.tokens.length).to.be(3); // one is owner token on activation
            const tokenIds = response.body.tokens.map(t => t.id);
            expect(tokenIds).to.contain(token.id);
            expect(tokenIds).to.contain(readOnlyToken.id);
        });

        it('can get token', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/tokens/${token.id}`)
                .query({ access_token: owner.token });
            expect(response.status).to.equal(200);
            expect(response.body.id).to.be(token.id);
        });

        it('can delete token', async function () {
            const response = await superagent.del(`${serverUrl}/api/v1/tokens/${token.id}`)
                .query({ access_token: owner.token });
            expect(response.status).to.equal(204);
        });
    });

    describe('allowedIpRanges', function () {
        let allowedRangeToken;

        it('cannot create token with bad range', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/tokens`)
                .query({ access_token: owner.token })
                .send({ name: 'mytoken1', allowedIpRanges: 'What' })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('can create token with valid range', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/tokens`)
                .query({ access_token: owner.token })
                .send({ name: 'mytoken1', allowedIpRanges: '#this is localhost\n10.0.0.0/8' });

            expect(response.status).to.equal(201);
            allowedRangeToken = response.body;
        });

        it('cannot use access restricted token', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/tokens`)
                .query({ access_token: allowedRangeToken.accessToken })
                .ok(() => true);
            expect(response.status).to.equal(401);
        });
    });

    describe('readonly token', function () {
        it('cannot create token with read only token', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/tokens`)
                .query({ access_token: readOnlyToken.accessToken })
                .send({ name: 'somename' })
                .ok(() => true);

            expect(response.status).to.equal(403);
        });

        it('can use read only token to list domains', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/domains`)
                .query({ access_token: readOnlyToken.accessToken })
                .ok(() => true);

            expect(response.status).to.equal(200);
            expect(response.body.domains.length).to.be(1);
        });

        it('cannot use read only token for creating a domain', async function () {
            const DOMAIN_0 = {
                domain: 'domain0.com',
                zoneName: 'domain0.com',
                provider: 'noop',
                config: { },
                tlsConfig: {
                    provider: 'fallback'
                }
            };

            const response = await superagent.post(`${serverUrl}/api/v1/domains`)
                .query({ access_token: readOnlyToken.accessToken })
                .send(DOMAIN_0)
                .ok(() => true);

            expect(response.status).to.equal(403);
        });

        it('cannot get non-existent token', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/tokens/foobar`)
                .query({ access_token: owner.token })
                .ok(() => true);
            expect(response.status).to.equal(404);
        });
    });
});
