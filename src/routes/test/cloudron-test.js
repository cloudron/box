'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const constants = require('../../constants.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

describe('Cloudron', function () {
    const { setup, cleanup, serverUrl, owner, user, dashboardFqdn } = common;

    before(setup);
    after(cleanup);

    describe('config', function () {
        it('cannot get config without token', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/dashboard/config`)
                .ok(() => true);

            expect(response.status).to.equal(401);
        });

        it('can get config (admin)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/dashboard/config`)
                .query({ access_token: owner.token });

            expect(response.status).to.equal(200);
            expect(response.body.apiServerOrigin).to.eql('http://localhost:6060');
            expect(response.body.webServerOrigin).to.eql('https://cloudron.io');
            expect(response.body.adminFqdn).to.eql(dashboardFqdn);
            expect(response.body.version).to.eql(constants.VERSION);
            expect(response.body.cloudronName).to.be.a('string');
        });

        it('can get config (non-admin)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/dashboard/config`)
                .query({ access_token: user.token });

            expect(response.status).to.equal(200);
            expect(response.body.apiServerOrigin).to.eql('http://localhost:6060');
            expect(response.body.webServerOrigin).to.eql('https://cloudron.io');
            expect(response.body.adminFqdn).to.eql(dashboardFqdn);
            expect(response.body.version).to.eql(constants.VERSION);
            expect(response.body.cloudronName).to.be.a('string');
        });
    });

    describe('account setup', function () {
        it('succeeds without pre-set username and display name', async function () {
            const USER = {
                email: 'setup1@account.com',
                password: 'test?!3434543534',
                username: 'setupuser1',
                displayName: 'setup user1',
            };

            const response = await superagent.post(`${serverUrl}/api/v1/users`)
                .query({ access_token: owner.token })
                .send({ email: USER.email });
            expect(response.status).to.equal(201);
            USER.id = response.body.id;

            const response2 = await superagent.get(`${serverUrl}/api/v1/users/${USER.id}/invite_link`)
                .query({ access_token: owner.token })
                .ok(() => true);
            expect(response2.status).to.equal(200);

            const response3 = await superagent.post(`${serverUrl}/api/v1/auth/setup_account`)
                .send({
                    inviteToken: require('url').parse(response2.body.inviteLink, true).query.inviteToken,
                    password: USER.password,
                    username: USER.username,
                    displayName: USER.displayName
                })
                .ok(() => true);
            expect(response3.status).to.equal(201);
            expect(response3.body.accessToken).to.be.a('string');

            const response4 = await superagent.get(`${serverUrl}/api/v1/users/${USER.id}`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response4.status).to.equal(200);
            expect(response4.body.username).to.equal(USER.username);
            expect(response4.body.displayName).to.equal(USER.displayName);

            const response5 = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: USER.username, password: USER.password });
            expect(response5.status).to.equal(200);
        });

        it('succeeds and overwrites with pre-set username and display name', async function () {
            const USER = {
                email: 'setup2@account.com',
                password: 'test?!3434543534',
                username: 'presetup2',
                displayName: 'setup user2',
            };

            const response = await superagent.post(`${serverUrl}/api/v1/users`)
                .query({ access_token: owner.token })
                .send({ email: USER.email, username: 'presetup2', displayName: 'pre setup' });
            expect(response.status).to.equal(201);
            USER.id = response.body.id;

            const response2 = await superagent.get(`${serverUrl}/api/v1/users/${USER.id}/invite_link`)
                .query({ access_token: owner.token })
                .ok(() => true);
            expect(response2.status).to.equal(200);

            const response3 = await superagent.post(`${serverUrl}/api/v1/auth/setup_account`)
                .send({
                    inviteToken: require('url').parse(response2.body.inviteLink, true).query.inviteToken,
                    password: USER.password,
                    username: 'setupuser2', // this will cause a conflict. cannot change username
                    displayName: USER.displayName
                })
                .ok(() => true);
            expect(response3.status).to.equal(409);

            const response4 = await superagent.post(`${serverUrl}/api/v1/auth/setup_account`)
                .send({
                    inviteToken: require('url').parse(response2.body.inviteLink, true).query.inviteToken,
                    password: USER.password,
                    displayName: USER.displayName
                })
                .ok(() => true);
            expect(response4.status).to.equal(201);
            expect(response4.body.accessToken).to.be.a('string');

            const response5 = await superagent.get(`${serverUrl}/api/v1/users/${USER.id}`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response5.status).to.equal(200);
            expect(response5.body.username).to.equal(USER.username);
            expect(response5.body.displayName).to.equal(USER.displayName);

            const response6 = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: USER.username, password: USER.password });
            expect(response6.status).to.equal(200);
        });

        it('succeeds and does not overwrite pre-set username and display name if profiles are locked', async function () {
            const USER = {
                email: 'setup3@account.com',
                password: 'test?!3434543534',
                username: 'setupuser3',
                displayName: 'setup user3',
            };

            const response0 = await superagent.post(`${serverUrl}/api/v1/user_directory/profile_config`)
                .query({ access_token: owner.token })
                .send({ lockUserProfiles: true, mandatory2FA: false });
            expect(response0.status).to.equal(200);

            const response = await superagent.post(`${serverUrl}/api/v1/users`)
                .query({ access_token: owner.token })
                .send({ email: USER.email, username: 'presetup3', displayName: 'pre setup3' });
            expect(response.status).to.equal(201);
            USER.id = response.body.id;

            const response2 = await superagent.get(`${serverUrl}/api/v1/users/${USER.id}/invite_link`)
                .query({ access_token: owner.token })
                .ok(() => true);
            expect(response2.status).to.equal(200);

            const response3 = await superagent.post(`${serverUrl}/api/v1/auth/setup_account`)
                .send({
                    inviteToken: require('url').parse(response2.body.inviteLink, true).query.inviteToken,
                    password: USER.password,
                    username: USER.username, // ignored
                    displayName: USER.displayName // ignored
                })
                .ok(() => true);
            expect(response3.status).to.equal(201);
            expect(response3.body.accessToken).to.be.a('string');

            const response4 = await superagent.get(`${serverUrl}/api/v1/users/${USER.id}`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response4.status).to.equal(200);
            expect(response4.body.username).to.equal('presetup3'); // what the admin provided
            expect(response4.body.displayName).to.equal('pre setup3'); // what the admin provided

            const response5 = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: 'presetup3', password: USER.password });
            expect(response5.status).to.equal(200);
        });
    });

    describe('login', function () {
        it('cannot login without body', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .ok(() => true);
            expect(response.status).to.equal(400);
        });

        it('cannot login without username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ password: owner.password })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot login without password', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: owner.username })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot login with empty username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: '', password: owner.password })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot login with empty password', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: owner.username, password: '' })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        it('cannot login with unknown username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: 'somethingrandom', password: owner.password })
                .ok(() => true);

            expect(response.status).to.equal(401);
        });

        it('cannot login with unknown email', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: 'randomgemail', password: owner.password })
                .ok(() => true);

            expect(response.status).to.equal(401);
        });

        it('cannot login with wrong password', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: owner.username, password: owner.password.toUpperCase() })
                .ok(() => true);

            expect(response.status).to.equal(401);
        });

        it('can login with username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: owner.username, password: owner.password });

            expect(response.status).to.equal(200);
            expect(new Date(response.body.expires).toString()).to.not.be('Invalid Date');
            expect(response.body.accessToken).to.be.a('string');
        });

        it('can login with uppercase username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: owner.username.toUpperCase(), password: owner.password });

            expect(response.status).to.equal(200);
            expect(new Date(response.body.expires).toString()).to.not.be('Invalid Date');
            expect(response.body.accessToken).to.be.a('string');
        });

        it('can login with email', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: owner.email, password: owner.password });

            expect(response.status).to.equal(200);
            expect(new Date(response.body.expires).toString()).to.not.be('Invalid Date');
            expect(response.body.accessToken).to.be.a('string');
        });

        it('can login with uppercase email', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/auth/login`)
                .send({ username: owner.email.toUpperCase(), password: owner.password });

            expect(response.status).to.equal(200);
            expect(new Date(response.body.expires).toString()).to.not.be('Invalid Date');
            expect(response.body.accessToken).to.be.a('string');
        });
    });

    describe('languages', function () {
        it('succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/cloudron/languages`);

            expect(response.status).to.equal(200);
            expect(response.body.languages).to.be.an('array');
            expect(response.body.languages.indexOf('en')).to.not.equal(-1);
        });
    });
});
