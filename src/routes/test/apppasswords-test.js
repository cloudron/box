/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

describe('App Passwords', function () {
    const { setup, cleanup, serverUrl, user } = common;

    before(setup);
    after(cleanup);

    describe('app password', function () {
        it('cannot add app password with invalid token', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/app_passwords`)
                .query({ access_token: user.token + 'xx' })
                .send({ name: 'my-device', identifier: 'someapp' })
                .ok(() => true);

            expect(response.status).to.equal(401);
        });

        it('cannot add app password without name', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/app_passwords`)
                .query({ access_token: user.token })
                .send({ identifier: 'someapp' })
                .ok(() => true);

            expect(response.status).to.equal(400);
        });

        let pwd;
        it('can add app password', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/app_passwords`)
                .query({ access_token: user.token })
                .send({ name: 'my-device', identifier: 'someapp' });

            expect(response.status).to.equal(201);
            expect(response.body.password).to.be.a('string');
            pwd = response.body;
        });

        it('can get app passwords', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/app_passwords`)
                .query({ access_token: user.token });

            expect(response.status).to.equal(200);
            expect(response.body.appPasswords).to.be.an(Array);
            expect(response.body.appPasswords.length).to.be(1);
            expect(response.body.appPasswords[0].name).to.be('my-device');
            expect(response.body.appPasswords[0].identifier).to.be('someapp');
            expect(response.body.appPasswords[0].hashedPassword).to.be(undefined);
            expect(response.body.appPasswords[0].password).to.be(undefined);
        });

        it('can get app password', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/app_passwords/${pwd.id}`)
                .query({ access_token: user.token });

            expect(response.status).to.equal(200);
            expect(response.body.name).to.be('my-device');
            expect(response.body.identifier).to.be('someapp');
            expect(response.body.hashedPassword).to.be(undefined);
            expect(response.body.password).to.be(undefined);
        });

        it('can del app password', async function () {
            const response = await superagent.del(`${serverUrl}/api/v1/app_passwords/${pwd.id}`)
                .query({ access_token: user.token });

            expect(response.status).to.equal(204);
        });
    });
});
