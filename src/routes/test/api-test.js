/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js'),
    timers = require('timers/promises'),
    tokens = require('../../tokens.js');

describe('API', function () {
    const { setup, cleanup, serverUrl, owner, user } = common;

    before(setup);
    after(cleanup);

    describe('express handlers', function () {
        it('does not crash with invalid JSON', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/users`)
                .query({ access_token: owner.token })
                .set('content-type', 'application/json')
                .send('some invalid non-strict json')
                .ok(() => true);

            expect(response.status).to.equal(400);
            expect(response.body.message).to.be('Failed to parse body');
        });

        it('does not crash with invalid string', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/users`)
                .query({ access_token: owner.token })
                .set('content-type', 'application/x-www-form-urlencoded')
                .send('some string')
                .ok(() => true);

            expect(response.status).to.equal(400);
        });
    });

    describe('authentication', function () {
        it('cannot get userInfo only with basic auth', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/users/${user.id}`)
                .auth(owner.username, owner.password)
                .ok(() => true);

            expect(response.status).to.equal(401);
        });

        it('cannot get userInfo with invalid token (token length)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/users/${user.id}`)
                .query({ access_token: 'x' + owner.token })
                .ok(() => true);

            expect(response.status).to.equal(401);
        });


        it('can get userInfo with token in auth header', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/users/${user.id}`)
                .set('Authorization', 'Bearer ' + owner.token);

            expect(response.status).to.equal(200);
            expect(response.body.username).to.equal(user.username.toLowerCase());
            expect(response.body.email).to.equal(user.email.toLowerCase());
        });

        it('cannot get userInfo with invalid token in auth header', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/users/${user.id}`)
                .set('Authorization', 'Bearer ' + 'x' + owner.token)
                .ok(() => true);

            expect(response.status).to.equal(401);
        });

        it('cannot get userInfo with expired token', async function () {
            const token2 = {
                name: 'token2',
                identifier: owner.id,
                clientId: 'clientid-2',
                expires: Date.now() + 2000, // expires in 3 seconds
                lastUsedTime: null,
                allowedIpRanges: '127.0.0.1'
            };

            const result = await tokens.add(token2);
            token2.id = result.id;
            token2.accessToken = result.accessToken;

            const response = await superagent.get(`${serverUrl}/api/v1/users/${user.id}`)
                .set('Authorization', 'Bearer ' + token2.accessToken);
            expect(response.status).to.be(200);

            await timers.setTimeout(3000); // wait for token to expire

            const response2 = await superagent.get(`${serverUrl}/api/v1/users/${user.id}`)
                .set('Authorization', 'Bearer ' + token2.accessToken)
                .ok(() => true);
            expect(response2.status).to.be(401);

        });
    });
});
