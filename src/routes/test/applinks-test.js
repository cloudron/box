'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const applinks = require('../../applinks.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('../../superagent.js');

describe('AppLinks API', function () {
    const { setup, cleanup, serverUrl, owner } = common;

    before(setup);
    after(cleanup);

    let applinkId;

    it('can add applink', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/applinks`)
            .query({ access_token: owner.token })
            .send({ label: 'Berlin', tags: ['city'], upstreamUri: 'https://www.berlin.de', accessRestriction: null });
        expect(response.status).to.equal(201);
        expect(response.body.id).to.be.ok();

        applinkId = response.body.id;
    });

    it('cannot get random applink', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/applinks/random`)
            .query({ access_token: owner.token })
            .ok(() => true);
        expect(response.status).to.equal(404);
    });

    it('cannot get valid applink', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/applinks/${applinkId}`)
            .query({ access_token: owner.token })
            .ok(() => true);
        expect(response.status).to.equal(200);
        expect(response.body.upstreamUri).to.be('https://www.berlin.de');
    });

    it('can get get icon', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/applinks/${applinkId}/icon`)
            .query({ access_token: owner.token })
            .ok(() => true);
        expect(response.status).to.equal(200);
        expect(response.headers['content-type']).to.be('application/octet-stream');
    });

    it('can update applink tags', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/applinks/${applinkId}`)
            .query({ access_token: owner.token })
            .send({ tags: ['city', 'germany'] });
        expect(response.status).to.equal(200);

        const result = await applinks.get(applinkId);
        expect(result.tags).to.eql(['city', 'germany']);
    });

    it('can list applinks', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/applinks`)
            .query({ access_token: owner.token });
        expect(response.status).to.equal(200);
        expect(response.body.applinks.length).to.equal(1);
        expect(response.body.applinks[0].upstreamUri).to.be('https://www.berlin.de');
    });

    it('cannot del random applink', async function () {
        const response = await superagent.del(`${serverUrl}/api/v1/applinks/random`)
            .query({ access_token: owner.token })
            .ok(() => true);
        expect(response.status).to.equal(404);
    });

    it('can del applink', async function () {
        const response = await superagent.del(`${serverUrl}/api/v1/applinks/${applinkId}`)
            .query({ access_token: owner.token })
            .ok(() => true);
        expect(response.status).to.equal(204);

        const result = await applinks.get(applinkId);
        expect(result).to.be(null);
    });
});
