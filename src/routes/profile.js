'use strict';

exports = module.exports = {
    canEditProfile,
    get,
    setDisplayName,
    setEmail,
    setFallbackEmail,
    getAvatar,
    setAvatar,
    setLanguage,
    getBackgroundImage,
    setBackgroundImage,
    setPassword,
    setTwoFactorAuthenticationSecret,
    enableTwoFactorAuthentication,
    disableTwoFactorAuthentication,
    setNotificationConfig
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    constants = require('../constants.js'),
    crypto = require('crypto'),
    dashboard = require('../dashboard.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    https = require('https'),
    path = require('path'),
    paths = require('../paths.js'),
    safe = require('safetydance'),
    userDirectory = require('../user-directory.js'),
    users = require('../users.js');

async function canEditProfile(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const [error, profileConfig] = await safe(userDirectory.getProfileConfig());
    if (error) return next(BoxError.toHttpError(error));

    if (profileConfig.lockUserProfiles) return next(new HttpError(403, 'admin has disallowed users from editing profiles'));

    next();
}

async function get(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const [error, backgroundImage] = await safe(users.getBackgroundImage(req.user.id));
    if (error) return next(BoxError.toHttpError(error));

    const [avatarError, avatar] = await safe(users.getAvatar(req.user.id));
    if (avatarError) return next(BoxError.toHttpError(error));

    let avatarType;
    if (avatar.equals(constants.AVATAR_GRAVATAR)) avatarType = constants.AVATAR_GRAVATAR;
    else if (avatar.equals(constants.AVATAR_NONE)) avatarType = constants.AVATAR_NONE;
    else avatarType = constants.AVATAR_CUSTOM;

    const { fqdn:dashboardFqdn } = await dashboard.getLocation();

    next(new HttpSuccess(200, {
        id: req.user.id,
        username: req.user.username,
        email: req.user.email,
        fallbackEmail: req.user.fallbackEmail,
        displayName: req.user.displayName,
        twoFactorAuthenticationEnabled: req.user.twoFactorAuthenticationEnabled,
        role: req.user.role,
        source: req.user.source,
        hasBackgroundImage: !!backgroundImage,
        language: req.user.language,
        notificationConfig: req.user.notificationConfig,
        avatarUrl: `https://${dashboardFqdn}/api/v1/profile/avatar/${req.user.id}`,
        avatarType: avatarType.toString() // this is a Buffer
    }));
}

async function setEmail(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.body, 'object');

    if ('email' in req.body && typeof req.body.email !== 'string') return next(new HttpError(400, 'email must be string'));

    const [error] = await safe(users.update(req.user, { email: req.body.email }, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204));
}

async function setFallbackEmail(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.body, 'object');

    if ('fallbackEmail' in req.body && typeof req.body.fallbackEmail !== 'string') return next(new HttpError(400, 'fallbackEmail must be string'));

    const [error] = await safe(users.update(req.user, { fallbackEmail: req.body.fallbackEmail }, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204));
}

async function setDisplayName(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.body, 'object');

    if ('displayName' in req.body && typeof req.body.displayName !== 'string') return next(new HttpError(400, 'displayName must be string'));

    const [error] = await safe(users.update(req.user, { displayName: req.body.displayName }, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204));
}

async function setLanguage(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.body, 'object');

    if ('language' in req.body && typeof req.body.language !== 'string') return next(new HttpError(400, 'language must be string'));

    const [error] = await safe(users.update(req.user, { language: req.body.language }, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204));
}

async function setAvatar(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    let avatar = typeof req.body.avatar === 'string' ? Buffer.from(req.body.avatar, 'utf8') : null;

    if (req.files && req.files.avatar) {
        avatar = safe.fs.readFileSync(req.files.avatar.path);
        safe.fs.unlinkSync(req.files.avatar.path);
        if (!avatar) return next(BoxError.toHttpError(new BoxError(BoxError.FS_ERROR, safe.error.message)));
    } else if (!avatar || (!avatar.equals(constants.AVATAR_GRAVATAR) && !avatar.equals(constants.AVATAR_NONE))) {
        return next(new HttpError(400, `avatar must be a file, ${constants.AVATAR_GRAVATAR} or ${constants.AVATAR_NONE}`));
    }

    const [error] = await safe(users.setAvatar(req.user.id, avatar));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, {}));
}

async function getAvatar(req, res, next) {
    assert.strictEqual(typeof req.params.identifier, 'string');

    const [,userId, ext] = req.params.identifier.match(/^(.*?)(?:\.(\w+))?$/);
    const [userError, user] = await safe(users.get(userId));
    if (userError) return next(BoxError.toHttpError(userError));

    const [avatarError, avatar] = await safe(users.getAvatar(userId));
    if (avatarError) return next(BoxError.toHttpError(avatarError));

    if (avatar.equals(constants.AVATAR_GRAVATAR)) {
        const gravatarHash = crypto.createHash('md5').update(user.email).digest('hex');
        https.get(`https://www.gravatar.com/avatar/${gravatarHash}.jpg`, function (upstreamRes) {
            if (upstreamRes.status !== 200) {
                console.error('Gravatar error:', upstreamRes.status);
                return res.status(404).end();
            }

            res.set('content-type', 'image/jpeg');
            upstreamRes.pipe(res);
        }).on('error', (e) => {
            console.error('Gravatar error:', e.message);
            return res.status(404).end();
        });
    } else if (avatar.equals(constants.AVATAR_NONE)) {
        if (ext) {
            res.sendFile(path.join(paths.DASHBOARD_DIR, '/img/avatar-default-symbolic.png'));
        } else {
            res.sendFile(path.join(paths.DASHBOARD_DIR, '/img/avatar-default-symbolic.svg'));
        }
    } else {
        res.set('Content-Type', 'image/png');
        res.send(avatar);
    }
}

async function setBackgroundImage(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    let backgroundImage = null;

    if (req.files && req.files.backgroundImage) {
        backgroundImage = safe.fs.readFileSync(req.files.backgroundImage.path);
        safe.fs.unlinkSync(req.files.backgroundImage.path);
        if (!backgroundImage) return next(BoxError.toHttpError(new BoxError(BoxError.FS_ERROR, safe.error.message)));
    }

    const [error] = await safe(users.setBackgroundImage(req.user.id, backgroundImage));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, {}));
}

async function getBackgroundImage(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const [error, backgroundImage] = await safe(users.getBackgroundImage(req.user.id));
    if (error) return next(BoxError.toHttpError(error));

    res.send(backgroundImage);
}

async function setPassword(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.user, 'object');

    if (typeof req.body.newPassword !== 'string') return next(new HttpError(400, 'newPassword must be a string'));

    const [error] = await safe(users.setPassword(req.user, req.body.newPassword, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204));
}

async function setTwoFactorAuthenticationSecret(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const [error, result] = await safe(users.setTwoFactorAuthenticationSecret(req.user, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { secret: result.secret, qrcode: result.qrcode }));
}

async function enableTwoFactorAuthentication(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.user, 'object');

    if (!req.body.totpToken || typeof req.body.totpToken !== 'string') return next(new HttpError(400, 'totpToken must be a nonempty string'));

    const [error] = await safe(users.enableTwoFactorAuthentication(req.user, req.body.totpToken, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204, {}));
}

async function disableTwoFactorAuthentication(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const [error] = await safe(users.disableTwoFactorAuthentication(req.user, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204, {}));
}

async function setNotificationConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.user, 'object');

    if (!Array.isArray(req.body.notificationConfig)) return next(new HttpError(400, 'notificationConfig must be an array of strings'));
    if (req.body.notificationConfig.some(k => typeof k !== 'string')) return next(new HttpError(400, 'notificationConfig must be an array of strings'));

    const [error] = await safe(users.setNotificationConfig(req.user, req.body.notificationConfig, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204, {}));
}
