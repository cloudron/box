'use strict';

exports = module.exports = {
    getConfig,
    setConfig,
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    directoryServer = require('../directoryserver.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance');

async function getConfig(req, res, next) {
    const [error, config] = await safe(directoryServer.getConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, config));
}

async function setConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.enabled !== 'boolean') return next(new HttpError(400, 'enabled must be a boolean'));

    // these can still be there when enabled=false so that the values are preserved
    if ('secret' in req.body && typeof req.body.secret !== 'string') return next(new HttpError(400, 'secret must be a string'));
    if ('allowlist' in req.body && typeof req.body.allowlist !== 'string') return next(new HttpError(400, 'allowlist must be a string'));

    const [error] = await safe(directoryServer.setConfig(req.body, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}
