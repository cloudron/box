'use strict';

exports = module.exports = {
    getAutoupdatePattern,
    setAutoupdatePattern,

    getUpdateInfo,
    update,
    checkForUpdates
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance'),
    updater = require('../updater.js'),
    updateChecker = require('../updatechecker.js');

async function getAutoupdatePattern(req, res, next) {
    const [error, pattern] = await safe(updater.getAutoupdatePattern());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { pattern }));
}

async function setAutoupdatePattern(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.pattern !== 'string') return next(new HttpError(400, 'pattern is required'));

    const [error] = await safe(updater.setAutoupdatePattern(req.body.pattern));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function update(req, res, next) {
    if ('skipBackup' in req.body && typeof req.body.skipBackup !== 'boolean') return next(new HttpError(400, 'skipBackup must be a boolean'));

    // this only initiates the update, progress can be checked via the progress route
    const [error, taskId] = await safe(updater.updateToLatest(req.body, AuditSource.fromRequest(req)));
    if (error && error.reason === BoxError.NOT_FOUND) return next(new HttpError(422, error.message));
    if (error && error.reason === BoxError.BAD_STATE) return next(new HttpError(409, error.message));
    if (error) return next(new HttpError(500, error));

    next(new HttpSuccess(202, { taskId }));
}

function getUpdateInfo(req, res, next) {
    next(new HttpSuccess(200, { update: updateChecker.getUpdateInfo() }));
}

async function checkForUpdates(req, res, next) {
    // it can take a while sometimes to get all the app updates one by one
    req.clearTimeout();

    await updateChecker.checkForUpdates({ automatic: false });
    next(new HttpSuccess(200, { update: updateChecker.getUpdateInfo() }));
}
