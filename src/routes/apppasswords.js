'use strict';

exports = module.exports = {
    list,
    get,
    del,
    add
};

const appPasswords = require('../apppasswords.js'),
    assert = require('assert'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance');

async function get(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.params.id, 'string');

    const [error, result] = await safe(appPasswords.get(req.params.id));
    if (error) return next(BoxError.toHttpError(error));
    if (!result) return next(new HttpError(404, 'appPassword not found'));

    next(new HttpSuccess(200, appPasswords.removePrivateFields(result)));
}

async function add(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be string'));
    if (typeof req.body.identifier !== 'string') return next(new HttpError(400, 'identifier must be string'));

    const [error, result] = await safe(appPasswords.add(req.user.id, req.body.identifier, req.body.name));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(201, { id: result.id, password: result.password }));
}

async function list(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    let [error, result] = await safe(appPasswords.list(req.user.id));
    if (error) return next(BoxError.toHttpError(error));

    result = result.map(appPasswords.removePrivateFields);
    next(new HttpSuccess(200, { appPasswords: result }));
}

async function del(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.params.id, 'string');

    // TODO: verify userId owns the id ?
    const [error] = await safe(appPasswords.del(req.params.id));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204, {}));
}
