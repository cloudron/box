'use strict';

exports = module.exports = {
    getCloudronName,
    setCloudronName,
    getCloudronAvatar,
    setCloudronAvatar,
    getCloudronBackground,
    setCloudronBackground,
    getFooter,
    setFooter,
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    branding = require('../branding.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance');

async function getFooter(req, res, next) {
    const [error, footer] = await safe(branding.getFooter());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { footer }));
}

async function setFooter(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.footer !== 'string') return next(new HttpError(400, 'footer is required'));

    const [error] = await safe(branding.setFooter(req.body.footer, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function setCloudronName(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name is required'));

    const [error] = await safe(branding.setCloudronName(req.body.name, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getCloudronName(req, res, next) {
    const [error, name] = await safe(branding.getCloudronName());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { name }));
}

async function setCloudronAvatar(req, res, next) {
    assert.strictEqual(typeof req.files, 'object');

    if (!req.files.avatar) return next(new HttpError(400, 'avatar must be provided'));
    const avatar = safe.fs.readFileSync(req.files.avatar.path);
    safe.fs.unlinkSync(req.files.avatar.path);
    if (!avatar) return next(500, safe.error.message);

    const [error] = await safe(branding.setCloudronAvatar(avatar, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getCloudronAvatar(req, res, next) {
    const [error, avatar] = await safe(branding.getCloudronAvatar());
    if (error) return next(BoxError.toHttpError(error));

    // avoid caching the avatar on the client to see avatar changes immediately
    res.set('Cache-Control', 'no-cache');

    res.set('Content-Type', 'image/png');
    res.status(200).send(avatar);
}

async function setCloudronBackground(req, res, next) {
    assert.strictEqual(typeof req.files, 'object');

    // if not provided it clears the background image from settings
    let backgroundImage = null;

    if (req.files.background) {
        backgroundImage = safe.fs.readFileSync(req.files.background.path);
        safe.fs.unlinkSync(req.files.background.path);
        if (!backgroundImage) return next(500, safe.error.message);
    }

    const [error] = await safe(branding.setCloudronBackground(backgroundImage));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, {}));
}

async function getCloudronBackground(req, res, next) {
    const [error, background] = await safe(branding.getCloudronBackground());
    if (error) return next(BoxError.toHttpError(error));

    if (!background) return next(new HttpError(404, 'no background set'));

    // avoid caching on the client to see changes immediately
    res.set('Cache-Control', 'no-cache');

    res.set('Content-Type', 'image/webp');
    res.status(200).send(background);
}
