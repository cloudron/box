'use strict';

exports = module.exports = {
    load,

    get,
    list,
    add,
    setName,
    del,
    setMembers,
    setAllowedApps
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    groups = require('../groups.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance');

async function load(req, res, next) {
    assert.strictEqual(typeof req.params.groupId, 'string');

    const [error, result] = await safe(groups.getWithMembers(req.params.groupId));
    if (error) return next(BoxError.toHttpError(error));
    if (!result) return next(new HttpError(404, 'Group not found'));

    req.resource = result;

    next();
}

async function add(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be string'));

    const [error, group] = await safe(groups.add({ name: req.body.name, source : '' }, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(201, { id: group.id, name: group.name }));
}

async function get(req, res, next) {
    assert.strictEqual(typeof req.params.groupId, 'string');

    next(new HttpSuccess(200, req.resource));
}

async function setName(req, res, next) {
    assert.strictEqual(typeof req.resource, 'object');
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be a string'));
    const [error] = await safe(groups.setName(req.resource, req.body.name, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function setMembers(req, res, next) {
    assert.strictEqual(typeof req.resource, 'object');

    if (!req.body.userIds) return next(new HttpError(404, 'missing or invalid userIds fields'));
    if (!Array.isArray(req.body.userIds)) return next(new HttpError(404, 'userIds must be an array'));
    if (req.body.userIds.some((u) => typeof u !== 'string')) return next(new HttpError(400, 'userIds array must contain strings'));

    const [error] = await safe(groups.setMembers(req.resource, req.body.userIds, { skipSourceCheck: false }, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function setAllowedApps(req, res, next) {
    assert.strictEqual(typeof req.resource, 'object');

    if (!req.body.appIds) return next(new HttpError(404, 'missing or invalid userIds fields'));
    if (!Array.isArray(req.body.appIds)) return next(new HttpError(404, 'appIds must be an array'));
    if (req.body.appIds.some((a) => typeof a !== 'string')) return next(new HttpError(400, 'appIds array must contain strings'));

    const [error] = await safe(groups.setAllowedApps(req.resource, req.body.appIds, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function list(req, res, next) {
    const [error, result] = await safe(groups.listWithMembers());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { groups: result }));
}

async function del(req, res, next) {
    assert.strictEqual(typeof req.params.groupId, 'string');

    const [error] = await safe(groups.del(req.resource, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204));
}
