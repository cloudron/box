'use strict';

exports = module.exports = {
    listByUser,
    add,
    get,
    update,
    del,
    getIcon,

    load
};

const assert = require('assert'),
    applinks = require('../applinks.js'),
    BoxError = require('../boxerror.js'),
    safe = require('safetydance'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess;

async function load(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    const [error, result] = await safe(applinks.get(req.params.id));
    if (error) return next(BoxError.toHttpError(error));
    if (!result) return next(new HttpError(404, 'Applink not found'));

    req.resource = result;

    next();
}

async function listByUser(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const [error, result] = await safe(applinks.listByUser(req.user));
    if (error) return next(BoxError.toHttpError(error));

    // we have a separate route for this
    result.forEach(function (a) { delete a.icon; });

    next(new HttpSuccess(200, { applinks: result }));
}

async function add(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    // required
    if (!req.body.upstreamUri || typeof req.body.upstreamUri !== 'string') return next(new HttpError(400, 'upstreamUri must be a non-empty string'));
    if (typeof req.body.accessRestriction !== 'object') return next(new HttpError(400, 'accessRestriction must be an object'));

    if ('label' in req.body && typeof req.body.label !== 'string') return next(new HttpError(400, 'label must be a string'));
    if ('tags' in req.body && !Array.isArray(req.body.tags)) return next(new HttpError(400, 'tags must be an array with strings'));
    if ('icon' in req.body && typeof req.body.icon !== 'string') return next(new HttpError(400, 'icon must be a string'));

    const [error, id] = await safe(applinks.add(req.body));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(201, { id }));
}

async function get(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    // we have a separate route for this
    delete req.resource.icon;

    next(new HttpSuccess(200, req.resource));
}

async function update(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if ('upstreamUri' in req.body && (!req.body.upstreamUri || typeof req.body.upstreamUri !== 'string')) return next(new HttpError(400, 'upstreamUri must be a non-empty string'));
    if ('accessRestriction' in req.body && typeof req.body.accessRestriction !== 'object') return next(new HttpError(400, 'accessRestriction must be an object'));
    if ('label' in req.body && typeof req.body.label !== 'string') return next(new HttpError(400, 'label must be a string'));
    if ('tags' in req.body && !Array.isArray(req.body.tags)) return next(new HttpError(400, 'tags must be an array with strings'));
    if ('icon' in req.body && typeof req.body.icon !== 'string') return next(new HttpError(400, 'icon must be a string'));

    const [error] = await safe(applinks.update(req.resource, req.body));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function del(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    const [error] = await safe(applinks.del(req.resource));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204));
}

async function getIcon(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    res.send(req.resource.icon);
}
