'use strict';

exports = module.exports = {
    getConfig,

    startPrepareLocation,
    changeLocation
};

const AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    constants = require('../constants.js'),
    dashboard = require('../dashboard.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance');

async function getConfig(req, res, next) {
    const [error, cloudronConfig] = await safe(dashboard.getConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, cloudronConfig));
}

async function startPrepareLocation(req, res, next) {
    if (!req.body.domain || typeof req.body.domain !== 'string') return next(new HttpError(400, 'domain must be a string'));

    const [error, taskId] = await safe(dashboard.startPrepareLocation(req.body.domain, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, { taskId }));
}

async function changeLocation(req, res, next) {
    if (!req.body.domain || typeof req.body.domain !== 'string') return next(new HttpError(400, 'domain must be a string'));

    const [error] = await safe(dashboard.changeLocation(constants.DASHBOARD_SUBDOMAIN, req.body.domain, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204, {}));
}
