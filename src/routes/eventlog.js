'use strict';

exports = module.exports = {
    get,
    list
};

const BoxError = require('../boxerror.js'),
    eventlog = require('../eventlog.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance');

async function get(req, res, next) {
    const [error, event] = await safe(eventlog.get(req.params.eventId));
    if (error) return next(BoxError.toHttpError(error));
    if (!event) return next(new HttpError(404, 'Eventlog not found'));

    next(new HttpSuccess(200, { event }));
}

async function list(req, res, next) {
    const page = typeof req.query.page !== 'undefined' ? parseInt(req.query.page) : 1;
    if (!page || page < 0) return next(new HttpError(400, 'page query param has to be a postive number'));

    const perPage = typeof req.query.per_page !== 'undefined'? parseInt(req.query.per_page) : 25;
    if (!perPage || perPage < 0) return next(new HttpError(400, 'per_page query param has to be a postive number'));

    if (req.query.actions && typeof req.query.actions !== 'string') return next(new HttpError(400, 'actions must be a comma separated string'));
    if (req.query.action && typeof req.query.action !== 'string') return next(new HttpError(400, 'action must be a string'));
    if (req.query.search && typeof req.query.search !== 'string') return next(new HttpError(400, 'search must be a string'));

    const actions = req.query.actions ? req.query.actions.split(',').map(function (s) { return s.trim(); }) : [];
    if (req.query.action) actions.push(req.query.action);

    const [error, eventlogs] = await safe(eventlog.listPaged(actions, req.query.search || null, page, perPage));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { eventlogs }));
}
