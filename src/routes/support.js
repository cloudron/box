'use strict';

exports = module.exports = {
    createTicket,

    getRemoteSupport,
    enableRemoteSupport,
};

const appstore = require('../appstore.js'),
    assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    constants = require('../constants.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance'),
    support = require('../support.js');

async function createTicket(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const VALID_TYPES = [ 'feedback', 'ticket', 'app_missing', 'app_error', 'upgrade_request', 'email_error', 'billing' ];

    if (typeof req.body.type !== 'string' || !req.body.type) return next(new HttpError(400, 'type must be string'));
    if (VALID_TYPES.indexOf(req.body.type) === -1) return next(new HttpError(400, 'unknown type'));
    if (typeof req.body.subject !== 'string' || !req.body.subject) return next(new HttpError(400, 'subject must be string'));
    if (typeof req.body.description !== 'string' || !req.body.description) return next(new HttpError(400, 'description must be string'));
    if (req.body.appId && typeof req.body.appId !== 'string') return next(new HttpError(400, 'appId must be string'));
    if (req.body.altEmail && typeof req.body.altEmail !== 'string') return next(new HttpError(400, 'altEmail must be string'));
    if (req.body.enableSshSupport && typeof req.body.enableSshSupport !== 'boolean') return next(new HttpError(400, 'enableSshSupport must be a boolean'));

    const [ticketError, result] = await safe(appstore.createTicket(Object.assign({ }, req.body, { email: req.user.email, displayName: req.user.displayName }), AuditSource.fromRequest(req)));
    if (ticketError) return next(new HttpError(503, `Error contacting cloudron.io: ${ticketError.message}. Please email ${constants.SUPPORT_EMAIL}`));

    next(new HttpSuccess(201, result));
}

async function enableRemoteSupport(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.enabled !== 'boolean') return next(new HttpError(400, 'enabled is required'));

    const [error] = await safe(support.enableRemoteSupport(req.body.enabled, AuditSource.fromRequest(req)));
    if (error) return next(new HttpError(503, 'Error enabling remote support. Try running "cloudron-support --enable-remote-support" on the server'));

    next(new HttpSuccess(202, {}));
}

async function getRemoteSupport(req, res, next) {
    const [error, enabled] = await safe(support.getRemoteSupport());
    if (error) return next(new HttpError(500, error));

    next(new HttpSuccess(200, { enabled }));
}
