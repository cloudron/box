'use strict';

exports = module.exports = {
    getTrustedIps,
    setTrustedIps,

    renewCerts,
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    reverseProxy = require('../reverseproxy.js'),
    safe = require('safetydance');

async function getTrustedIps(req, res, next) {
    const [error, trustedIps] = await safe(reverseProxy.getTrustedIps());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { trustedIps }));
}

async function setTrustedIps(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    // this is a string to allow comments
    if (typeof req.body.trustedIps !== 'string') return next(new HttpError(400, 'trustedIps must be a string'));

    const [error] = await safe(reverseProxy.setTrustedIps(req.body.trustedIps, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function renewCerts(req, res, next) {
    if ('rebuild' in req.body && typeof req.body.rebuild !== 'boolean') return next(new HttpError(400, 'rebuild must be a boolean'));

    const [error, taskId] = await safe(reverseProxy.startRenewCerts(req.body, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, { taskId }));
}
