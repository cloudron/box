'use strict';

exports = module.exports = {
    getRegistryConfig,
    setRegistryConfig
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    docker = require('../docker.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance');

async function getRegistryConfig(req, res, next) {
    const [error, registryConfig] = await safe(docker.getRegistryConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, docker.removePrivateFields(registryConfig)));
}

async function setRegistryConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.provider || typeof req.body.provider !== 'string') return next(new HttpError(400, 'provider is required'));
    if (req.body.provider !== 'noop') {
        if (typeof req.body.serverAddress !== 'string') return next(new HttpError(400, 'serverAddress is required'));
        if ('username' in req.body && typeof req.body.username !== 'string') return next(new HttpError(400, 'username is required'));
        if ('email' in req.body && typeof req.body.email !== 'string') return next(new HttpError(400, 'email is required'));
        if ('password' in req.body && typeof req.body.password !== 'string') return next(new HttpError(400, 'password is required'));
    }

    const [error] = await safe(docker.setRegistryConfig(req.body));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200));
}
