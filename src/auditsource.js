'use strict';

class AuditSource {
    constructor(username, userId, ip) {
        this.username = username || null; // this can be a real user or a module like cron/apptask/platform
        this.userId = userId || null;
        this.ip = ip || null;
    }

    static fromRequest(req) {
        const ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress || null;
        return new AuditSource(req.user?.username, req.user?.id, ip);
    }

    static fromDirectoryServerRequest(req) {
        const ip = req.connection.ldap.id; // also contains the port
        return new AuditSource('directoryserver', null, ip);
    }

    static fromOidcRequest(req) {
        const ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress || null;
        return new AuditSource('oidc', req.body?.username, ip);
    }
}

// these can be static variables but see https://stackoverflow.com/questions/60046847/eslint-does-not-allow-static-class-properties#comment122122927_60464446
AuditSource.APPTASK = new AuditSource('apptask');
AuditSource.BOOT = new AuditSource('boot');
AuditSource.CRON =  new AuditSource('cron');
AuditSource.EXTERNAL_LDAP = new AuditSource('externalldap');
AuditSource.HEALTH_MONITOR = new AuditSource('healthmonitor');
AuditSource.LDAP = new AuditSource('ldap');
AuditSource.MAIL = new AuditSource('mail');
AuditSource.PLATFORM = new AuditSource('platform');

exports = module.exports = AuditSource;
