'use strict';

// -------------------------------------------
//  This file just describes the interface
//
//  New backends can start from here
// -------------------------------------------

exports = module.exports = {
    getIPv4,
    getIPv6,
    testIPv4Config,
    testIPv6Config
};

const assert = require('assert'),
    BoxError = require('../boxerror.js');

async function getIPv4(config) {
    assert.strictEqual(typeof config, 'object');

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'getIPv4 is not implemented');
}

async function getIPv6(config) {
    assert.strictEqual(typeof config, 'object');

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'getIPv6 is not implemented');
}

async function testIPv4Config(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}

async function testIPv6Config(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}
