'use strict';

exports = module.exports = {
    getIPv4,
    getIPv6,
    testIPv4Config,
    testIPv6Config
};

const assert = require('assert');

async function getIPv4(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}

async function getIPv6(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}

async function testIPv4Config(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}

async function testIPv6Config(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}
