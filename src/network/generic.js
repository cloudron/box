'use strict';

exports = module.exports = {
    getIPv4,
    getIPv6,
    testIPv4Config,
    testIPv6Config
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    constants = require('../constants.js'),
    debug = require('debug')('box:network/generic'),
    safe = require('safetydance'),
    superagent = require('../superagent.js');

const gCache = { ipv4: {}, ipv6: {} }; // each has { timestamp, value, request }

async function getIP(type) {
    const url = `https://${type}.api.cloudron.io/api/v1/helper/public_ip`;

    gCache[type].value = null; // clear the obsolete value

    debug(`getIP: querying ${url} to get ${type}`);
    const [networkError, response] = await safe(superagent.get(url).timeout(30 * 1000).retry(2).ok(() => true));

    if (networkError || response.status !== 200) {
        debug(`getIP: Error getting IP. ${networkError.message}`);
        throw new BoxError(BoxError.EXTERNAL_ERROR, `Unable to detect ${type}. API server (${type}.api.cloudron.io) unreachable`);
    }

    if (!response.body && !response.body.ip) {
        debug('get: Unexpected answer. No "ip" found in response body.', response.body);
        throw new BoxError(BoxError.EXTERNAL_ERROR, `Unable to detect ${type}. No IP found in response`);
    }

    gCache[type].value = response.body.ip;
    gCache[type].timestamp = Date.now();
}

async function getIPv4(config) {
    assert.strictEqual(typeof config, 'object');

    if (constants.TEST) return '127.0.0.1';

    if (gCache.ipv4.value && (Date.now() - gCache.ipv4.timestamp <= 5 * 60 * 1000)) return gCache.ipv4.value;

    if (!gCache.request) gCache.request = getIP('ipv4');
    await gCache.request;
    gCache.request = null;
    return gCache.ipv4.value;
}

async function getIPv6(config) {
    assert.strictEqual(typeof config, 'object');

    if (constants.TEST) return '::1';

    if (gCache.ipv6.value && (Date.now() - gCache.ipv6.timestamp <= 5 * 60 * 1000)) return gCache.ipv6.value;

    if (!gCache.request) gCache.request = getIP('ipv6');
    await gCache.request;
    gCache.request = null;
    return gCache.ipv6.value;
}

async function testIPv4Config(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}

async function testIPv6Config(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}
