'use strict';

// IMPORTANT: These patterns are together because they spin tasks which acquire a lock
// If the patterns overlap all the time, then the task may not ever get a chance to run!
// If you change this change dashboard patterns in settings.html
const DEFAULT_CLEANUP_BACKUPS_PATTERN = '00 30 1,3,5,23 * * *',
    DEFAULT_AUTOUPDATE_PATTERN = '00 00 1,3,5,23 * * *';

exports = module.exports = {
    startJobs,

    stopJobs,

    handleBackupPolicyChanged,
    handleTimeZoneChanged,
    handleAutoupdatePatternChanged,
    handleDynamicDnsChanged,
    handleExternalLdapChanged,

    DEFAULT_AUTOUPDATE_PATTERN,
};

const appHealthMonitor = require('./apphealthmonitor.js'),
    apps = require('./apps.js'),
    assert = require('assert'),
    AuditSource = require('./auditsource.js'),
    backups = require('./backups.js'),
    cloudron = require('./cloudron.js'),
    constants = require('./constants.js'),
    { CronJob } = require('cron'),
    debug = require('debug')('box:cron'),
    domains = require('./domains.js'),
    dyndns = require('./dyndns.js'),
    externalLdap = require('./externalldap.js'),
    eventlog = require('./eventlog.js'),
    janitor = require('./janitor.js'),
    mail = require('./mail.js'),
    network = require('./network.js'),
    oidc = require('./oidc.js'),
    paths = require('./paths.js'),
    reverseProxy = require('./reverseproxy.js'),
    safe = require('safetydance'),
    scheduler = require('./scheduler.js'),
    system = require('./system.js'),
    updater = require('./updater.js'),
    updateChecker = require('./updatechecker.js'),
    _ = require('./underscore.js');

const gJobs = {
    autoUpdater: null,
    backup: null,
    updateChecker: null,
    systemChecks: null,
    mailStatusCheck: null,
    diskSpaceChecker: null,
    certificateRenew: null,
    cleanupBackups: null,
    cleanupEventlog: null,
    cleanupTokens: null,
    cleanupOidc: null,
    dockerVolumeCleaner: null,
    dynamicDns: null,
    schedulerSync: null,
    appHealthMonitor: null,
    diskUsage: null,
    externalLdapSyncer: null,
    checkDomainConfigs: null
};

// cron format
// Seconds: 0-59
// Minutes: 0-59
// Hours: 0-23
// Day of Month: 1-31
// Months: 0-11
// Day of Week: 0-6

function getCronSeed() {
    let hour = null;
    let minute = null;

    const seedData = safe.fs.readFileSync(paths.CRON_SEED_FILE, 'utf8') || '';
    const parts = seedData.split(':');
    if (parts.length === 2) {
        hour = parseInt(parts[0]) || null;
        minute = parseInt(parts[1]) || null;
    }

    if ((hour == null || hour < 0 || hour > 23) || (minute == null || minute < 0 || minute > 60)) {
        hour = Math.floor(24 * Math.random());
        minute = Math.floor(60 * Math.random());

        debug(`getCronSeed: writing new cron seed file with ${hour}:${minute} to ${paths.CRON_SEED_FILE}`);

        safe.fs.writeFileSync(paths.CRON_SEED_FILE, `${hour}:${minute}`);
    }

    return { hour, minute };
}

async function startJobs() {
    const { hour, minute } = getCronSeed();

    debug(`startJobs: starting cron jobs with hour ${hour} and minute ${minute}`);

    gJobs.systemChecks = CronJob.from({
        cronTime: `00 ${minute} 2 * * *`, // once a day. if you change this interval, change the notification messages with correct duration
        onTick: async () => await safe(system.runSystemChecks(), { debug }),
        start: true
    });

    gJobs.mailStatusCheck = CronJob.from({
        cronTime: `00 ${minute} 2 * * *`, // once a day. if you change this interval, change the notification messages with correct duration
        onTick: async () => await safe(mail.checkStatus(), { debug }),
        start: true
    });

    gJobs.diskUsage = CronJob.from({
        cronTime: `00 ${minute} 3 * * *`, // once a day
        onTick: async () => await safe(system.startUpdateDiskUsage(), { debug }),
        start: true
    });

    gJobs.diskSpaceChecker = CronJob.from({
        cronTime: '00 30 * * * *', // every 30 minutes. if you change this interval, change the notification messages with correct duration
        onTick: async () => await safe(system.checkDiskSpace(), { debug }),
        start: true
    });

    // this is run separately from the update itself so that the user can disable automatic updates but can still get a notification
    gJobs.updateCheckerJob = CronJob.from({
        cronTime: `00 ${minute} 1,5,9,13,17,21,23 * * *`,
        onTick: async () => await safe(updateChecker.checkForUpdates({ automatic: true }), { debug }),
        start: true
    });

    gJobs.cleanupTokens = CronJob.from({
        cronTime: '00 */30 * * * *', // every 30 minutes
        onTick: async () => await safe(janitor.cleanupTokens(), { debug }),
        start: true
    });

    gJobs.cleanupOidc = CronJob.from({
        cronTime: '00 10 * * * *', // every hour ten minutes past
        onTick: async () => await safe(oidc.cleanupExpired(), { debug }),
        start: true
    });

    gJobs.cleanupBackups = CronJob.from({
        cronTime: DEFAULT_CLEANUP_BACKUPS_PATTERN,
        onTick: async () => await safe(backups.startCleanupTask(AuditSource.CRON), { debug }),
        start: true
    });

    gJobs.cleanupEventlog = CronJob.from({
        cronTime: '00 */30 * * * *', // every 30 minutes
        onTick: async () => await safe(eventlog.cleanup({ creationTime: new Date(Date.now() - 90 * 60 * 24 * 60 * 1000) }), { debug }), // 90 days ago
        start: true
    });

    gJobs.dockerVolumeCleaner = CronJob.from({
        cronTime: '00 00 */12 * * *', // every 12 hours
        onTick: async () => await safe(janitor.cleanupDockerVolumes(), { debug }),
        start: true
    });

    gJobs.schedulerSync = CronJob.from({
        cronTime: constants.TEST ? '*/10 * * * * *' : '00 */1 * * * *', // every minute
        onTick: async () => await safe(scheduler.sync(), { debug }),
        start: true
    });

    // randomized per Cloudron based on hourlySeed
    gJobs.certificateRenew = CronJob.from({
        cronTime: `00 10 ${hour} * * *`,
        onTick: async () => await safe(reverseProxy.startRenewCerts({}, AuditSource.CRON), { debug }),
        start: true
    });


    gJobs.checkDomainConfigs = CronJob.from({
        cronTime: `00 ${minute} 5 * * *`, // once a day
        onTick: async () => await safe(domains.checkConfigs(AuditSource.CRON), { debug }),
        start: true
    });

    gJobs.appHealthMonitor = CronJob.from({
        cronTime: '*/10 * * * * *', // every 10 seconds
        onTick: async () => await safe(appHealthMonitor.run(10), { debug }), // 10 is the max run time
        start: true
    });

    await handleBackupPolicyChanged(await backups.getPolicy());
    await handleAutoupdatePatternChanged(await updater.getAutoupdatePattern());
    await handleDynamicDnsChanged(await network.getDynamicDns());
    await handleExternalLdapChanged(await externalLdap.getConfig());
}

async function handleBackupPolicyChanged(value) {
    assert.strictEqual(typeof value, 'object');

    const tz = await cloudron.getTimeZone();

    debug(`backupPolicyChanged: schedule ${value.schedule} (${tz})`);

    if (gJobs.backup) gJobs.backup.stop();
    gJobs.backup = null;

    gJobs.backup = CronJob.from({
        cronTime: value.schedule,
        onTick: async () => await safe(backups.startBackupTask(AuditSource.CRON), { debug }),
        start: true,
        timeZone: tz
    });
}

async function handleTimeZoneChanged(tz) {
    assert.strictEqual(typeof tz, 'string');

    debug('handleTimeZoneChanged: recreating all jobs');
    await stopJobs();
    await scheduler.deleteJobs(); // have to re-create with new tz
    await startJobs();
}

async function handleAutoupdatePatternChanged(pattern) {
    assert.strictEqual(typeof pattern, 'string');

    const tz = await cloudron.getTimeZone();

    debug(`autoupdatePatternChanged: pattern - ${pattern} (${tz})`);

    if (gJobs.autoUpdater) gJobs.autoUpdater.stop();
    gJobs.autoUpdater = null;

    if (pattern === constants.AUTOUPDATE_PATTERN_NEVER) return;

    gJobs.autoUpdater = CronJob.from({
        cronTime: pattern,
        onTick: async function() {
            const updateInfo = updateChecker.getUpdateInfo();
            // do box before app updates. for the off chance that the box logic fixes some app update logic issue
            if (updateInfo.box && !updateInfo.box.unstable) {
                debug('Starting box autoupdate to %j', updateInfo.box.version);
                const [error] = await safe(updater.updateToLatest({ skipBackup: false }, AuditSource.CRON));
                if (!error) return; // do not start app updates when a box update got scheduled
                debug(`Failed to start box autoupdate task: ${error.message}`);
                // fall through to update apps if box update never started (failed ubuntu or avx check)
            }

            const appUpdateInfo = _.omit(updateInfo, ['box']);
            if (Object.keys(appUpdateInfo).length > 0) {
                debug('Starting app autoupdate: %j', Object.keys(appUpdateInfo));
                const [error] = await safe(apps.autoupdateApps(appUpdateInfo, AuditSource.CRON));
                if (error) debug(`Failed to app autoupdate: ${error.message}`);
            } else {
                debug('No app auto updates available');
            }
        },

        start: true,
        timeZone: tz
    });
}

function handleDynamicDnsChanged(enabled) {
    assert.strictEqual(typeof enabled, 'boolean');

    debug('Dynamic DNS setting changed to %s', enabled);

    if (gJobs.dynamicDns) gJobs.dynamicDns.stop();
    gJobs.dynamicDns = null;

    if (!enabled) return;

    gJobs.dynamicDns = CronJob.from({
        // until we can be smarter about actual IP changes, lets ensure it every 10minutes
        cronTime: '00 */10 * * * *',
        onTick: async () => { await safe(dyndns.refreshDns(AuditSource.CRON), { debug }); },
        start: true
    });
}

async function handleExternalLdapChanged(config) {
    assert.strictEqual(typeof config, 'object');

    if (gJobs.externalLdapSyncer) gJobs.externalLdapSyncer.stop();
    gJobs.externalLdapSyncer = null;

    if (config.provider === 'noop') return;

    gJobs.externalLdapSyncer = CronJob.from({
        cronTime: '00 00 */4 * * *', // every 4 hours
        onTick: async () => await safe(externalLdap.startSyncer(AuditSource.CRON), { debug }),
        start: true
    });
}

async function stopJobs() {
    for (const job in gJobs) {
        if (!gJobs[job]) continue;
        gJobs[job].stop();
        gJobs[job] = null;
    }
}
