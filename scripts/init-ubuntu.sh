#!/bin/bash

# This script is run on the base ubuntu. Put things here which are managed by ubuntu
# This script is also run after ubuntu upgrade

set -euv -o pipefail

readonly SOURCE_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

readonly arg_infraversionpath="${SOURCE_DIR}/../src"

function die {
    echo $1
    exit 1
}

export DEBIAN_FRONTEND=noninteractive

readonly ubuntu_codename=$(lsb_release -cs)
readonly ubuntu_version=$(lsb_release -rs)

# hold grub since updating it breaks on some VPS providers. also, dist-upgrade will trigger it
apt-mark hold grub* >/dev/null
apt-get -o Dpkg::Options::="--force-confdef" update -y
apt-get -o Dpkg::Options::="--force-confdef" upgrade -y
apt-mark unhold grub* >/dev/null

vendor=$(cat /sys/devices/virtual/dmi/id/sys_vendor || true)
if [[ "${vendor}" == "netcup" && ! -L /etc/resolv.conf ]]; then
    echo "==> Fix netcup DNS setup"
    nameservers=$(sed -ne 's/nameserver \(.*\)/"\1"/p' /etc/resolv.conf | paste -sd "," -) # json array
    if lsattr -l /etc/resolv.conf 2>/dev/null | grep -q Immutable; then
        chattr -i /etc/resolv.conf # this code is also in cloudron-setup. that code can be removed much after 8.0
    fi
    ln -frs /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
    netplan set --origin-hint 50-cloud-init  "ethernets.eth0.nameservers.addresses=[${nameservers}]"
    netplan apply # generates /run/systemd/resolve/resolv.conf
    systemctl restart systemd-resolved
fi

echo "==> Installing required packages"

debconf-set-selections <<< 'mysql-server mysql-server/root_password password password'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password password'

# this enables automatic security upgrades (https://help.ubuntu.com/community/AutomaticSecurityUpdates)
case "${ubuntu_version}" in
16.04)
    gpg_package="gnupg"
    mysql_package="mysql-server-5.7"
    ntpd_package=""
    python_package="python2.7"
    nginx_package="" # we use custom package for TLS v1.3 support
    ;;
18.04)
    gpg_package="gpg"
    mysql_package="mysql-server-5.7"
    ntpd_package=""
    python_package="python2.7"
    nginx_package="" # we use custom package for TLS v1.3 support
    ;;
20.04)
    gpg_package="gpg"
    mysql_package="mysql-server-8.0"
    ntpd_package="systemd-timesyncd"
    python_package="python3.8"
    nginx_package="nginx-full"
    ;;
22.04)
    gpg_package="gpg"
    mysql_package="mysql-server-8.0"
    ntpd_package="systemd-timesyncd"
    python_package="python3.10"
    nginx_package="nginx-full"
    ;;
24.04)
    gpg_package="gpg"
    mysql_package="mysql-server-8.0"
    ntpd_package="systemd-timesyncd"
    python_package="python3.12"
    nginx_package="nginx-full"
    ;;
esac

apt-get -y install --no-install-recommends \
    acl \
    apparmor \
    build-essential \
    cifs-utils \
    cron \
    curl \
    debconf-utils \
    dmsetup \
    $gpg_package \
    ipset \
    iptables \
    lib${python_package} \
    linux-generic \
    logrotate \
    $mysql_package \
    nfs-common \
    $nginx_package \
    $ntpd_package \
    openssh-server \
    python3-magic \
    pwgen \
    sshfs \
    swaks \
    tzdata \
    unattended-upgrades \
    unbound \
    unbound-anchor \
    unzip \
    whois \
    xfsprogs

# on some providers like scaleway the sudo file is changed and we want to keep the old one
apt-get -o Dpkg::Options::="--force-confold" install -y --no-install-recommends sudo

# this ensures that unattended upgades are enabled, if it was disabled during ubuntu install time (see #346)
# debconf-set-selection of unattended-upgrades/enable_auto_updates + dpkg-reconfigure does not work
# logs of upgrades are at /var/log/apt/history.log and /var/log/unattended-upgrades/unattended-upgrades-dpkg.log
# apt-daily-upgrade.service (timer) runs the unattended-upgrades script depending on APT::Periodic::Unattended-Upgrade
echo "==> Enabling automatic upgrades"
cp /usr/share/unattended-upgrades/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades

echo "==> Ensuring python $python_package"
apt-get install -y --no-install-recommends $python_package   # Install python which is required for npm rebuild

# do not upgrade grub because it might prompt user and break this script
echo "==> Enable memory accounting"
apt-get -y --no-upgrade --no-install-recommends install grub2-common
sed -e 's/^GRUB_CMDLINE_LINUX="\(.*\)"$/GRUB_CMDLINE_LINUX="\1 cgroup_enable=memory swapaccount=1 panic_on_oops=1 panic=5"/' -i /etc/default/grub
update-grub

echo "==> Install collectd"
# without this, libnotify4 will install gnome-shell
apt-get install -y libnotify4 libcurl3-gnutls --no-install-recommends
# https://bugs.launchpad.net/ubuntu/+source/collectd/+bug/1872281
if [[ "${ubuntu_version}" == "22.04" ]]; then
    readonly launchpad="https://launchpad.net/ubuntu/+source/collectd/5.12.0-9/+build/23189375/+files"
    cd /tmp && wget -q "${launchpad}/collectd_5.12.0-9_amd64.deb" "${launchpad}/collectd-utils_5.12.0-9_amd64.deb" "${launchpad}/collectd-core_5.12.0-9_amd64.deb" "${launchpad}/libcollectdclient1_5.12.0-9_amd64.deb"
    cd /tmp && apt install -y --no-install-recommends ./libcollectdclient1_5.12.0-9_amd64.deb ./collectd-core_5.12.0-9_amd64.deb ./collectd_5.12.0-9_amd64.deb ./collectd-utils_5.12.0-9_amd64.deb && rm -f /tmp/collectd_*.deb
    echo -e "\nLD_PRELOAD=/usr/lib/python3.10/config-3.10-x86_64-linux-gnu/libpython3.10.so" >> /etc/default/collectd
else
    if ! apt-get install -y --no-install-recommends collectd collectd-utils; then
        # FQDNLookup is true in default debian config. The box code has a custom collectd.conf that fixes this
        echo "Failed to install collectd, continuing anyway. Presumably because of http://mailman.verplant.org/pipermail/collectd/2015-March/006491.html"
    fi

    if [[ "${ubuntu_version}" == "20.04" ]]; then
        echo -e "\nLD_PRELOAD=/usr/lib/python3.8/config-3.8-x86_64-linux-gnu/libpython3.8.so" >> /etc/default/collectd
    fi
fi
sed -e 's/^FQDNLookup true/FQDNLookup false/' -i /etc/collectd/collectd.conf

# some hosts like atlantic install ntp which conflicts with timedatectl. https://serverfault.com/questions/1024770/ubuntu-20-04-time-sync-problems-and-possibly-incorrect-status-information
echo "==> Configuring host"
sed -e 's/^#NTP=/NTP=0.ubuntu.pool.ntp.org 1.ubuntu.pool.ntp.org 2.ubuntu.pool.ntp.org 3.ubuntu.pool.ntp.org/' -i /etc/systemd/timesyncd.conf
if systemctl is-active ntp; then
    systemctl stop ntp
    apt purge -y ntp
fi
timedatectl set-ntp 1
# mysql follows the system timezone
timedatectl set-timezone UTC

echo "==> Adding sshd configuration warning"
sed -e '/Port 22/ i # NOTE: Read https://docs.cloudron.io/security/#securing-ssh-access before changing this' -i /etc/ssh/sshd_config

# https://bugs.launchpad.net/ubuntu/+source/base-files/+bug/1701068
echo "==> Disabling motd news"
if [[ -f "/etc/default/motd-news" ]]; then
    sed -i 's/^ENABLED=.*/ENABLED=0/' /etc/default/motd-news
fi

# If privacy extensions are not disabled on server, this breaks IPv6 detection
# https://bugs.launchpad.net/ubuntu/+source/procps/+bug/1068756
if [[ ! -f /etc/sysctl.d/99-cloudimg-ipv6.conf ]]; then
    echo "==> Disable temporary address (IPv6)"
    echo -e "# See https://bugs.launchpad.net/ubuntu/+source/procps/+bug/1068756\nnet.ipv6.conf.all.use_tempaddr = 0\nnet.ipv6.conf.default.use_tempaddr = 0\n\n" > /etc/sysctl.d/99-cloudimg-ipv6.conf
fi

# Disable exim4 (1blu.de)
systemctl stop exim4 || true
systemctl disable exim4 || true

# Disable bind for good measure (on online.net, kimsufi servers these are pre-installed)
systemctl stop bind9 || true
systemctl disable bind9 || true

# on ovh images dnsmasq seems to run by default
systemctl stop dnsmasq || true
systemctl disable dnsmasq || true

# on ssdnodes postfix seems to run by default
systemctl stop postfix || true
systemctl disable postfix || true

# on vultr, ufw is enabled by default. we have our own firewall
ufw disable || true

# nfs-common depends on rpcbind which is only needed for NFS v2/v3 . systemctl list-sockets | grep 111
systemctl disable rpcbind.socket rpcbind.service || true
systemctl stop rpcbind.socket rpcbind.service || true

# Ubuntu 22 has private home directories by default (https://discourse.ubuntu.com/t/private-home-directories-for-ubuntu-21-04-onwards/)
sed -e 's/^HOME_MODE\([[:space:]]\+\).*$/HOME_MODE\10755/' -i /etc/login.defs

# create the yellowtent user. system user has different numeric range, no age and won't show in login/gdm UI
# the nologin will also disable su/login. hardcoding uid helps in restoring
if ! id yellowtent 2>/dev/null; then
    useradd --system --uid 808 --comment "Cloudron Box" --create-home --shell /usr/sbin/nologin yellowtent
fi

# add support user (no password, sudo)
if ! id cloudron-support 2>/dev/null; then
    useradd --system --comment "Cloudron Support (support@cloudron.io)" --create-home --no-user-group --shell /bin/bash cloudron-support
fi

