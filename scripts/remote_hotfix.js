'use strict';

if (process.env.BOX_ENV !== 'cloudron') {
    console.error('!! This is only meant to be run with cloudron hotfix');
    process.exit(1);
}

const spawn = require('child_process').spawn,
    path = require('path');

const NEW_BOX_SOURCE_DIR = '/tmp/box-src-hotfix';

console.log('=> Running installer.sh');
const installer = spawn(path.join(NEW_BOX_SOURCE_DIR, 'scripts/installer.sh'), []);

installer.stdout.pipe(process.stdout);
installer.stderr.pipe(process.stderr);

installer.on('exit', function (code) {
    console.log('Finished with code', code);
    process.exit(code);
});
