// for the angular stuff

window.cloudronApiOrigin = import.meta.env.VITE_API_ORIGIN || '';

import '@fortawesome/fontawesome-free/css/all.min.css';
import '@fontsource/noto-sans';
import '@fontsource/dm-sans';
import '@fontsource/arimo';
import '@fontsource/inter';
import 'bootstrap-sass';
import Chart from 'chart.js/auto';

import * as moment from 'moment/min/moment-with-locales';

// attach to global for compatibility
window.moment = moment.default;
window.Chart = Chart;

// hack to disable old bootstrap transitions
setTimeout(() => window.$.support.transition = false, 3000);
