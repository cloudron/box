import { createApp } from 'vue';

import './style.css';

import '@fontsource/noto-sans';

import i18n from './i18n.js';
import Terminal from './components/Terminal.vue';

(async function init() {
  const app = createApp(Terminal);

  app.use(await i18n());

  app.mount('#app');
})();
