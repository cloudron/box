
const API_ORIGIN = import.meta.env.VITE_API_ORIGIN || window.location.origin;

// app.accessRestriction:
// null = ANY
// false = NOSSO
// { users: [], groups: [] } = RESTRICTED
const ACL_OPTIONS = Object.freeze({
  ANY: 'any',
  RESTRICTED: 'restricted',
  NOSSO: 'nosso',
});

// keep in sync with box/src/apps.js
const ISTATES = {
    PENDING_INSTALL: 'pending_install',
    PENDING_CLONE: 'pending_clone',
    PENDING_CONFIGURE: 'pending_configure',
    PENDING_UNINSTALL: 'pending_uninstall',
    PENDING_RESTORE: 'pending_restore',
    PENDING_IMPORT: 'pending_import',
    PENDING_UPDATE: 'pending_update',
    PENDING_BACKUP: 'pending_backup',
    PENDING_RECREATE_CONTAINER: 'pending_recreate_container', // env change or addon change
    PENDING_LOCATION_CHANGE: 'pending_location_change',
    PENDING_DATA_DIR_MIGRATION: 'pending_data_dir_migration',
    PENDING_RESIZE: 'pending_resize',
    PENDING_DEBUG: 'pending_debug',
    PENDING_START: 'pending_start',
    PENDING_STOP: 'pending_stop',
    PENDING_RESTART: 'pending_restart',
    ERROR: 'error',
    INSTALLED: 'installed'
};

const HSTATES = {
    HEALTHY: 'healthy',
    UNHEALTHY: 'unhealthy',
    ERROR: 'error',
    DEAD: 'dead'
};

const RSTATES ={
    RUNNING: 'running',
    STOPPED: 'stopped'
};

const ERROR = {
    ACCESS_DENIED: 'Access Denied',
    ALREADY_EXISTS: 'Already Exists',
    BAD_FIELD: 'Bad Field',
    COLLECTD_ERROR: 'Collectd Error',
    CONFLICT: 'Conflict',
    DATABASE_ERROR: 'Database Error',
    DNS_ERROR: 'DNS Error',
    DOCKER_ERROR: 'Docker Error',
    EXTERNAL_ERROR: 'External Error',
    FS_ERROR: 'FileSystem Error',
    INTERNAL_ERROR: 'Internal Error',
    LOGROTATE_ERROR: 'Logrotate Error',
    NETWORK_ERROR: 'Network Error',
    NOT_FOUND: 'Not found',
    REVERSEPROXY_ERROR: 'ReverseProxy Error',
    TASK_ERROR: 'Task Error',
    UNKNOWN_ERROR: 'Unknown Error' // only used for portin,
};

const ROLES = {
    OWNER: 'owner',
    ADMIN: 'admin',
    MAIL_MANAGER: 'mailmanager',
    USER_MANAGER: 'usermanager',
    USER: 'user'
};

// sync up with tasks.js
const TASK_TYPES = {
    TASK_APP: 'app',
    TASK_BACKUP: 'backup',
    TASK_UPDATE: 'update',
    TASK_CHECK_CERTS: 'checkCerts',
    TASK_PREPARE_DASHBOARD_LOCATION: 'prepareDashboardLocation',
    TASK_CLEAN_BACKUPS: 'cleanBackups',
    TASK_SYNC_EXTERNAL_LDAP: 'syncExternalLdap',
    TASK_CHANGE_MAIL_LOCATION: 'changeMailLocation',
    TASK_SYNC_DNS_RECORDS: 'syncDnsRecords',
    TASK_UPDATE_DISK_USAGE: 'updateDiskUsage',
};

const APP_TYPES = {
    APP: 'app', //default
    LINK: 'link',
    PROXIED: 'proxied'
};

const PROXY_APP_ID = 'io.cloudron.builtin.appproxy';

// sync up with tokens.js
const TOKEN_TYPES = {
    ID_WEBADMIN: 'cid-webadmin',        // dashboard
    ID_DEVELOPMENT: 'cid-development',  // dashboard development
    ID_CLI: 'cid-cli',                  // cloudron cli
    ID_SDK: 'cid-sdk',                  // created by user via dashboard
};

const ENDPOINTS_OVH = [
    { name: 'OVH Europe', value: 'ovh-eu' },
    { name: 'OVH US', value: 'ovh-us' },
    { name: 'OVH North-America', value: 'ovh-ca' },
    { name: 'SoYouStart Europe', value: 'soyoustart-eu' },
    { name: 'SoYouStart North-America', value: 'soyoustart-ca' },
    { name: 'Kimsufi Europe', value: 'kimsufi-eu' },
    { name: 'Kimsufi North-America', value: 'kimsufi-ca' },
];

const SECRET_PLACEHOLDER = String.fromCharCode(0x25CF).repeat(8);

// List is from http://docs.aws.amazon.com/general/latest/gr/rande.html#s3_region
const REGIONS_S3 = [
    { name: 'Africa (Cape Town)', value: 'af-south-1' },
    { name: 'Asia Pacific (Hong Kong)', value: 'ap-east-1' },
    { name: 'Asia Pacific (Hyderabad)', value: 'ap-south-2' },
    { name: 'Asia Pacific (Jakarta)', value: 'ap-southeast-3' },
    { name: 'Asia Pacific (Melbourne)', value: 'ap-southeast-4' },
    { name: 'Asia Pacific (Mumbai)', value: 'ap-south-1' },
    { name: 'Asia Pacific (Osaka)', value: 'ap-northeast-3' },
    { name: 'Asia Pacific (Seoul)', value: 'ap-northeast-2' },
    { name: 'Asia Pacific (Singapore)', value: 'ap-southeast-1' },
    { name: 'Asia Pacific (Sydney)', value: 'ap-southeast-2' },
    { name: 'Asia Pacific (Tokyo)', value: 'ap-northeast-1' },
    { name: 'Canada (Central)', value: 'ca-central-1' },
    { name: 'China (Beijing)', value: 'cn-north-1' },
    { name: 'China (Ningxia)', value: 'cn-northwest-1' },
    { name: 'Middle East (Bahrain)', value: 'me-south-1' },
    { name: 'Middle East (UAE)', value: 'me-central-1' },
    { name: 'EU (Frankfurt)', value: 'eu-central-1' },
    { name: 'EU (Ireland)', value: 'eu-west-1' },
    { name: 'EU (London)', value: 'eu-west-2' },
    { name: 'EU (Milan)', value: 'eu-south-1' },
    { name: 'EU (Paris)', value: 'eu-west-3' },
    { name: 'EU (Spain)', value: 'eu-south-2' },
    { name: 'EU (Stockholm)', value: 'eu-north-1' },
    { name: 'EU (Zurich)', value: 'eu-central-2' },
    { name: 'South America (São Paulo)', value: 'sa-east-1' },
    { name: 'US East (N. Virginia)', value: 'us-east-1' },
    { name: 'US East (Ohio)', value: 'us-east-2' },
    { name: 'US West (N. California)', value: 'us-west-1' },
    { name: 'US West (Oregon)', value: 'us-west-2' },
];

// https://wasabi.com/locations/
const REGIONS_WASABI = [
    { name: 'Amsterdam (EU Central 1)', value: 'https://s3.eu-central-1.wasabisys.com' },
    { name: 'Frankfurt (EU Central 2)', value: 'https://s3.eu-central-2.wasabisys.com' },
    { name: 'London (EU West 1)', value: 'https://s3.eu-west-1.wasabisys.com' },
    { name: 'Oregon (US West 1)', value: 'https://s3.us-west-1.wasabisys.com' },
    { name: 'Osaka (AP Northeast 2)', value: 'https://s3.ap-northeast-2.wasabisys.com' },
    { name: 'Paris (EU West 2)', value: 'https://s3.eu-west-2.wasabisys.com' },
    { name: 'Plano (US Central 1)', value: 'https://s3.us-central-1.wasabisys.com' },
    { name: 'Singapore (AP Southeast 1)', value: 'https://s3.ap-southeast-1.wasabisys.com' },
    { name: 'Sydney (AP Southeast 2)', value: 'https://s3.ap-southeast-2.wasabisys.com' },
    { name: 'Tokyo (AP Northeast 1)', value: 'https://s3.ap-northeast-1.wasabisys.com' },
    { name: 'Toronto (CA Central 1)', value: 'https://s3.ca-central-1.wasabisys.com' },
    { name: 'Virginia (US East 1)', value: 'https://s3.us-east-1.wasabisys.com' },
    { name: 'Virginia (US East 2)', value: 'https://s3.us-east-2.wasabisys.com' }
];

const REGIONS_HETZNER = [
    { name: 'Falkenstein (FSN1)', value: 'https://fsn1.your-objectstorage.com' },
    { name: 'Helsinki (HEL1)', value: 'https://hel1.your-objectstorage.com' },
    { name: 'Nuremberg (NBG1)', value: 'https://nbg1.your-objectstorage.com' }
];

// https://docs.digitalocean.com/products/platform/availability-matrix/
const REGIONS_DIGITALOCEAN = [
    { name: 'AMS3', value: 'https://ams3.digitaloceanspaces.com' },
    { name: 'FRA1', value: 'https://fra1.digitaloceanspaces.com' },
    { name: 'LON1', value: 'https://lon1.digitaloceanspaces.com' },
    { name: 'NYC3', value: 'https://nyc3.digitaloceanspaces.com' },
    { name: 'SFO2', value: 'https://sfo2.digitaloceanspaces.com' },
    { name: 'SFO3', value: 'https://sfo3.digitaloceanspaces.com' },
    { name: 'SGP1', value: 'https://sgp1.digitaloceanspaces.com' },
    { name: 'SYD1', value: 'https://syd1.digitaloceanspaces.com' }
];

// https://www.exoscale.com/datacenters/
const REGIONS_EXOSCALE = [
    { name: 'Vienna (AT-VIE-1)', value: 'https://sos-at-vie-1.exo.io' },
    { name: 'Vienna (AT-VIE-2)', value: 'https://sos-at-vie-2.exo.io' },
    { name: 'Sofia (BG-SOF-1)', value: 'https://sos-bg-sof-1.exo.io' },
    { name: 'Zurich (CH-DK-2)', value: 'https://sos-ch-dk-2.exo.io' },
    { name: 'Geneva (CH-GVA-2)', value: 'https://sos-ch-gva-2.exo.io' },
    { name: 'Frankfurt (DE-FRA-1)', value: 'https://sos-de-fra-1.exo.io' },
    { name: 'Munich (DE-MUC-1)', value: 'https://sos-de-muc-1.exo.io' },
];

// https://www.scaleway.com/docs/object-storage-feature/
const REGIONS_SCALEWAY = [
    { name: 'Paris (FR-PAR)', value: 'https://s3.fr-par.scw.cloud', region: 'fr-par' }, // default
    { name: 'Amsterdam (NL-AMS)', value: 'https://s3.nl-ams.scw.cloud', region: 'nl-ams' },
    { name: 'Warsaw (PL-WAW)', value: 'https://s3.pl-waw.scw.cloud', region: 'nl-ams' }
];

// https://www.linode.com/docs/products/storage/object-storage/guides/urls/
const REGIONS_LINODE = [
    { name: 'Amsterdam', value: 'https://nl-ams-1.linodeobjects.com', region: 'nl-ams-1' },
    { name: 'Atlanta', value: 'https://us-southeast-1.linodeobjects.com', region: 'us-southeast-1' },
    { name: 'Chennai', value: 'https://in-maa-1.linodeobjects.com', region: 'in-maa-1' },
    { name: 'Chicago', value: 'https://us-ord-1.linodeobjects.com', region: 'us-ord-1' },
    { name: 'Frankfurt', value: 'https://eu-central-1.linodeobjects.com', region: 'eu-central-1' },
    { name: 'Jakarta', value: 'https://id-cgk-1.linodeobjects.com', region: 'id-cgk-1' },
    { name: 'Los Angeles, CA (USA)', value: 'https://us-lax-1.linodeobjects.com', region: 'us-lax-1' },
    { name: 'Miami', value: 'https://us-mia-1.linodeobjects.com', region: 'us-mia-1' },
    { name: 'Milan', value: 'https://it-mil-1.linodeobjects.com', region: 'it-mil-1' },
    { name: 'Newark', value: 'https://us-east-1.linodeobjects.com', region: 'us-east-1' }, // default
    { name: 'Osaka', value: 'https://jp-osa-1.linodeobjects.com', region: 'jp-osa-1' },
    { name: 'Paris', value: 'https://fr-par-1.linodeobjects.com', region: 'fr-par-1' },
    { name: 'Sao Paulo', value: 'https://br-gru-1.linodeobjects.com', region: 'br-gru-1' },
    { name: 'Seattle', value: 'https://us-sea-1.linodeobjects.com', region: 'us-sea-1' },
    { name: 'Singapore', value: 'https://ap-south-1.linodeobjects.com', region: 'ap-south-1' },
    { name: 'Stockholm', value: 'https://se-sto-1.linodeobjects.com', region: 'se-sto-1' },
    { name: 'Washington', value: 'https://us-iad-1.linodeobjects.com', region: 'us-iad-1' },
];

// note: ovh also has a storage endpoint but that only supports path style access (https://docs.ovh.com/au/en/storage/object-storage/s3/location/)
const REGIONS_OVH = [
    { name: 'Beauharnois (BHS)', value: 'https://s3.bhs.io.cloud.ovh.net', region: 'bhs' }, // default
    { name: 'Frankfurt (DE)', value: 'https://s3.de.io.cloud.ovh.net', region: 'de' },
    { name: 'Gravelines (GRA)', value: 'https://s3.gra.io.cloud.ovh.net', region: 'gra' },
    { name: 'Roubaix (RBX)', value: 'https://s3.rbx.io.cloud.ovh.net', region: 'rbx' },
    { name: 'Strasbourg (SBG)', value: 'https://s3.sbg.io.cloud.ovh.net', region: 'sbg' },
    { name: 'London (UK)', value: 'https://s3.uk.io.cloud.ovh.net', region: 'uk' },
    { name: 'Sydney (SYD)', value: 'https://s3.syd.io.cloud.ovh.net', region: 'syd' },
    { name: 'Warsaw (WAW)', value: 'https://s3.waw.io.cloud.ovh.net', region: 'waw' },
];

// https://docs.ionos.com/cloud/managed-services/s3-object-storage/endpoints
const REGIONS_IONOS = [
    { name: 'Berlin (eu-central-3)', value: 'https://s3.eu-central-3.ionoscloud.com', region: 'de' }, // default. contract-owned
    { name: 'Frankfurt (DE)', value: 'https://s3.eu-central-1.ionoscloud.com', region: 'de' },
    { name: 'Berlin (eu-central-2)', value: 'https://s3-eu-central-2.ionoscloud.com', region: 'eu-central-2' },
    { name: 'Logrono (eu-south-2)', value: 'https://s3-eu-south-2.ionoscloud.com', region: 'eu-south-2' },
];

// this is not used anywhere because upcloud needs endpoint URL. we detect region from the URL (https://upcloud.com/data-centres)
const REGIONS_UPCLOUD = [
    { name: 'AU-SYD1 (Australia)', value: 'https://au-syd1.upcloudobjects.com', region: 'au-syd1' }, // default
    { name: 'DE-FRA1 (Germany)', value: 'https://de-fra1.upcloudobjects.com', region: 'de-fra1' },
    { name: 'ES-MAD1 (Spain)', value: 'https://es-mad1.upcloudobjects.com', region: 'es-mad1' },
    { name: 'FI-HEL2 (Finland)', value: 'https://fi-hel2.upcloudobjects.com', region: 'fi-hel2' },
    { name: 'NL-AMS1 (Netherlands)', value: 'https://nl-ams1.upcloudobjects.com', region: 'nl-ams1' },
    { name: 'PL-WAW1 (Poland)', value: 'https://pl-waw1.upcloudobjects.com', region: 'pl-waw1' },
    { name: 'SG-SIN1 (Singapore)', value: 'https://sg-sin1.upcloudobjects.com', region: 'sg-sin1' },
    { name: 'UK-LON1 (United Kingdom)', value: 'https://uk-lon1.upcloudobjects.com', region: 'uk-lon1' },
    { name: 'US-CHI1 (USA)', value: 'https://us-chi1.upcloudobjects.com', region: 'us-chi1' },
    { name: 'US-NYC1 (USA)', value: 'https://us-nyc1.upcloudobjects.com', region: 'us-nyc1' },
    { name: 'US-SJO1 (USA)', value: 'https://us-sjo1.upcloudobjects.com', region: 'us-sjo1' },
];

// region is ignored in vultr (https://www.vultr.com/docs/vultr-object-storage/)
const REGIONS_VULTR = [
    { name: 'Amsterdam', value: 'https://ams1.vultrobjects.com', region: 'eu-central-1' },
    { name: 'Bangalore', value: 'https://blr1.vultrobjects.com', region: 'ap-south-1' },
    { name: 'New Jersey', value: 'https://ewr1.vultrobjects.com', region: 'us-east-1' },
    { name: 'Silicon Valley', value: 'https://sjc1.vultrobjects.com', region: 'us-west-1' },
    { name: 'Singapore', value: 'https://sgp1.vultrobjects.com', region: 'ap-southeast-1' },
];

// https://docs.contabo.com/docs/products/Object-Storage/s3-connection-settings
const REGIONS_CONTABO = [
    { name: 'European Union (Germany)', value: 'https://eu2.contabostorage.com' , region: 'us-east-1' },
    { name: 'Singapore', value: 'https://sin1.contabostorage.com' , region: 'us-east-1' },
    { name: 'United States', value: 'https://usc1.contabostorage.com' , region: 'us-east-1' }
];

const STORAGE_PROVIDERS = [
    { name: 'Amazon S3', value: 's3' },
    { name: 'Backblaze B2 (S3 API)', value: 'backblaze-b2' },
    { name: 'CIFS Mount', value: 'cifs' },
    { name: 'Cloudflare R2', value: 'cloudflare-r2' },
    { name: 'Contabo Object Storage', value: 'contabo-objectstorage' },
    { name: 'DigitalOcean Spaces', value: 'digitalocean-spaces' },
    { name: 'External/Local Disk (EXT4 or XFS)', value: 'disk' },
    { name: 'EXT4 Disk', value: 'ext4' },
    { name: 'Exoscale SOS', value: 'exoscale-sos' },
    { name: 'Filesystem', value: 'filesystem' },
    { name: 'Filesystem (Mountpoint)', value: 'mountpoint' }, // legacy
    { name: 'Google Cloud Storage', value: 'gcs' },
    { name: 'Hetzner Object Storage', value: 'hetzner-objectstorage' },
    { name: 'IDrive e2', value: 'idrive-e2' },
    { name: 'IONOS (Profitbricks)', value: 'ionos-objectstorage' },
    { name: 'Linode Object Storage', value: 'linode-objectstorage' },
    { name: 'Minio', value: 'minio' },
    { name: 'NFS Mount', value: 'nfs' },
    { name: 'OVH Object Storage', value: 'ovh-objectstorage' },
    { name: 'S3 API Compatible (v4)', value: 's3-v4-compat' },
    { name: 'Scaleway Object Storage', value: 'scaleway-objectstorage' },
    { name: 'SSHFS Mount', value: 'sshfs' },
    { name: 'UpCloud Object Storage', value: 'upcloud-objectstorage' },
    { name: 'Vultr Object Storage', value: 'vultr-objectstorage' },
    { name: 'Wasabi', value: 'wasabi' },
    { name: 'XFS Disk', value: 'xfs' },
];

const BACKUP_FORMATS = [
    { name: 'Tarball (zipped)', value: 'tgz' },
    { name: 'rsync', value: 'rsync' }
];

// named exports
export {
    API_ORIGIN,
    ACL_OPTIONS,
    APP_TYPES,
    ERROR,
    HSTATES,
    ISTATES,
    RSTATES,
    ROLES,
    TASK_TYPES,
    PROXY_APP_ID,
    TOKEN_TYPES,
    ENDPOINTS_OVH,
    SECRET_PLACEHOLDER,
    STORAGE_PROVIDERS,
    BACKUP_FORMATS,
    REGIONS_CONTABO,
    REGIONS_VULTR,
    REGIONS_UPCLOUD,
    REGIONS_IONOS,
    REGIONS_OVH,
    REGIONS_LINODE,
    REGIONS_SCALEWAY,
    REGIONS_EXOSCALE,
    REGIONS_DIGITALOCEAN,
    REGIONS_HETZNER,
    REGIONS_WASABI,
    REGIONS_S3,
};

// default export
export default {
    API_ORIGIN,
    ACL_OPTIONS,
    APP_TYPES,
    ERROR,
    HSTATES,
    ISTATES,
    RSTATES,
    ROLES,
    TASK_TYPES,
    PROXY_APP_ID,
    TOKEN_TYPES,
    ENDPOINTS_OVH,
    SECRET_PLACEHOLDER,
    STORAGE_PROVIDERS,
    BACKUP_FORMATS,
    REGIONS_CONTABO,
    REGIONS_VULTR,
    REGIONS_UPCLOUD,
    REGIONS_IONOS,
    REGIONS_OVH,
    REGIONS_LINODE,
    REGIONS_SCALEWAY,
    REGIONS_EXOSCALE,
    REGIONS_DIGITALOCEAN,
    REGIONS_HETZNER,
    REGIONS_WASABI,
    REGIONS_S3,
};
