import { createApp } from 'vue';

import i18n from './i18n.js';
import PasswordReset from './components/PasswordReset.vue';

(async function init() {
  const app = createApp(PasswordReset);

  app.use(await i18n());

  app.mount('#app');
})();
