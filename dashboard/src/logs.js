import { createApp } from 'vue';

import './style.css';

import '@fontsource/noto-sans';

import i18n from './i18n.js';
import LogsViewer from './components/LogsViewer.vue';

(async function init() {
  const app = createApp(LogsViewer);

  app.use(await i18n());

  app.mount('#app');
})();
