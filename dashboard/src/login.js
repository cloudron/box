import { createApp } from 'vue';

import i18n from './i18n.js';
import Login from './components/Login.vue';

(async function init() {
  const app = createApp(Login);

  app.use(await i18n());

  app.mount('#app');
})();
