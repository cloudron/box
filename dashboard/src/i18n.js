import { createI18n } from 'vue-i18n';
import { fetcher } from 'pankow';
import { API_ORIGIN } from './constants.js';

const translations = {};

const i18n = createI18n({
  locale: 'en', // set locale
  fallbackLocale: 'en', // set fallback locale
  messages: translations,
  // will replace our double {{}} to vue-i18n single brackets
  messageResolver: function (keys, key) {
    const message = key.split('.').reduce((o, k) => o && o[k] || null, keys);

    // if not found return null to fallback to resolving for english
    if (message === null) return null;

    return message.replaceAll('{{', '{').replaceAll('}}', '}');
  }
});

// https://vue-i18n.intlify.dev/guide/advanced/lazy.html
async function loadLanguage(lang) {
  try {
    const result = await fetcher.get(`${API_ORIGIN}/translation/${lang}.json`);
    translations[lang] = result.body;
  } catch (e) {
    console.error(`Failed to load language file for ${lang}`, e);
  }
}

async function main() {
  // load at least fallback english
  await loadLanguage('en');

  let locale = window.localStorage.NG_TRANSLATE_LANG_KEY;
  if (!locale) {
    try {
      const res = await fetcher.get(`${API_ORIGIN}/api/v1/auth/branding`);
      locale = res.body.language;
    } catch (error) {
      console.error('Failed to get branding info for translation.', error);
    }
  }

  if (locale && locale !== 'en') {
    await loadLanguage(locale);

    if (i18n.mode === 'legacy') {
      i18n.global.locale = locale;
    } else {
      i18n.global.locale.value = locale;
    }
  }

  return i18n;
}

export default main;
