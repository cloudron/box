
import { fetcher } from 'pankow';
import { API_ORIGIN } from '../constants.js';

function create() {
  const accessToken = localStorage.token;

  return {
    async list() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/domains`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body.domains];
    },
    async add(domain, zoneName, provider, config, fallbackCertificate, tlsConfig) {
      const data = { domain, provider, config, tlsConfig };
      if (zoneName) data.zoneName = zoneName;
      if (fallbackCertificate) data.fallbackCertificate = fallbackCertificate;

      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/domains`, data, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 201) return [error || result];
      return [null];
    },
    async update(domain, zoneName, provider, config, fallbackCertificate, tlsConfig) {
      const data = { provider, config, tlsConfig };
      if (zoneName) data.zoneName = zoneName;
      if (fallbackCertificate) data.fallbackCertificate = fallbackCertificate;

      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/domains/${domain}/config`, data, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 204) return [error || result];

      // TODO is this still needed on update? a syncdns should also do this
      // this is done so that an out-of-sync dkim key can be synced
      // [error] = await domainsModel.setDnsRecords({ domain: domain, type: 'mail' });
      // if (error) console.error(error); // not fatal for now

      return [null];
    },
    async remove(domain) {
      let error, result;
      try {
        result = await fetcher.del(`${API_ORIGIN}/api/v1/domains/${domain}`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 204) return [error || result];
      return [null];
    },
    async renewCerts(options) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/reverseproxy/renew_certs`, { rebuild: !!options.rebuild }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 202) return [error || result];
      return [null, result.body.taskId];
    },
    async setDnsRecords(options) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/domains/sync_dns`, options, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 201) return [error || result];
      return [null, result.body.taskId];
    },
    async setWellKnown(domain, wellKnown) {
      // see WellKnownDialog.vue for the slightly complex wellKnown object
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/domains/${domain}/wellknown`, { wellKnown }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 204) return [error || result];
      return [null];
    },
    async checkRecords(domain, subdomain) {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/domains/${domain}/dns_check`, { subdomain, access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body];
    },
  };
}

export default {
  create,
};
