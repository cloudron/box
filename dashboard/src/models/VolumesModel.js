
import { fetcher } from 'pankow';
import { API_ORIGIN } from '../constants.js';

function create() {
  const accessToken = localStorage.token;

  return {
    async list() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/volumes`, { access_token: accessToken });
      } catch (e) {
          return [e];
      }

      if (result.status !== 200) return [result];

      return [null, result.body.volumes];
    },
    async getStatus(id) {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/volumes/${id}/status`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) {
        console.error('Failed to get volume status.', error, result);
        return {};
      }

      return result.body;
    },
    async remount(id) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/volumes/${id}/remount`, {}, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 202) console.error('Failed to remount volume.', error, result);
    },
    async add(name, mountType, mountOptions) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/volumes`, { name, mountType, mountOptions }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 201) throw error || result;
    },
    async update(id, mountOptions) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/volumes/${id}`, { mountOptions }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 204) throw error || result;
    },
    async remove(id) {
      let error, result;
      try {
        result = await fetcher.del(`${API_ORIGIN}/api/v1/volumes/${id}`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 204) throw error || result;
    },
    async getBlockDevices() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/system/block_devices`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) {
        console.error('Failed to get block devices.', error, result);
        return {};
      }

      return result.body.devices;
    },
  };
}

export default {
  create
};
