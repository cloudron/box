
import { fetcher } from 'pankow';
import { API_ORIGIN } from '../constants.js';

function create() {
  const accessToken = localStorage.token;

  return {
    async memory() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/system/memory`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body];
    },
    async cpus() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/system/cpus`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body.cpus];
    },
    async blockDevices() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/system/block_devices`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body.devices];
    },
    async info() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/system/info`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body.info];
    },
    async reboot() {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/system/reboot`, {}, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 202) return [error || result];
      return [null];
    },
    async diskUsage() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/system/disk_usage`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body.usage];
    },
    async rescanDiskUsage() {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/system/disk_usage`, {}, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 201) return [error || result];
      return [null, result.body.taskId];
    },
    async graphs(fromMinutes) {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/system/graphs`, { fromMinutes, access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body];
    },
  };
}

export default {
  create,
};
