
import { fetcher } from 'pankow';
import { API_ORIGIN } from '../constants.js';

function create() {
  const accessToken = localStorage.token;

  return {
    async list() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/appstore/apps`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) {
        console.error('Failed to list apps.', error || result.status);
        return [];
      }

      return result.body.apps;
    },
    async get(id, version = '') {
      let url = `${API_ORIGIN}/api/v1/appstore/apps/${id}`;
      if (version && version !== 'latest') url += `/versions/${version}`;

      let error, result;
      try {
        result = await fetcher.get(url, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) {
        console.error('Failed to get app.', error || result.status);
        return null;
      }

      return result.body;
    },
    async getSubscription() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/appstore/subscription`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body];
    },
  };
}

export default {
  create,
};
