
import { fetcher } from 'pankow';
import { API_ORIGIN } from '../constants.js';

function create() {
  const accessToken = localStorage.token;

  return {
    async getIpv4Config() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/network/ipv4_config`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body];
    },
    async setIpv4Config(provider, ip, ifname) {
      const data = { provider };

      if (provider === 'fixed') data.ip = ip;
      else if (provider === 'network-interface') data.ifname = ifname;

      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/network/ipv4_config`, data, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null];
    },
    async getIpv4() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/network/ipv4`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body.ip];
    },
    async getIpv6Config() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/network/ipv6_config`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body];
    },
    async setIpv6Config(provider, ip, ifname) {
      const data = { provider };

      if (provider === 'fixed') data.ip = ip;
      else if (provider === 'network-interface') data.ifname = ifname;

      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/network/ipv6_config`, data, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null];
    },
    async getIpv6() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/network/ipv6`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body.ip];
    },
    async getDynDnsConfig() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/network/dynamic_dns`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body];
    },
    async setDynDnsConfig(enabled) {
      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/network/dynamic_dns`, { enabled }, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null];
    },
    async getBlocklist() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/network/blocklist`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body.blocklist];
    },
    async setBlocklist(blocklist) {
      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/network/blocklist`, { blocklist }, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null];
    },
    async getTrustedIps() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/reverseproxy/trusted_ips`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body.trustedIps];
    },
    async setTrustedIps(trustedIps) {
      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/reverseproxy/trusted_ips`, { trustedIps }, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null];
    },
  };
}

export default {
  create,
};
