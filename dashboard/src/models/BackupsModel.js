
import { fetcher } from 'pankow';
import { API_ORIGIN } from '../constants.js';

function create() {
  const accessToken = localStorage.token;

  return {
    async list() {
      const page = 1;
      const per_page = 1000;

      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/backups`, { page, per_page, access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body.backups];
    },
    async create() {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/backups/create`, {}, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 202) return [error || result];
      return [null, result.body.taskId];
    },
    async edit(id, label, preserveSecs) {
      // if preserveSecs === -1 we will keep it
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/backups/${id}`, { label, preserveSecs }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null];
    },
    async getConfig() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/backups/config`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];

      // ensure we have objects
      if (!result.body.mountOptions) result.body.mountOptions = {};
      if (!result.body.limits) result.body.limits = {};

      return [null, result.body];
    },
    async setConfig(config, limits) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/backups/config/storage`, config, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];

      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/backups/config/limits`, limits, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null];
    },
    async getPolicy() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/backups/policy`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body.policy];
    },
    async setPolicy(policy) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/backups/policy`, policy, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null];
    },
    async cleanup() {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/backups/cleanup`, {}, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 202) return [error || result];
      return [null, result.body.taskId];
    },
  };
}

export default {
  create,
};
