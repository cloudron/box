
import { fetcher } from 'pankow';
import { API_ORIGIN } from '../constants.js';

function create() {
  const accessToken = localStorage.token;

  return {
    async list(domain, search = '') {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mail/${domain}/mailboxes`, { page: 1, per_page: 1000, access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body.mailboxes];
    },
  };
}

export default {
  create,
};
