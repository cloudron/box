
import { fetcher } from 'pankow';
import { API_ORIGIN } from '../constants.js';

function create() {
  const accessToken = localStorage.token;

  return {
    async config(domain) {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mail/${domain}`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body];
    },
    async status(domain) {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mail/${domain}/status`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body];
    },
    async mailboxCount(domain) {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mail/${domain}/mailbox_count`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body.count];
    },
    async usage(domain) {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mailserver/usage`, { domain, access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body.usage];
    },
    async location() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mailserver/location`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body];
    },
    async setLocation(subdomain, domain) {
      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/mailserver/location`, { subdomain, domain }, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 202) return [result];
      return [null, result.body.taskId];
    },
    async maxEmailSize() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mailserver/max_email_size`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body.size];
    },
    async setMaxEmailSize(size) {
      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/mailserver/max_email_size`, { size }, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null];
    },
    async mailboxSharing() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mailserver/mailbox_sharing`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body.enabled];
    },
    async setMailboxSharing(enable) {
      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/mailserver/mailbox_sharing`, { enable }, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null];
    },
    async virtualAllMail() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mailserver/virtual_all_mail`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body.enabled];
    },
    async setVirtualAllMail(enable) {
      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/mailserver/virtual_all_mail`, { enable }, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null];
    },
    async dnsblConfig() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mailserver/dnsbl_config`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body.zones];
    },
    async setDnsblConfig(zones) {
      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/mailserver/dnsbl_config`, { zones }, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null];
    },
    async ftsConfig() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mailserver/fts_config`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body];
    },
    async setFtsConfig(enable) {
      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/mailserver/fts_config`, { enable }, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null];
    },
    async spamAcl() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mailserver/spam_acl`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body];
    },
    async setSpamAcl(allowlist, blocklist) {
      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/mailserver/spam_acl`, { allowlist, blocklist }, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null];
    },
    async spamCustomConfig() {
      let result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/mailserver/spam_custom_config`, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null, result.body.config];
    },
    async setSpamCustomConfig(config) {
      let result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/mailserver/spam_custom_config`, { config }, { access_token: accessToken });
      } catch (e) {
        return [e];
      }

      if (result.status !== 200) return [result];
      return [null];
    },
  };
}

export default {
  create,
};
