
import { fetcher } from 'pankow';
import { API_ORIGIN } from '../constants.js';

function create() {
  const accessToken = localStorage.token;

  return {
    async getFooter() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/branding/footer`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body.footer];
    },
    async setFooter(footer) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/branding/footer`, { footer }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null];
    },
    async setName(name) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/branding/cloudron_name`, { name }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null];
    },
    async setBackground(image) {
      // Blob type if object
      const fd = new FormData();
      if (image) fd.append('background', image);

      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/branding/cloudron_background`, fd, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 202) return [error || result];
      return [null];
    },
    async setAvatar(image) {
      // Blob type if object
      const fd = new FormData();
      if (image) fd.append('avatar', image);

      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/branding/cloudron_avatar`, fd, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null];
    },
  };
}

export default {
  create,
};
