
import { fetcher } from 'pankow';
import { sleep } from 'pankow/utils';
import { API_ORIGIN } from '../constants.js';

function create() {
  const accessToken = localStorage.token;

  return {
    async wait(id) {
      while(true) {
        let result;
        try {
          result = await fetcher.get(`${API_ORIGIN}/api/v1/tasks/${id}`, { access_token: accessToken });
        } catch (e) {
          return [e];
        }

        if (!result.body.active) return [result.body.error, result.body.result];

        await sleep(2000);
      }
    },
    async getLatestByType(type) {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/tasks`, { type, page: 1, per_page: 1, access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body.tasks.length ? result.body.tasks[0] : null];
    },
    async getByType(type) {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/tasks`, { type, access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body.tasks];
    },
    async get(id) {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/tasks/${id}`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body];
    },
    async stop(id) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/tasks/${id}/stop`, {}, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 204) return [error || result];
      return [null];
    },
  };
}

export default {
  create,
};
