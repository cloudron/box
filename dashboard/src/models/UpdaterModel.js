
import { fetcher } from 'pankow';
import { API_ORIGIN } from '../constants.js';

function create() {
  const accessToken = localStorage.token;

  return {
    async info() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/updater/updates`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body.update];
    },
    async getAutoupdatePattern() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/updater/autoupdate_pattern`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body];
    },
    async setAutoupdatePattern(pattern) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/updater/autoupdate_pattern`, { pattern }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null];
    },
    async check() {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/updater/check_for_updates`, {}, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null];
    },
  };
}

export default {
  create,
};
