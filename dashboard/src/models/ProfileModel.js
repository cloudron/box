
import { ROLES, API_ORIGIN } from '../constants.js';
import { fetcher } from 'pankow';

function create() {
  const accessToken = localStorage.token;

  return {
    name: 'ProfileModel',
    async logout() {
      // destroy oidc session in the spirit of true SSO
      await fetcher.del(`${API_ORIGIN}/api/v1/oidc/sessions`, { access_token: accessToken });

      localStorage.removeItem('token');

      window.location.href = '/';
    },
    async get() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/profile`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) {
        console.error('Failed to get profile.', error || result.status);
        return [];
      }

      if (error || result.status !== 200) return [error || result];

      result.body.isAtLeastAdmin = [ ROLES.OWNER, ROLES.ADMIN ].indexOf(result.body.role) !== -1;
      result.body.isAtLeastOwner = [ ROLES.OWNER ].indexOf(result.body.role) !== -1;

      return [null, result.body];
    },
    async setPassword(password, newPassword) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/profile/password`, { password, newPassword }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error) return error;
      if (result.status !== 204) return result;

      return null;
    },
    async setDisplayName(displayName) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/profile/display_name`, { displayName }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error) return error;
      if (result.status !== 204) return result;

      return null;
    },
    async setEmail(email, password) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/profile/email`, { email, password }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error) return error;
      if (result.status !== 204) return result;

      return null;
    },
    async setFallbackEmail(fallbackEmail, password) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/profile/fallback_email`, { fallbackEmail, password }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error) return error;
      if (result.status !== 204) return result;

      return null;
    },
    async setLanguage(language) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/profile/language`, { language }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error) return error;
      if (result.status !== 204) return result;

      return null;
    },
    async setAvatar(file) {
      const fd = new FormData();
      fd.append('avatar', file);

      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/profile/avatar`, fd, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error) return error;
      if (result.status !== 202) return result;

      return null;
    },
    async sendPasswordReset(identifier) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/auth/password_reset_request`, { identifier }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error) return error;
      if (result.status !== 202) return result;

      return null;
    },
    async setTwoFASecret() {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/profile/twofactorauthentication_secret`, {}, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null,  result.body];
    },
    async enableTwoFA(totpToken) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/profile/twofactorauthentication_enable`, { totpToken }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 204) return [error || result];
      return [null];
    },
    async disableTwoFA(password) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/profile/twofactorauthentication_disable`, { password }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 204) return [error || result];
      return [null];
    },
  };
}

export default {
  create,
};
