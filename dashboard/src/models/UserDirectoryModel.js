
import { fetcher } from 'pankow';
import { API_ORIGIN } from '../constants.js';

function create() {
  const accessToken = localStorage.token;

  return {
    async getGlobalProfileConfig() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/user_directory/profile_config`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body];
    },
    async setGlobalProfileConfig(config) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/user_directory/profile_config`, config, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null];
    },
    async getExternalLdapConfig() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/external_ldap/config`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body];
    },
    async setExternalLdapConfig(config) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/external_ldap/config`, config, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null];
    },
    async startExternalLdapSync() {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/external_ldap/sync`, {}, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 202) return [error || result];
      return [null, result.body.taskId];
    },
    async getExposedLdapConfig() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/directory_server/config`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body];
    },
    async setExposedLdapConfig(config) {
      const data = {
        enabled: config.enabled,
        allowlist: config.allowlist,
        secret: config.secret,
      };

      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/directory_server/config`, data, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 202) return [error || result];
      return [null];
    },
    async getOpenIdClients() {
      let error, result;
      try {
        result = await fetcher.get(`${API_ORIGIN}/api/v1/oidc/clients`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 200) return [error || result];
      return [null, result.body.clients];
    },
    async addOpenIdClient(name, loginRedirectUri, tokenSignatureAlgorithm) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/oidc/clients`, { name, loginRedirectUri, tokenSignatureAlgorithm }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 201) return [error || result];
      return [null];
    },
    async updateOpenIdClient(id, name, loginRedirectUri, tokenSignatureAlgorithm) {
      let error, result;
      try {
        result = await fetcher.post(`${API_ORIGIN}/api/v1/oidc/clients/${id}`, { name, loginRedirectUri, tokenSignatureAlgorithm }, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 201) return [error || result];
      return [null];
    },
    async removeOpenIdClient(id) {
      let error, result;
      try {
        result = await fetcher.del(`${API_ORIGIN}/api/v1/oidc/clients/${id}`, { access_token: accessToken });
      } catch (e) {
        error = e;
      }

      if (error || result.status !== 204) return [error || result];
      return [null];
    },
  };
}

export default {
  create,
};
