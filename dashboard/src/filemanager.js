import { createApp } from 'vue';

import './style.css';

import '@fontsource/noto-sans';

import { createRouter, createWebHashHistory } from 'vue-router';

import i18n from './i18n.js';
import FileManager from './components/FileManager.vue';
import FolderView from './components/FolderView.vue';
import FileViewer from './components/FileViewer.vue';

const routes = [
  { path: '/', redirect: '/home' },
  { path: '/home/:type?/:resourceId?/:cwd*', component: FolderView },
  { path: '/viewer/:type/:resourceId/:filePath*', component: FileViewer }
];

const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
  history: createWebHashHistory(),
  routes,
});

(async function init() {
  const app = createApp(FileManager);

  app.use(await i18n());
  app.use(router);

  app.mount('#app');
})();
