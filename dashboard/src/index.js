import { createApp } from 'vue';

import '@fontsource/noto-sans';

import { tooltip, fallbackImage } from 'pankow';

import i18n from './i18n.js';
import Index from './Index.vue';

import './style.css';

(async function init() {
  const app = createApp(Index);

  app.use(await i18n());

  app.directive('tooltip', tooltip);
  app.directive('fallback-image', fallbackImage);

  app.mount('#app');
})();
