import js from '@eslint/js';
import pluginVue from 'eslint-plugin-vue';

export default [
    js.configs.recommended,
    ...pluginVue.configs['flat/essential'],
    {
        rules: {
            "semi": "error",
            "prefer-const": "error",
            "vue/no-reserved-component-names": "off",
            "vue/multi-word-component-names": "off",
        }
    }
];
