#!/bin/bash

set -eu

echo "=> Set API origin"
export VITE_API_ORIGIN="${DASHBOARD_DEVELOPMENT_ORIGIN}"

# only really used for prod builds to bust cache
export VITE_CACHE_ID="develop"

echo "=> Run vite locally"
npm run dev
