import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';
import { NodePackageImporter } from 'sass';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue()
  ],
  server: {
    fs: {
      // Allow serving files from one level up to the project root for monaco editor assets
      allow: [ '../../../' ]
    },
  },
  // https://vitejs.dev/guide/build.html#multi-page-app
  build: {
    rollupOptions: {
      input: {
        activation: resolve('activation.html'),
        appstatus: resolve('appstatus.html'),
        authcallback: resolve('authcallback.html'),
        filemanager: resolve('filemanager.html'),
        index: resolve('index.html'),
        login: resolve('login.html'),
        oidc_error: resolve('oidc_error.html'),
        oidc_interaction_confirm: resolve('oidc_interaction_confirm.html'),
        oidc_interaction_abort: resolve('oidc_interaction_abort.html'),
        proxyauth_login: resolve('proxyauth_login.html'),
        logs: resolve('logs.html'),
        notfound: resolve('notfound.html'),
        passwordreset: resolve('passwordreset.html'),
        restore: resolve('restore.html'),
        setup: resolve('setup.html'),
        setupaccount: resolve('setupaccount.html'),
        terminal: resolve('terminal.html'),
      },
    },
  },
  // only exists since we also want to use sass to compile theme.css for oidc login views
  css: {
    preprocessorOptions: {
      scss: {
        api: 'modern-compiler',
        importers: [ new NodePackageImporter() ],
        quietDeps: true,
        silenceDeprecations: [ 'import', 'global-builtin' ],
      },
    },
  },
});
