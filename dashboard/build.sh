#!/bin/bash

set -eu

echo "=> Create timezones.js"
./scripts/createTimezones.cjs ./public/js/timezones.js

echo "=> Build theme.css for oidc views"
./node_modules/.bin/sass --quiet --pkg-importer=node  ./src/theme.scss ./public/theme.css

export VITE_CACHE_ID=$(date +%s)

echo "=> Build the dashboard apps"
./node_modules/.bin/vite build
