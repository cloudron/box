'use strict';

/* global $, angular, redirectIfNeeded */

// create main application module
var app = angular.module('Application', ['pascalprecht.translate', 'ngCookies', 'angular-md5', 'ui-notification', 'ui.bootstrap']);

app.controller('SetupController', ['$scope', 'Client', function ($scope, Client) {
    // Stupid angular location provider either wants html5 location mode or not, do the query parsing on my own
    const search = decodeURIComponent(window.location.search).slice(1).split('&').map(function (item) { return item.split('='); }).reduce(function (o, k) { o[k[0]] = k[1]; return o; }, {});

    $scope.client = Client;
    $scope.view = '';
    $scope.initialized = false;
    $scope.setupToken = '';
    $scope.firstTimeLoginUrl = '';

    $scope.owner = {
        error: null,
        busy: false,

        email: '',
        displayName: '',
        username: '',
        password: '',

        submit: function () {
            $scope.owner.busy = true;
            $scope.owner.error = null;

            var data = {
                username: $scope.owner.username,
                password: $scope.owner.password,
                email: $scope.owner.email,
                displayName: $scope.owner.displayName,
                setupToken: $scope.setupToken
            };

            Client.createAdmin(data, function (error, autoLoginToken) {
                if (error && error.statusCode === 400) {
                    $scope.owner.busy = false;

                    if (error.message === 'Invalid email') {
                        $scope.owner.error = { email: error.message };
                        $scope.owner.email = '';
                        $scope.ownerForm.email.$setPristine();
                        setTimeout(function () { $('#inputEmail').focus(); }, 200);
                    } else {
                        $scope.owner.error = { username: error.message };
                        $scope.owner.username = '';
                        $scope.ownerForm.username.$setPristine();
                        setTimeout(function () { $('#inputUsername').focus(); }, 200);
                    }
                    return;
                } else if (error) {
                    $scope.owner.busy = false;
                    console.error('Internal error', error);
                    $scope.owner.error = { generic: error.message };
                    return;
                }

                // set token to autologin on first oidc flow
                localStorage.cloudronFirstTimeToken = autoLoginToken;

                $scope.firstTimeLoginUrl = '/openid/auth?client_id=cid-webadmin&scope=openid email profile&response_type=code token&redirect_uri=' + window.location.origin + '/authcallback.html';

                setView('finished');
            });
        }
    };

    function setView(view) {
        if (view === 'finished') {
            $scope.view = 'finished';
        } else {
            $scope.view = 'owner';
        }
    }

    function init() {
        Client.getProvisionStatus(function (error, status) {
            if (error) return Client.initError(error, init);

            if (redirectIfNeeded(status, 'activation')) return; // redirected to some other view...

            setView(search.view);

            $scope.setupToken = search.setupToken;
            $scope.initialized = true;

            // Ensure we have a good autofocus
            setTimeout(function () {
                $(document).find("[autofocus]:first").focus();
            }, 250);
        });
    }

    init();
}]);
