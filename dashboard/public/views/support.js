'use strict';

/* global angular:false */
/* global $:false */
/* global ISTATES */
/* global async */

angular.module('Application').controller('SupportController', ['$scope', '$location', 'Client', function ($scope, $location, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().isAtLeastOwner) $location.path('/'); });

    $scope.ready = false;
    $scope.config = Client.getConfig();
    $scope.user = Client.getUserInfo();
    $scope.installedApps = Client.getInstalledApps();

    $scope.toggleSshSupportError = '';
    $scope.sshSupportEnabled = false;

    $scope.troubleshoot = $location.search().troubleshoot;

    $scope.updateAllBusy = false;
    $scope.repairAllBusy = false;
    $scope.troubleshootingMessage = '';

    $scope.updateAll = function () {
        $scope.updateAllBusy = true;
        $scope.troubleshootingMessage = '';
        let count = 0, unstable = 0;

        Client.checkForUpdates(function (error) {
            if (error) Client.error(error);

            async.eachSeries(Object.keys($scope.config.update), function (appId, iteratorDone) {
                if ($scope.config.update[appId].unstable) { ++unstable; return iteratorDone(); }

                Client.updateApp(appId, $scope.config.update[appId].manifest, { skipBackup: false }, function (error) {
                   if (error) Client.error(error);
                   else ++count;

                    iteratorDone();
                });
            }, function () {
                $scope.troubleshootingMessage = `${count} apps updated. ${unstable} apps with unstable updates skipped.`;
                $scope.updateAllBusy = false;
            });
        });
    };

    $scope.repairAll = function () {
        $scope.repairAllBusy = true;
        $scope.troubleshootingMessage = '';
        let count = 0;

        Client.refreshInstalledApps(function () {
            async.eachSeries($scope.installedApps, function (app, iteratorDone) {
                if (app.installationState !== ISTATES.ERROR) return iteratorDone();

                Client.repairApp(app.id, {}, function (error) {
                    if (error) Client.error(error);
                    else ++count;

                    iteratorDone();
                });
            }, function () {
                $scope.troubleshootingMessage = `${count} apps repaired.`;
                $scope.repairAllBusy = false;
            });
        });
    };

    $scope.toggleSshSupport = function () {
        $scope.toggleSshSupportError = '';

        Client.enableRemoteSupport(!$scope.sshSupportEnabled, function (error) {
            if (error) {
                if (error.statusCode === 412 || error.statusCode === 417) $scope.toggleSshSupportError = error.message;
                else console.error(error);
                return;
            }

            $scope.sshSupportEnabled = !$scope.sshSupportEnabled;
        });
    };

    Client.onReady(function () {
        Client.getRemoteSupport(function (error, enabled) {
            if (error) return console.error(error);

            $scope.sshSupportEnabled = enabled;

            $scope.ready = true;
        });
    });

    $('.modal-backdrop').remove();
}]);
