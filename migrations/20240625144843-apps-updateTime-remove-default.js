'use strict';

exports.up = async function(db) {
    await db.runSql('ALTER TABLE apps MODIFY updateTime TIMESTAMP NULL DEFAULT NULL');
    await db.runSql('UPDATE apps SET updateTime=? WHERE creationTime=updateTime', [ null ]);
};

exports.down = async function(/* db */) {
};
