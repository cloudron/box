'use strict';

exports.up = function(db, callback) {
    db.runSql('DELETE FROM settings WHERE name=?', [ 'license_key' ], callback);
};

exports.down = function(db, callback) {
    callback();
};
