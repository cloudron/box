'use strict';

exports.up = function(db, callback) {
    db.all('SELECT value FROM settings WHERE name="backup_config"', function (error, results) {
        if (error || results.length === 0) return callback(error);

        const backupConfig = JSON.parse(results[0].value);
        if (backupConfig.provider === 'sshfs' || backupConfig.provider === 'cifs' || backupConfig.provider === 'nfs' || backupConfig.externalDisk) {
            backupConfig.chown = backupConfig.provider === 'nfs' || backupConfig.provider === 'sshfs' || backupConfig.externalDisk;
            backupConfig.preserveAttributes = !!backupConfig.externalDisk;
            backupConfig.provider = 'mountpoint';
            if (backupConfig.externalDisk) {
                backupConfig.mountPoint = backupConfig.backupFolder;
                backupConfig.prefix = '';
                delete backupConfig.backupFolder;
                delete backupConfig.externalDisk;
            }
            db.runSql('UPDATE settings SET value=? WHERE name="backup_config"', [JSON.stringify(backupConfig)], callback);
        } else {
            callback();
        }
    });
};

exports.down = function(db, callback) {
    callback();
};
