'use strict';

exports.up = async function (db) {
    await db.runSql('ALTER TABLE apps ADD COLUMN upstreamUri VARCHAR(256) DEFAULT ""');
};

exports.down = async function (db) {
    await db.runSql('ALTER TABLE apps DROP COLUMN upstreamUri');
};
