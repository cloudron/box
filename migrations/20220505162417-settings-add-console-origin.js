'use strict';

exports.up = function(db, callback) {
    db.all('SELECT * FROM settings WHERE name = ?', [ 'api_server_origin' ], function (error, result) {
        if (error || result.length === 0) return callback(error);

        let consoleOrigin;
        switch (result[0].value) {
        case 'https://api.dev.cloudron.io': consoleOrigin = 'https://console.dev.cloudron.io'; break;
        case 'https://api.staging.cloudron.io': consoleOrigin = 'https://console.staging.cloudron.io'; break;
        default: consoleOrigin = 'https://console.cloudron.io'; break;
        }

        db.runSql('REPLACE INTO settings (name, value) VALUES (?, ?)', [ 'console_server_origin', consoleOrigin ], callback);
    });
};

exports.down = function(db, callback) {
    callback();
};
