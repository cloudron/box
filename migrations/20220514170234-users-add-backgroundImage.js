'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE users ADD COLUMN backgroundImage MEDIUMBLOB', callback);
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE users DROP COLUMN backgroundImage', callback);
};
