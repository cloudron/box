'use strict';

exports.up = function(db, callback) {
    const cmd = 'CREATE TABLE blobs(' +
      'id VARCHAR(128) NOT NULL UNIQUE,' +
      'value MEDIUMBLOB,' +
      'PRIMARY KEY (id)) CHARACTER SET utf8 COLLATE utf8_bin';

    db.runSql(cmd, function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('DROP TABLE blobs', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
