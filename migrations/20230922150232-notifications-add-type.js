'use strict';

exports.up = async function (db) {
    await db.runSql('ALTER TABLE notifications ADD COLUMN type VARCHAR(128) NOT NULL DEFAULT ""');
};

exports.down = async function (db) {
    await db.runSql('ALTER TABLE notifications DROP COLUMN type');
};
