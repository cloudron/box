'use strict';

const async = require('async'),
    safe = require('safetydance');

exports.up = function(db, callback) {
    db.all('SELECT * FROM volumes', function (error, volumes) {
        if (error || volumes.length === 0) return callback(error);

        async.eachSeries(volumes, function (volume, iteratorDone) {
            if (volume.mountType !== 'noop') return iteratorDone();

            let mountType;
            if (safe.child_process.execSync(`mountpoint -q -- ${volume.hostPath}`)) {
                mountType = 'mountpoint';
            } else {
                mountType = 'filesystem';
            }
            db.runSql('UPDATE volumes SET mountType=? WHERE id=?', [ mountType, volume.id ], iteratorDone);
        }, callback);
    });
};

exports.down = function(db, callback) {
    callback();
};
