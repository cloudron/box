'use strict';

exports.up = async function(db) {
    const result = await db.runSql('SELECT * FROM settings WHERE name=?', [ 'backup_storage' ]);
    if (!result.length) return;

    const storageConfig = JSON.parse(result[0].value);
    delete storageConfig.limits; // this is already saved in backup_limits key
    await db.runSql('UPDATE settings SET value=? WHERE name=?', [ JSON.stringify(storageConfig), 'backup_storage']);
};

exports.down = async function(/* db */) {
};

