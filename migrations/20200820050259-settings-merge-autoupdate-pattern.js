'use strict';

var async = require('async');

exports.up = function(db, callback) {
    db.runSql('SELECT * FROM settings WHERE name=?', ['app_autoupdate_pattern'], function (error, results) {
        if (error || results.length === 0) return callback(error); // will use defaults from box code

        var updatePattern = results[0].value; // use app auto update patter for the box as well

        async.series([
            db.runSql.bind(db, 'START TRANSACTION;'),
            db.runSql.bind(db, 'DELETE FROM settings WHERE name=? OR name=?', ['app_autoupdate_pattern', 'box_autoupdate_pattern']),
            db.runSql.bind(db, 'INSERT settings (name, value) VALUES(?, ?)', ['autoupdate_pattern', updatePattern]),
            db.runSql.bind(db, 'COMMIT')
        ], callback);
    });
};

exports.down = function(db, callback) {
    callback();
};
