'use strict';

exports.up = async function(db) {
    await db.runSql('UPDATE settings SET name=? WHERE name=?', [ 'ipv4_config', 'sysinfo_config']);
};

exports.down = async function(/* db */) {
};
