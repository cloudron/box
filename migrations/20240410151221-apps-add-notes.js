'use strict';

exports.up = async function (db) {
    await db.runSql('ALTER TABLE apps ADD COLUMN notes TEXT');
};

exports.down = async function (db) {
    await db.runSql('ALTER TABLE apps DROP COLUMN notes');
};
