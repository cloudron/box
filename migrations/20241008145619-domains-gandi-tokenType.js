'use strict';

exports.up = async function(db) {
    const domains = await db.runSql('SELECT * FROM domains');
    for (const domain of domains) {
        if (domain.provider !== 'gandi') continue;

        const config = JSON.parse(domain.configJson);
        config.tokenType = 'ApiKey';

        await db.runSql('UPDATE domains SET configJson = ? WHERE domain = ?', [ JSON.stringify(config), domain.domain ]);
    }
};

exports.down = async function( /* db */) {
};
