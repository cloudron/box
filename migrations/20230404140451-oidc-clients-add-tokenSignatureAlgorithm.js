'use strict';

exports.up = async function(db) {
    await db.runSql('ALTER TABLE oidcClients ADD COLUMN tokenSignatureAlgorithm VARCHAR(128) DEFAULT ""');
};

exports.down = async function(db) {
    await db.runSql('ALTER TABLE oidcClients DROP COLUMN tokenSignatureAlgorithm');
};
