'use strict';

const safe = require('safetydance');

exports.up = async function (db) {
    const mailDomains = await db.runSql('SELECT * FROM mail', []);

    for (const mailDomain of mailDomains) {
        let catchAll = safe.JSON.parse(mailDomain.catchAllJson) || [];
        if (catchAll.length === 0) continue;

        catchAll = catchAll.map(a => `${a}@${mailDomain.domain}`);
        await db.runSql('UPDATE mail SET catchAllJson = ? WHERE domain = ?', [ JSON.stringify(catchAll), mailDomain.domain ]);
    }
};

exports.down = async function( /* db */) {
};
