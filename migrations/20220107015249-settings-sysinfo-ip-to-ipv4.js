'use strict';

exports.up = function(db, callback) {
    db.runSql('SELECT value FROM settings WHERE name=?', [ 'sysinfo_config' ], function (error, result) {
        if (error || result.length === 0) return callback(error);
        const sysinfoConfig = JSON.parse(result[0].value);
        if (sysinfoConfig.provider !== 'fixed' || !sysinfoConfig.ip) return callback();
        sysinfoConfig.ipv4 = sysinfoConfig.ip;
        delete sysinfoConfig.ip;

        db.runSql('REPLACE INTO settings (name, value) VALUES(?, ?)', [ 'sysinfo_config', JSON.stringify(sysinfoConfig) ], callback);
    });
};

exports.down = function(db, callback) {
    callback();
};
