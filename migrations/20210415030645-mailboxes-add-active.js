'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE mailboxes ADD COLUMN active BOOLEAN DEFAULT 1', function (error) {
        if (error) return callback(error);

        callback();
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE mailboxes DROP COLUMN active', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

