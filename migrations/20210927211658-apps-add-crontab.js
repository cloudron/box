'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE apps ADD COLUMN crontab TEXT', callback);
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE apps DROP COLUMN crontab', callback);
};
