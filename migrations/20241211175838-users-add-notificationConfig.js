'use strict';

exports.up = async function (db) {
    await db.runSql('ALTER TABLE users ADD COLUMN notificationConfigJson TEXT');
};

exports.down = async function (db) {
    await db.runSql('ALTER TABLE users DROP COLUMN notificationConfigJson');
};
