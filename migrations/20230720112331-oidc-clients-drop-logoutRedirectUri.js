'use strict';

exports.up = async function(db) {
    await db.runSql('ALTER TABLE oidcClients DROP COLUMN logoutRedirectUri');
};

exports.down = async function(db) {
    await db.runSql('ALTER TABLE oidcClients ADD COLUMN logoutRedirectUri VARCHAR(256) NOT NULL');
};
