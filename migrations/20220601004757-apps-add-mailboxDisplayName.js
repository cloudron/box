'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE apps ADD COLUMN mailboxDisplayName VARCHAR(128) DEFAULT "" NOT NULL', [], callback);
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE apps DROP COLUMN mailboxDisplayName', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
