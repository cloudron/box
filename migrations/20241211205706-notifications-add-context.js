'use strict';

exports.up = async function (db) {
    await db.runSql('ALTER TABLE notifications ADD COLUMN context VARCHAR(128) DEFAULT ""');
};

exports.down = async function (db) {
    await db.runSql('ALTER TABLE notifications DROP COLUMN context');
};
