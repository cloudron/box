'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE users MODIFY loginLocationsJson MEDIUMTEXT', [], function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE users MODIFY loginLocationsJson TEXT', [], function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
