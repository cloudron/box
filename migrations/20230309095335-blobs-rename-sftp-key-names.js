'use strict';

exports.up = async function (db) {
    await db.runSql('UPDATE blobs SET id = ? WHERE id = ?', [ 'sftp_rsa_private_key', 'sftp_private_key' ]);
    await db.runSql('UPDATE blobs SET id = ? WHERE id = ?', [ 'sftp_rsa_public_key', 'sftp_public_key' ]);
};

exports.down = async function () {
};
