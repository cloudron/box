'use strict';

exports.up = async function(db) {
    await db.runSql('UPDATE appPortBindings SET count=1 WHERE count IS NULL');
    await db.runSql('ALTER TABLE appPortBindings MODIFY count INTEGER NOT NULL DEFAULT 1');
};

exports.down = async function(/* db */) {
};
