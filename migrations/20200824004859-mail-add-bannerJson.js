'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE mail ADD COLUMN bannerJson TEXT', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE mail DROP COLUMN bannerJson', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
