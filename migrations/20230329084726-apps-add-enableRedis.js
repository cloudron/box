'use strict';

exports.up = async function(db) {
    await db.runSql('ALTER TABLE apps ADD COLUMN enableRedis BOOLEAN DEFAULT 1');
};

exports.down = async function(db) {
    await db.runSql('ALTER TABLE apps DROP COLUMN enableRedis');
};

