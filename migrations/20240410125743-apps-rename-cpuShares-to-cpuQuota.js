'use strict';

exports.up = async function (db) {
    await db.runSql('ALTER TABLE apps RENAME COLUMN cpuShares to cpuQuota');
    await db.runSql('ALTER TABLE apps MODIFY COLUMN cpuQuota INTEGER DEFAULT 100');
    await db.runSql('UPDATE apps SET cpuQuota=?', [ 100 ]);
};

exports.down = async function () {
};
