'use strict';

exports.up = async function(db) {
    let result = await db.runSql('SELECT * FROM settings WHERE name=?', ['ipv4_config']);
    if (result.length) {
        const ipv4Config = JSON.parse(result[0].value);
        ipv4Config.ip = ipv4Config.ipv4;
        delete ipv4Config.ipv4;
        await db.runSql('UPDATE settings SET value=? WHERE name=?', [ JSON.stringify(ipv4Config), 'ipv4_config']);
    }

    result = await db.runSql('SELECT * FROM settings WHERE name=?', ['ipv6_config']);
    if (result.length) {
        const ipv6Config = JSON.parse(result[0].value);
        ipv6Config.ip = ipv6Config.ipv6;
        delete ipv6Config.ipv6;
        await db.runSql('UPDATE settings SET value=? WHERE name=?', [ JSON.stringify(ipv6Config), 'ipv6_config']);
    }
};

exports.down = async function(/* db */) {
};
