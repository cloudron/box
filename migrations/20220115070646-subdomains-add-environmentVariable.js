'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE subdomains ADD COLUMN environmentVariable VARCHAR(128)', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE subdomains DROP COLUMN environmentVariable', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
