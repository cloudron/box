'use strict';

'use strict';

exports.up = async function (db) {
    const cmd = 'CREATE TABLE IF NOT EXISTS locks(' +
        'id VARCHAR(128) NOT NULL UNIQUE,' +
        'dataJson TEXT,' +
        'version INT DEFAULT 1,' +
        'ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' +
        ') CHARACTER SET utf8 COLLATE utf8_bin';

    await db.runSql(cmd);
};

exports.down = async function (db) {
    await db.runSql('DROP TABLE locks');
};
