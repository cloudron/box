'use strict';

exports.up = function(db, callback) {
    db.all('SELECT value FROM settings WHERE name="backup_config"', function (error, results) {
        if (error || results.length === 0) return callback(error);

        var backupConfig = JSON.parse(results[0].value);
        if (backupConfig.intervalSecs === 6 * 60 * 60) { // every 6 hours
            backupConfig.schedulePattern = '00 00 5,11,17,23 * * *';
        } else if (backupConfig.intervalSecs === 12 * 60 * 60) { // every 12 hours
            backupConfig.schedulePattern = '00 00 5,17 * * *';
        } else if (backupConfig.intervalSecs === 24 * 60 * 60) { // every day
            backupConfig.schedulePattern = '00 00 23 * * *';
        } else if (backupConfig.intervalSecs === 3 * 24 * 60 * 60) { // every 3 days (based on day)
            backupConfig.schedulePattern = '00 00 23 * * 1,3,5';
        } else if (backupConfig.intervalSecs === 7 * 24 * 60 * 60) { // every week (saturday)
            backupConfig.schedulePattern = '00 00 23 * * 6';
        } else { // default to everyday
            backupConfig.schedulePattern = '00 00 23 * * *';
        }

        delete backupConfig.intervalSecs;
        db.runSql('UPDATE settings SET value=? WHERE name="backup_config"', [ JSON.stringify(backupConfig) ], callback);
    });
};

exports.down = function(db, callback) {
    callback();
};
