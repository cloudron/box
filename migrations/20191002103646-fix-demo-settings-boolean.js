'use strict';

exports.up = function(db, callback) {
    // We clear all demo state in the Cloudron...the demo cloudron needs manual intervention afterwards
    db.runSql('REPLACE INTO settings (name, value) VALUES(?, ?)', [ 'demo', '' ], callback);
};

exports.down = function(db, callback) {
    callback();
};
