'use strict';

exports.up = async function (db) {
    await db.runSql('ALTER TABLE tokens ADD COLUMN allowedIpRanges TEXT NULL');
};

exports.down = async function (db) {
    await db.runSql('ALTER TABLE tokens DROP COLUMN allowedIpRanges');
};
