'use strict';

exports.up = async function (db) {
    await db.runSql('ALTER TABLE backups ADD COLUMN appConfigJson TEXT');
};

exports.down = async function (db) {
    await db.runSql('ALTER TABLE backups DROP COLUMN appConfigJson');
};
