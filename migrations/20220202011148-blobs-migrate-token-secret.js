'use strict';

const safe = require('safetydance');

const PROXY_AUTH_TOKEN_SECRET_FILE = '/home/yellowtent/platformdata/proxy-auth-token-secret';

exports.up = function (db, callback) {
    const token = safe.fs.readFileSync(PROXY_AUTH_TOKEN_SECRET_FILE);
    if (!token) return callback();
    db.runSql('INSERT INTO blobs (id, value) VALUES (?, ?)', [ 'proxy_auth_token_secret', token ], function (error) {
        if (error) return callback(error);
        safe.fs.unlinkSync(PROXY_AUTH_TOKEN_SECRET_FILE);
        callback();
    });
};

exports.down = function(db, callback) {
    callback();
};
