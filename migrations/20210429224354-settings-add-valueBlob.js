'use strict';

const fs = require('fs');

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE settings ADD COLUMN valueBlob MEDIUMBLOB', function (error) {
        if (error) return callback(error);

        fs.readFile('/home/yellowtent/boxdata/avatar.png', function (error, avatar) {
            if (error && error.code === 'ENOENT') return callback();
            if (error) return callback(error);

            db.runSql('INSERT INTO settings (name, valueBlob) VALUES (?, ?)', [ 'cloudron_avatar', avatar ], callback);
        });
    });
};

exports.down = function(db, callback) {
    callback();
};
