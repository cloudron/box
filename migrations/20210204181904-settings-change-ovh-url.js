'use strict';

exports.up = function(db, callback) {
    /* this contained an invalid migration of OVH URLs from s3 subdomain to storage subdomain. See https://forum.cloudron.io/topic/4584/issue-with-backups-listings-and-saving-backup-config-in-6-2  */
    callback();
};

exports.down = function(db, callback) {
    callback();
};
