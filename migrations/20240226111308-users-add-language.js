'use strict';

exports.up = async function (db) {
    await db.runSql('ALTER TABLE users ADD COLUMN language VARCHAR(8) NOT NULL DEFAULT ""');
};

exports.down = async function (db) {
    await db.runSql('ALTER TABLE users DROP COLUMN language');
};
