'use strict';

exports.up = async function (db) {
    var cmd = 'CREATE TABLE oidcClients(' +
        'id VARCHAR(128) NOT NULL UNIQUE,' +
        'secret VARCHAR(128) NOT NULL,' +
        'appId VARCHAR(128) DEFAULT "", ' +
        'name VARCHAR(128) DEFAULT "", ' +
        'loginRedirectUri VARCHAR(256) NOT NULL,' +
        'logoutRedirectUri VARCHAR(256) NOT NULL,' +
        'PRIMARY KEY (id))';

    await db.runSql(cmd);
};

exports.down = async function (db) {
    await db.runSql('DROP TABLE oidcClients');
};
