'use strict';

exports.up = async function (db) {
    await db.runSql('ALTER TABLE appPortBindings ADD COLUMN count INTEGER DEFAULT 1');
};

exports.down = async function (db) {
    await db.runSql('ALTER TABLE appPortBindings DROP COLUMN count');
};
