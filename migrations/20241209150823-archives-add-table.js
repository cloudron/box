'use strict';

exports.up = async function (db) {
    const cmd = 'CREATE TABLE archives(' +
        'id VARCHAR(128) NOT NULL UNIQUE,' +
        'backupId VARCHAR(128) NOT NULL,' +
        'creationTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,' +
        'appStoreIcon MEDIUMBLOB,' +
        'icon MEDIUMBLOB,' +
        'FOREIGN KEY(backupId) REFERENCES backups(id),' +
        'PRIMARY KEY (id)) ' +
        'CHARACTER SET utf8 COLLATE utf8_bin';

    await db.runSql(cmd);
};

exports.down = async function (db) {
    await db.runSql('DROP TABLE archives');
};
