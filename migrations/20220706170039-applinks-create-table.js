'use strict';

exports.up = async function (db) {
    var cmd = 'CREATE TABLE applinks(' +
                'id VARCHAR(128) NOT NULL UNIQUE,' +
                'accessRestrictionJson TEXT,' +
                'creationTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,' +
                'updateTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,' +
                'ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,' +
                'label VARCHAR(128),' +
                'tagsJson VARCHAR(2048),' +
                'icon MEDIUMBLOB,' +
                'upstreamUri VARCHAR(256) DEFAULT "",' +
                'PRIMARY KEY (id)) CHARACTER SET utf8 COLLATE utf8_bin';

    await db.runSql(cmd);
};

exports.down = async function (db) {
    await db.runSql('DROP TABLE applinks');
};

