'use strict';

// WARNING: THIS MIGRATION IS WRONG. THIS GETS FIXED IN 20230825105154-settings-rename-to-directory-server-again.js
exports.up = async function(db) {
    await db.runSql('UPDATE settings SET name=? WHERE name=?', [ 'user_directory_config', 'directory_server_config']);
};

exports.down = async function(/* db */) {
};
