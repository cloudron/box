'use strict';

exports.up = async function(db) {
    await db.runSql('UPDATE settings SET name=?,value=? WHERE name=?', [ 'dashboard_subdomain', 'my', 'admin_fqdn']);
    await db.runSql('UPDATE settings SET name=? WHERE name=?', [ 'dashboard_domain', 'admin_domain']);

    let result = await db.runSql('SELECT * FROM settings WHERE name=?', [ 'mail_fqdn' ]);
    if (!result.length) return;
    const mailFqdn = result[0].value;

    result = await db.runSql('SELECT * FROM settings WHERE name=?', [ 'mail_domain' ]);
    if (!result.length) return;
    const mailDomain = result[0].value;
    const mailSubdomain = mailFqdn.slice(0, -mailDomain.length-1);

    await db.runSql('UPDATE settings SET name=?,value=? WHERE name=?', [ 'mail_subdomain', mailSubdomain, 'mail_fqdn']);
};

exports.down = async function(/* db */) {
};
