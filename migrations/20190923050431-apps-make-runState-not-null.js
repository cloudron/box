'use strict';

exports.up = function(db, callback) {
    db.runSql('UPDATE apps SET runState=? WHERE runState IS NULL', [ 'running' ], function (error) {
        if (error) return callback(error);

        db.runSql('ALTER TABLE apps MODIFY runState VARCHAR(512) NOT NULL', [], function (error) {
            if (error) console.error(error);
            callback(error);
        });
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE app MODIFY runState VARCHAR(512)', [], function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
