'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE apps ADD CONSTRAINT inbox_domain_constraint FOREIGN KEY(inboxDomain) REFERENCES domains(domain)', function (error)
{
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE apps DROP FOREIGN KEY inbox_domain_constraint', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

