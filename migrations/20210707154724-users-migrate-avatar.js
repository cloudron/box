'use strict';

exports.up = function(db, callback) {
    db.runSql('UPDATE users SET avatar="gravatar" WHERE avatar IS NULL', function (error) {
        if (error) return callback(error);

        db.runSql('ALTER TABLE users MODIFY avatar MEDIUMBLOB NOT NULL', callback);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE users MODIFY avatar MEDIUMBLOB', callback);
};
