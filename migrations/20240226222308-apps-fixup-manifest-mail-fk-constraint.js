'use strict';

// ensure the inboxDomain and mailboxDomain are cleared when the addons are missing or disabled
// this allows the domain to be deleted. otherwise, the ui hides these fields and user cannot do anything to delete the domain
exports.up = async function(db) {
    const apps = await db.runSql('SELECT * FROM apps', []);
    for (const app of apps) {
        const manifest = JSON.parse(app.manifestJson);
        if (!manifest.addons?.recvmail || !app.enableInbox) {
            await db.runSql('UPDATE apps SET enableInbox=?, inboxName=?, inboxDomain=? WHERE id=?', [ false, null, null, app.id ]);
        }

        if (!manifest.addons?.sendmail || !app.enableMailbox) {
            await db.runSql('UPDATE apps SET enableMailbox=?, mailboxName=?, mailboxDomain=? WHERE id=?', [ false, null, null, app.id ]);
        }
    }
};

exports.down = async function(/* db */) {
};
