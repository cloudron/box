'use strict';

exports.up = async function(db) {
    await db.runSql('UPDATE settings SET name=? WHERE name=?', [ 'directory_server_config', 'user_directory_config']);
};

exports.down = async function(/* db */) {
};
