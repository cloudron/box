![Translation status](https://translate.cloudron.io/widgets/cloudron/-/svg-badge.svg)

# Cloudron

[Cloudron](https://cloudron.io) is the best way to run apps on your server.

Web applications like email, contacts, blog, chat are the backbone of the modern
internet. Yet, we live in a world where hosting these essential applications is
a complex task.

We are building the ultimate platform for self-hosting web apps. The Cloudron allows
anyone to effortlessly host web applications on their server on their own terms.

## Features

* Single click install for apps. Check out the [App Store](https://cloudron.io/appstore.html).

* Per-app encrypted backups and restores.

* App updates delivered via the App Store.

* Secure - Cloudron manages the firewall. All apps are secured with HTTPS. Certificates are
  installed and renewed automatically.

* Centralized User & Group management. Control who can access which app.

* Single Sign On. Use same credentials across all apps.

* Automatic updates for the Cloudron platform.

* Trivially migrate to another server keeping your apps and data (for example, switch your
  infrastructure provider or move to a bigger server).

* Comprehensive [REST API](https://docs.cloudron.io/api/).

* [CLI](https://docs.cloudron.io/custom-apps/cli/) to configure apps.

* Alerts, audit logs, graphs, dns management ... and much more

## Demo

Try our demo at https://my.demo.cloudron.io (username: cloudron password: cloudron).

## Installing

[Install script](https://docs.cloudron.io/installation/) - [Pricing](https://cloudron.io/pricing.html)

**Note:** This repo is just a part of what gets installed on the server. Database addons,
Mail Server, Stat contains etc are not part of this repo. As such, don't clone this repo and
npm install and expect something to work.

## License

Please note that the Cloudron code is under a source-available license. This is not the same as an
open source license but ensures the code is available for transparency and introspection (and hacking!).

## Contributions

We are very restrictive in merging changes. We are a small team and would like to keep our maintenance burden low,
not to mention legal issues. It might be best to discuss features first in the [forum](https://forum.cloudron.io),
to also figure out how many other people will use it to justify maintenance for a feature.

# Localization

![Translation status](https://translate.cloudron.io/widgets/cloudron/-/287x66-white.png)

## Support

* [Documentation](https://docs.cloudron.io/)
* [Forum](https://forum.cloudron.io/)


